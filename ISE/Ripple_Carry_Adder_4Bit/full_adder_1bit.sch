<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_11" />
        <signal name="XLXN_18" />
        <signal name="XLXN_25" />
        <signal name="XLXN_32" />
        <signal name="XLXN_39" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_46" />
        <signal name="XLXN_5" />
        <signal name="FA_IN_CARRY" />
        <signal name="FA_IN_B" />
        <signal name="FA_IN_A" />
        <signal name="XLXN_51" />
        <signal name="FA_OUT_SUM" />
        <signal name="FA_OUT_CARRY" />
        <port polarity="Input" name="FA_IN_CARRY" />
        <port polarity="Input" name="FA_IN_B" />
        <port polarity="Input" name="FA_IN_A" />
        <port polarity="Output" name="FA_OUT_SUM" />
        <port polarity="Output" name="FA_OUT_CARRY" />
        <blockdef name="half_adder">
            <timestamp>2014-10-23T10:18:19</timestamp>
            <rect width="304" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="432" y1="-96" y2="-96" x1="368" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="half_adder" name="XLXI_1">
            <blockpin signalname="FA_IN_CARRY" name="HA_IN_A" />
            <blockpin signalname="XLXN_4" name="HA_IN_B" />
            <blockpin signalname="FA_OUT_SUM" name="HA_OUT_SUM" />
            <blockpin signalname="XLXN_5" name="HA_OUT_CARRY" />
        </block>
        <block symbolname="or2" name="XLXI_3">
            <blockpin signalname="XLXN_3" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="FA_OUT_CARRY" name="O" />
        </block>
        <block symbolname="half_adder" name="XLXI_2">
            <blockpin signalname="FA_IN_A" name="HA_IN_A" />
            <blockpin signalname="FA_IN_B" name="HA_IN_B" />
            <blockpin signalname="XLXN_4" name="HA_OUT_SUM" />
            <blockpin signalname="XLXN_3" name="HA_OUT_CARRY" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1056" y="656" name="XLXI_3" orien="R90" />
        <branch name="XLXN_3">
            <wire x2="752" y1="400" y2="400" x1="688" />
            <wire x2="752" y1="400" y2="496" x1="752" />
            <wire x2="1120" y1="496" y2="496" x1="752" />
            <wire x2="1120" y1="496" y2="640" x1="1120" />
            <wire x2="1120" y1="640" y2="656" x1="1120" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1184" y1="640" y2="656" x1="1184" />
            <wire x2="1536" y1="640" y2="640" x1="1184" />
            <wire x2="1536" y1="336" y2="336" x1="1456" />
            <wire x2="1536" y1="336" y2="640" x1="1536" />
        </branch>
        <branch name="FA_IN_B">
            <wire x2="240" y1="400" y2="400" x1="224" />
            <wire x2="256" y1="400" y2="400" x1="240" />
        </branch>
        <branch name="FA_IN_A">
            <wire x2="240" y1="336" y2="336" x1="224" />
            <wire x2="256" y1="336" y2="336" x1="240" />
        </branch>
        <instance x="256" y="432" name="XLXI_2" orien="R0">
        </instance>
        <iomarker fontsize="28" x="224" y="336" name="FA_IN_A" orien="R180" />
        <iomarker fontsize="28" x="224" y="400" name="FA_IN_B" orien="R180" />
        <branch name="XLXN_4">
            <wire x2="1024" y1="336" y2="336" x1="688" />
        </branch>
        <instance x="1024" y="368" name="XLXI_1" orien="R0">
        </instance>
        <branch name="FA_IN_CARRY">
            <wire x2="864" y1="256" y2="272" x1="864" />
            <wire x2="1008" y1="272" y2="272" x1="864" />
            <wire x2="1024" y1="272" y2="272" x1="1008" />
        </branch>
        <iomarker fontsize="28" x="864" y="256" name="FA_IN_CARRY" orien="R270" />
        <branch name="FA_OUT_SUM">
            <wire x2="1488" y1="272" y2="272" x1="1456" />
        </branch>
        <iomarker fontsize="28" x="1488" y="272" name="FA_OUT_SUM" orien="R0" />
        <branch name="FA_OUT_CARRY">
            <wire x2="1152" y1="912" y2="944" x1="1152" />
        </branch>
        <iomarker fontsize="28" x="1152" y="944" name="FA_OUT_CARRY" orien="R90" />
    </sheet>
</drawing>