<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="HA_IN_A" />
        <signal name="HA_IN_B" />
        <signal name="HA_OUT_SUM" />
        <signal name="HA_OUT_CARRY" />
        <port polarity="Input" name="HA_IN_A" />
        <port polarity="Input" name="HA_IN_B" />
        <port polarity="Output" name="HA_OUT_SUM" />
        <port polarity="Output" name="HA_OUT_CARRY" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="HA_IN_B" name="I0" />
            <blockpin signalname="HA_IN_A" name="I1" />
            <blockpin signalname="HA_OUT_CARRY" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_2">
            <blockpin signalname="HA_IN_B" name="I0" />
            <blockpin signalname="HA_IN_A" name="I1" />
            <blockpin signalname="HA_OUT_SUM" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="480" y="288" name="XLXI_2" orien="R0" />
        <branch name="HA_IN_A">
            <wire x2="416" y1="160" y2="160" x1="240" />
            <wire x2="480" y1="160" y2="160" x1="416" />
            <wire x2="416" y1="160" y2="304" x1="416" />
        </branch>
        <branch name="HA_IN_B">
            <wire x2="352" y1="224" y2="224" x1="240" />
            <wire x2="480" y1="224" y2="224" x1="352" />
            <wire x2="352" y1="224" y2="304" x1="352" />
        </branch>
        <instance x="288" y="304" name="XLXI_1" orien="R90" />
        <iomarker fontsize="28" x="240" y="160" name="HA_IN_A" orien="R180" />
        <iomarker fontsize="28" x="240" y="224" name="HA_IN_B" orien="R180" />
        <branch name="HA_OUT_SUM">
            <wire x2="768" y1="192" y2="192" x1="736" />
        </branch>
        <iomarker fontsize="28" x="768" y="192" name="HA_OUT_SUM" orien="R0" />
        <branch name="HA_OUT_CARRY">
            <wire x2="384" y1="560" y2="592" x1="384" />
        </branch>
        <iomarker fontsize="28" x="384" y="592" name="HA_OUT_CARRY" orien="R90" />
    </sheet>
</drawing>