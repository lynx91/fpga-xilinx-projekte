<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="IN_CARRY" />
        <signal name="A3" />
        <signal name="B3" />
        <signal name="A2" />
        <signal name="B2" />
        <signal name="A1" />
        <signal name="B1" />
        <signal name="A0" />
        <signal name="B0" />
        <signal name="OUT_CARRY" />
        <signal name="S3" />
        <signal name="S2" />
        <signal name="S1" />
        <signal name="S0" />
        <port polarity="Input" name="IN_CARRY" />
        <port polarity="Input" name="A3" />
        <port polarity="Input" name="B3" />
        <port polarity="Input" name="A2" />
        <port polarity="Input" name="B2" />
        <port polarity="Input" name="A1" />
        <port polarity="Input" name="B1" />
        <port polarity="Input" name="A0" />
        <port polarity="Input" name="B0" />
        <port polarity="Output" name="OUT_CARRY" />
        <port polarity="Output" name="S3" />
        <port polarity="Output" name="S2" />
        <port polarity="Output" name="S1" />
        <port polarity="Output" name="S0" />
        <blockdef name="Ripple_Carry_Adder_4Bit">
            <timestamp>2014-10-23T10:51:28</timestamp>
            <rect width="432" x="64" y="-576" height="576" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="560" y1="-544" y2="-544" x1="496" />
            <line x2="560" y1="-416" y2="-416" x1="496" />
            <line x2="560" y1="-288" y2="-288" x1="496" />
            <line x2="560" y1="-160" y2="-160" x1="496" />
            <line x2="560" y1="-32" y2="-32" x1="496" />
        </blockdef>
        <block symbolname="Ripple_Carry_Adder_4Bit" name="XLXI_1">
            <blockpin signalname="IN_CARRY" name="A_4Bit_IN_CARRY" />
            <blockpin signalname="A3" name="A_4Bit_A3" />
            <blockpin signalname="B3" name="A_4Bit_B3" />
            <blockpin signalname="A2" name="A_4Bit_A2" />
            <blockpin signalname="B2" name="A_4Bit_B2" />
            <blockpin signalname="A1" name="A_4Bit_A1" />
            <blockpin signalname="B1" name="A_4Bit_B1" />
            <blockpin signalname="A0" name="A_4Bit_A0" />
            <blockpin signalname="B0" name="A_4Bit_B0" />
            <blockpin signalname="OUT_CARRY" name="A_4Bit_OUT_CARRY" />
            <blockpin signalname="S3" name="A_4Bit_S3" />
            <blockpin signalname="S2" name="A_4Bit_S2" />
            <blockpin signalname="S1" name="A_4Bit_S1" />
            <blockpin signalname="S0" name="A_4Bit_S0" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="832" y="960" name="XLXI_1" orien="R0">
        </instance>
        <branch name="IN_CARRY">
            <wire x2="832" y1="416" y2="416" x1="800" />
        </branch>
        <iomarker fontsize="28" x="800" y="416" name="IN_CARRY" orien="R180" />
        <branch name="A3">
            <wire x2="832" y1="480" y2="480" x1="800" />
        </branch>
        <iomarker fontsize="28" x="800" y="480" name="A3" orien="R180" />
        <branch name="B3">
            <wire x2="832" y1="544" y2="544" x1="800" />
        </branch>
        <iomarker fontsize="28" x="800" y="544" name="B3" orien="R180" />
        <branch name="A2">
            <wire x2="832" y1="608" y2="608" x1="800" />
        </branch>
        <iomarker fontsize="28" x="800" y="608" name="A2" orien="R180" />
        <branch name="B2">
            <wire x2="832" y1="672" y2="672" x1="800" />
        </branch>
        <iomarker fontsize="28" x="800" y="672" name="B2" orien="R180" />
        <branch name="A1">
            <wire x2="832" y1="736" y2="736" x1="800" />
        </branch>
        <iomarker fontsize="28" x="800" y="736" name="A1" orien="R180" />
        <branch name="B1">
            <wire x2="832" y1="800" y2="800" x1="800" />
        </branch>
        <iomarker fontsize="28" x="800" y="800" name="B1" orien="R180" />
        <branch name="A0">
            <wire x2="832" y1="864" y2="864" x1="800" />
        </branch>
        <iomarker fontsize="28" x="800" y="864" name="A0" orien="R180" />
        <branch name="B0">
            <wire x2="832" y1="928" y2="928" x1="800" />
        </branch>
        <iomarker fontsize="28" x="800" y="928" name="B0" orien="R180" />
        <branch name="OUT_CARRY">
            <wire x2="1424" y1="416" y2="416" x1="1392" />
        </branch>
        <iomarker fontsize="28" x="1424" y="416" name="OUT_CARRY" orien="R0" />
        <branch name="S3">
            <wire x2="1424" y1="544" y2="544" x1="1392" />
        </branch>
        <iomarker fontsize="28" x="1424" y="544" name="S3" orien="R0" />
        <branch name="S2">
            <wire x2="1424" y1="672" y2="672" x1="1392" />
        </branch>
        <iomarker fontsize="28" x="1424" y="672" name="S2" orien="R0" />
        <branch name="S1">
            <wire x2="1424" y1="800" y2="800" x1="1392" />
        </branch>
        <iomarker fontsize="28" x="1424" y="800" name="S1" orien="R0" />
        <branch name="S0">
            <wire x2="1424" y1="928" y2="928" x1="1392" />
        </branch>
        <iomarker fontsize="28" x="1424" y="928" name="S0" orien="R0" />
    </sheet>
</drawing>