--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : Ripple_Carry_Adder_4Bit.vhf
-- /___/   /\     Timestamp : 10/23/2014 13:05:30
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl C:/Users/heringm/ownCloud/BA/FPGA/ISE/Ripple_Carry_Adder_4Bit/Ripple_Carry_Adder_4Bit.vhf -w C:/Users/heringm/ownCloud/BA/FPGA/ISE/Ripple_Carry_Adder_4Bit/Ripple_Carry_Adder_4Bit.sch
--Design Name: Ripple_Carry_Adder_4Bit
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity half_adder_MUSER_Ripple_Carry_Adder_4Bit is
   port ( HA_IN_A      : in    std_logic; 
          HA_IN_B      : in    std_logic; 
          HA_OUT_CARRY : out   std_logic; 
          HA_OUT_SUM   : out   std_logic);
end half_adder_MUSER_Ripple_Carry_Adder_4Bit;

architecture BEHAVIORAL of half_adder_MUSER_Ripple_Carry_Adder_4Bit is
   attribute BOX_TYPE   : string ;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>HA_IN_B,
                I1=>HA_IN_A,
                O=>HA_OUT_CARRY);
   
   XLXI_2 : XOR2
      port map (I0=>HA_IN_B,
                I1=>HA_IN_A,
                O=>HA_OUT_SUM);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity full_adder_1bit_MUSER_Ripple_Carry_Adder_4Bit is
   port ( FA_IN_A      : in    std_logic; 
          FA_IN_B      : in    std_logic; 
          FA_IN_CARRY  : in    std_logic; 
          FA_OUT_CARRY : out   std_logic; 
          FA_OUT_SUM   : out   std_logic);
end full_adder_1bit_MUSER_Ripple_Carry_Adder_4Bit;

architecture BEHAVIORAL of full_adder_1bit_MUSER_Ripple_Carry_Adder_4Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_3       : std_logic;
   signal XLXN_4       : std_logic;
   signal XLXN_5       : std_logic;
   component half_adder_MUSER_Ripple_Carry_Adder_4Bit
      port ( HA_IN_A      : in    std_logic; 
             HA_IN_B      : in    std_logic; 
             HA_OUT_SUM   : out   std_logic; 
             HA_OUT_CARRY : out   std_logic);
   end component;
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : half_adder_MUSER_Ripple_Carry_Adder_4Bit
      port map (HA_IN_A=>FA_IN_CARRY,
                HA_IN_B=>XLXN_4,
                HA_OUT_CARRY=>XLXN_5,
                HA_OUT_SUM=>FA_OUT_SUM);
   
   XLXI_2 : half_adder_MUSER_Ripple_Carry_Adder_4Bit
      port map (HA_IN_A=>FA_IN_A,
                HA_IN_B=>FA_IN_B,
                HA_OUT_CARRY=>XLXN_3,
                HA_OUT_SUM=>XLXN_4);
   
   XLXI_3 : OR2
      port map (I0=>XLXN_3,
                I1=>XLXN_5,
                O=>FA_OUT_CARRY);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Ripple_Carry_Adder_4Bit is
   port ( A_4Bit_A0        : in    std_logic; 
          A_4Bit_A1        : in    std_logic; 
          A_4Bit_A2        : in    std_logic; 
          A_4Bit_A3        : in    std_logic; 
          A_4Bit_B0        : in    std_logic; 
          A_4Bit_B1        : in    std_logic; 
          A_4Bit_B2        : in    std_logic; 
          A_4Bit_B3        : in    std_logic; 
          A_4Bit_IN_CARRY  : in    std_logic; 
          A_4Bit_OUT_CARRY : out   std_logic; 
          A_4Bit_S0        : out   std_logic; 
          A_4Bit_S1        : out   std_logic; 
          A_4Bit_S2        : out   std_logic; 
          A_4Bit_S3        : out   std_logic);
end Ripple_Carry_Adder_4Bit;

architecture BEHAVIORAL of Ripple_Carry_Adder_4Bit is
   signal XLXN_1           : std_logic;
   signal XLXN_2           : std_logic;
   signal XLXN_3           : std_logic;
   component full_adder_1bit_MUSER_Ripple_Carry_Adder_4Bit
      port ( FA_IN_CARRY  : in    std_logic; 
             FA_IN_B      : in    std_logic; 
             FA_IN_A      : in    std_logic; 
             FA_OUT_SUM   : out   std_logic; 
             FA_OUT_CARRY : out   std_logic);
   end component;
   
begin
   XLXI_1 : full_adder_1bit_MUSER_Ripple_Carry_Adder_4Bit
      port map (FA_IN_A=>A_4Bit_A0,
                FA_IN_B=>A_4Bit_B0,
                FA_IN_CARRY=>A_4Bit_IN_CARRY,
                FA_OUT_CARRY=>XLXN_1,
                FA_OUT_SUM=>A_4Bit_S0);
   
   XLXI_2 : full_adder_1bit_MUSER_Ripple_Carry_Adder_4Bit
      port map (FA_IN_A=>A_4Bit_A1,
                FA_IN_B=>A_4Bit_B1,
                FA_IN_CARRY=>XLXN_1,
                FA_OUT_CARRY=>XLXN_2,
                FA_OUT_SUM=>A_4Bit_S1);
   
   XLXI_3 : full_adder_1bit_MUSER_Ripple_Carry_Adder_4Bit
      port map (FA_IN_A=>A_4Bit_A2,
                FA_IN_B=>A_4Bit_B2,
                FA_IN_CARRY=>XLXN_2,
                FA_OUT_CARRY=>XLXN_3,
                FA_OUT_SUM=>A_4Bit_S2);
   
   XLXI_4 : full_adder_1bit_MUSER_Ripple_Carry_Adder_4Bit
      port map (FA_IN_A=>A_4Bit_A3,
                FA_IN_B=>A_4Bit_B3,
                FA_IN_CARRY=>XLXN_3,
                FA_OUT_CARRY=>A_4Bit_OUT_CARRY,
                FA_OUT_SUM=>A_4Bit_S3);
   
end BEHAVIORAL;


