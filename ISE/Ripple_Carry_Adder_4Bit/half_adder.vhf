--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : half_adder.vhf
-- /___/   /\     Timestamp : 10/23/2014 13:05:31
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl C:/Users/heringm/ownCloud/BA/FPGA/ISE/Ripple_Carry_Adder_4Bit/half_adder.vhf -w C:/Users/heringm/ownCloud/BA/FPGA/ISE/Ripple_Carry_Adder_4Bit/half_adder.sch
--Design Name: half_adder
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity half_adder is
   port ( HA_IN_A      : in    std_logic; 
          HA_IN_B      : in    std_logic; 
          HA_OUT_CARRY : out   std_logic; 
          HA_OUT_SUM   : out   std_logic);
end half_adder;

architecture BEHAVIORAL of half_adder is
   attribute BOX_TYPE   : string ;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>HA_IN_B,
                I1=>HA_IN_A,
                O=>HA_OUT_CARRY);
   
   XLXI_2 : XOR2
      port map (I0=>HA_IN_B,
                I1=>HA_IN_A,
                O=>HA_OUT_SUM);
   
end BEHAVIORAL;


