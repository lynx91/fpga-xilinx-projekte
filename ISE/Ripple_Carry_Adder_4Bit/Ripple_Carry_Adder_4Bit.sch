<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="A_4Bit_IN_CARRY" />
        <signal name="A_4Bit_OUT_CARRY" />
        <signal name="A_4Bit_A3" />
        <signal name="A_4Bit_B3" />
        <signal name="A_4Bit_A2" />
        <signal name="A_4Bit_B2" />
        <signal name="A_4Bit_A1" />
        <signal name="A_4Bit_B1" />
        <signal name="A_4Bit_A0" />
        <signal name="A_4Bit_B0" />
        <signal name="A_4Bit_S3" />
        <signal name="A_4Bit_S2" />
        <signal name="A_4Bit_S1" />
        <signal name="A_4Bit_S0" />
        <port polarity="Input" name="A_4Bit_IN_CARRY" />
        <port polarity="Output" name="A_4Bit_OUT_CARRY" />
        <port polarity="Input" name="A_4Bit_A3" />
        <port polarity="Input" name="A_4Bit_B3" />
        <port polarity="Input" name="A_4Bit_A2" />
        <port polarity="Input" name="A_4Bit_B2" />
        <port polarity="Input" name="A_4Bit_A1" />
        <port polarity="Input" name="A_4Bit_B1" />
        <port polarity="Input" name="A_4Bit_A0" />
        <port polarity="Input" name="A_4Bit_B0" />
        <port polarity="Output" name="A_4Bit_S3" />
        <port polarity="Output" name="A_4Bit_S2" />
        <port polarity="Output" name="A_4Bit_S1" />
        <port polarity="Output" name="A_4Bit_S0" />
        <blockdef name="full_adder_1bit">
            <timestamp>2014-10-23T10:34:54</timestamp>
            <rect width="352" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="480" y1="-160" y2="-160" x1="416" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
        </blockdef>
        <block symbolname="full_adder_1bit" name="XLXI_1">
            <blockpin signalname="A_4Bit_IN_CARRY" name="FA_IN_CARRY" />
            <blockpin signalname="A_4Bit_B0" name="FA_IN_B" />
            <blockpin signalname="A_4Bit_A0" name="FA_IN_A" />
            <blockpin signalname="A_4Bit_S0" name="FA_OUT_SUM" />
            <blockpin signalname="XLXN_1" name="FA_OUT_CARRY" />
        </block>
        <block symbolname="full_adder_1bit" name="XLXI_2">
            <blockpin signalname="XLXN_1" name="FA_IN_CARRY" />
            <blockpin signalname="A_4Bit_B1" name="FA_IN_B" />
            <blockpin signalname="A_4Bit_A1" name="FA_IN_A" />
            <blockpin signalname="A_4Bit_S1" name="FA_OUT_SUM" />
            <blockpin signalname="XLXN_2" name="FA_OUT_CARRY" />
        </block>
        <block symbolname="full_adder_1bit" name="XLXI_3">
            <blockpin signalname="XLXN_2" name="FA_IN_CARRY" />
            <blockpin signalname="A_4Bit_B2" name="FA_IN_B" />
            <blockpin signalname="A_4Bit_A2" name="FA_IN_A" />
            <blockpin signalname="A_4Bit_S2" name="FA_OUT_SUM" />
            <blockpin signalname="XLXN_3" name="FA_OUT_CARRY" />
        </block>
        <block symbolname="full_adder_1bit" name="XLXI_4">
            <blockpin signalname="XLXN_3" name="FA_IN_CARRY" />
            <blockpin signalname="A_4Bit_B3" name="FA_IN_B" />
            <blockpin signalname="A_4Bit_A3" name="FA_IN_A" />
            <blockpin signalname="A_4Bit_S3" name="FA_OUT_SUM" />
            <blockpin signalname="A_4Bit_OUT_CARRY" name="FA_OUT_CARRY" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1024" y="624" name="XLXI_4" orien="R90">
        </instance>
        <instance x="1456" y="624" name="XLXI_3" orien="R90">
        </instance>
        <instance x="1888" y="624" name="XLXI_2" orien="R90">
        </instance>
        <instance x="2304" y="624" name="XLXI_1" orien="R90">
        </instance>
        <branch name="XLXN_1">
            <wire x2="2192" y1="608" y2="608" x1="2048" />
            <wire x2="2208" y1="608" y2="608" x1="2192" />
            <wire x2="2208" y1="608" y2="1104" x1="2208" />
            <wire x2="2208" y1="1104" y2="1120" x1="2208" />
            <wire x2="2336" y1="1120" y2="1120" x1="2208" />
            <wire x2="2048" y1="608" y2="624" x1="2048" />
            <wire x2="2336" y1="1104" y2="1120" x1="2336" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="1760" y1="608" y2="608" x1="1616" />
            <wire x2="1760" y1="608" y2="1104" x1="1760" />
            <wire x2="1760" y1="1104" y2="1120" x1="1760" />
            <wire x2="1920" y1="1120" y2="1120" x1="1760" />
            <wire x2="1616" y1="608" y2="624" x1="1616" />
            <wire x2="1920" y1="1104" y2="1120" x1="1920" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1328" y1="592" y2="592" x1="1184" />
            <wire x2="1328" y1="592" y2="1104" x1="1328" />
            <wire x2="1328" y1="1104" y2="1120" x1="1328" />
            <wire x2="1488" y1="1120" y2="1120" x1="1328" />
            <wire x2="1184" y1="592" y2="624" x1="1184" />
            <wire x2="1488" y1="1104" y2="1120" x1="1488" />
        </branch>
        <branch name="A_4Bit_IN_CARRY">
            <wire x2="2464" y1="352" y2="624" x1="2464" />
        </branch>
        <iomarker fontsize="28" x="2464" y="352" name="A_4Bit_IN_CARRY" orien="R270" />
        <branch name="A_4Bit_OUT_CARRY">
            <wire x2="1056" y1="1120" y2="1120" x1="688" />
            <wire x2="1056" y1="1104" y2="1120" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="688" y="1120" name="A_4Bit_OUT_CARRY" orien="R180" />
        <branch name="A_4Bit_A3">
            <wire x2="1056" y1="592" y2="624" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="1056" y="592" name="A_4Bit_A3" orien="R270" />
        <branch name="A_4Bit_B3">
            <wire x2="1120" y1="592" y2="624" x1="1120" />
        </branch>
        <iomarker fontsize="28" x="1120" y="592" name="A_4Bit_B3" orien="R270" />
        <branch name="A_4Bit_A2">
            <wire x2="1488" y1="592" y2="624" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="1488" y="592" name="A_4Bit_A2" orien="R270" />
        <branch name="A_4Bit_B2">
            <wire x2="1552" y1="592" y2="624" x1="1552" />
        </branch>
        <iomarker fontsize="28" x="1552" y="592" name="A_4Bit_B2" orien="R270" />
        <branch name="A_4Bit_A1">
            <wire x2="1920" y1="592" y2="624" x1="1920" />
        </branch>
        <iomarker fontsize="28" x="1920" y="592" name="A_4Bit_A1" orien="R270" />
        <branch name="A_4Bit_B1">
            <wire x2="1984" y1="592" y2="624" x1="1984" />
        </branch>
        <iomarker fontsize="28" x="1984" y="592" name="A_4Bit_B1" orien="R270" />
        <branch name="A_4Bit_A0">
            <wire x2="2336" y1="592" y2="624" x1="2336" />
        </branch>
        <iomarker fontsize="28" x="2336" y="592" name="A_4Bit_A0" orien="R270" />
        <branch name="A_4Bit_B0">
            <wire x2="2400" y1="592" y2="624" x1="2400" />
        </branch>
        <iomarker fontsize="28" x="2400" y="592" name="A_4Bit_B0" orien="R270" />
        <branch name="A_4Bit_S3">
            <wire x2="1184" y1="1104" y2="1136" x1="1184" />
        </branch>
        <iomarker fontsize="28" x="1184" y="1136" name="A_4Bit_S3" orien="R90" />
        <branch name="A_4Bit_S2">
            <wire x2="1616" y1="1104" y2="1136" x1="1616" />
        </branch>
        <iomarker fontsize="28" x="1616" y="1136" name="A_4Bit_S2" orien="R90" />
        <branch name="A_4Bit_S1">
            <wire x2="2048" y1="1104" y2="1136" x1="2048" />
        </branch>
        <iomarker fontsize="28" x="2048" y="1136" name="A_4Bit_S1" orien="R90" />
        <branch name="A_4Bit_S0">
            <wire x2="2464" y1="1104" y2="1136" x1="2464" />
        </branch>
        <iomarker fontsize="28" x="2464" y="1136" name="A_4Bit_S0" orien="R90" />
    </sheet>
</drawing>