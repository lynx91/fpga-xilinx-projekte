--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : full_adder_1bit.vhf
-- /___/   /\     Timestamp : 10/23/2014 13:05:30
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl C:/Users/heringm/ownCloud/BA/FPGA/ISE/Ripple_Carry_Adder_4Bit/full_adder_1bit.vhf -w C:/Users/heringm/ownCloud/BA/FPGA/ISE/Ripple_Carry_Adder_4Bit/full_adder_1bit.sch
--Design Name: full_adder_1bit
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity half_adder_MUSER_full_adder_1bit is
   port ( HA_IN_A      : in    std_logic; 
          HA_IN_B      : in    std_logic; 
          HA_OUT_CARRY : out   std_logic; 
          HA_OUT_SUM   : out   std_logic);
end half_adder_MUSER_full_adder_1bit;

architecture BEHAVIORAL of half_adder_MUSER_full_adder_1bit is
   attribute BOX_TYPE   : string ;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>HA_IN_B,
                I1=>HA_IN_A,
                O=>HA_OUT_CARRY);
   
   XLXI_2 : XOR2
      port map (I0=>HA_IN_B,
                I1=>HA_IN_A,
                O=>HA_OUT_SUM);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity full_adder_1bit is
   port ( FA_IN_A      : in    std_logic; 
          FA_IN_B      : in    std_logic; 
          FA_IN_CARRY  : in    std_logic; 
          FA_OUT_CARRY : out   std_logic; 
          FA_OUT_SUM   : out   std_logic);
end full_adder_1bit;

architecture BEHAVIORAL of full_adder_1bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_3       : std_logic;
   signal XLXN_4       : std_logic;
   signal XLXN_5       : std_logic;
   component half_adder_MUSER_full_adder_1bit
      port ( HA_IN_A      : in    std_logic; 
             HA_IN_B      : in    std_logic; 
             HA_OUT_SUM   : out   std_logic; 
             HA_OUT_CARRY : out   std_logic);
   end component;
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : half_adder_MUSER_full_adder_1bit
      port map (HA_IN_A=>FA_IN_CARRY,
                HA_IN_B=>XLXN_4,
                HA_OUT_CARRY=>XLXN_5,
                HA_OUT_SUM=>FA_OUT_SUM);
   
   XLXI_2 : half_adder_MUSER_full_adder_1bit
      port map (HA_IN_A=>FA_IN_A,
                HA_IN_B=>FA_IN_B,
                HA_OUT_CARRY=>XLXN_3,
                HA_OUT_SUM=>XLXN_4);
   
   XLXI_3 : OR2
      port map (I0=>XLXN_3,
                I1=>XLXN_5,
                O=>FA_OUT_CARRY);
   
end BEHAVIORAL;


