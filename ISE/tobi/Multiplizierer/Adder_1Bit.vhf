--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : Adder_1Bit.vhf
-- /___/   /\     Timestamp : 10/29/2014 09:05:44
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl D:/Programme/Xilinx/Projekte/Multiplizierer/Adder_1Bit.vhf -w D:/Programme/Xilinx/Projekte/Multiplizierer/Adder_1Bit.sch
--Design Name: Adder_1Bit
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Adder_1Bit is
   port ( Carry_IN  : in    std_logic; 
          Enable    : in    std_logic; 
          Enable_B  : in    std_logic; 
          In_A      : in    std_logic; 
          IN_B      : in    std_logic; 
          Carry_OUT : out   std_logic; 
          SUM       : out   std_logic);
end Adder_1Bit;

architecture BEHAVIORAL of Adder_1Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_3    : std_logic;
   signal XLXN_10   : std_logic;
   signal XLXN_12   : std_logic;
   signal XLXN_14   : std_logic;
   signal XLXN_15   : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>IN_B,
                I1=>Enable_B,
                O=>XLXN_3);
   
   XLXI_2 : AND2
      port map (I0=>Enable,
                I1=>XLXN_12,
                O=>SUM);
   
   XLXI_6 : XOR2
      port map (I0=>In_A,
                I1=>XLXN_3,
                O=>XLXN_10);
   
   XLXI_7 : XOR2
      port map (I0=>XLXN_10,
                I1=>Carry_IN,
                O=>XLXN_12);
   
   XLXI_8 : AND3
      port map (I0=>Enable,
                I1=>In_A,
                I2=>XLXN_3,
                O=>XLXN_14);
   
   XLXI_9 : AND3
      port map (I0=>Enable,
                I1=>XLXN_10,
                I2=>Carry_IN,
                O=>XLXN_15);
   
   XLXI_10 : OR2
      port map (I0=>XLXN_14,
                I1=>XLXN_15,
                O=>Carry_OUT);
   
end BEHAVIORAL;


