<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_4" />
        <signal name="XLXN_3" />
        <signal name="In_A" />
        <signal name="IN_B" />
        <signal name="Enable_B" />
        <signal name="Enable" />
        <signal name="XLXN_10" />
        <signal name="Carry_IN" />
        <signal name="XLXN_12" />
        <signal name="SUM" />
        <signal name="Carry_OUT" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <port polarity="Input" name="In_A" />
        <port polarity="Input" name="IN_B" />
        <port polarity="Input" name="Enable_B" />
        <port polarity="Input" name="Enable" />
        <port polarity="Input" name="Carry_IN" />
        <port polarity="Output" name="SUM" />
        <port polarity="Output" name="Carry_OUT" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="IN_B" name="I0" />
            <blockpin signalname="Enable_B" name="I1" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_6">
            <blockpin signalname="In_A" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_10" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="Enable" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="SUM" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_8">
            <blockpin signalname="Enable" name="I0" />
            <blockpin signalname="In_A" name="I1" />
            <blockpin signalname="XLXN_3" name="I2" />
            <blockpin signalname="XLXN_14" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_9">
            <blockpin signalname="Enable" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="Carry_IN" name="I2" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_7">
            <blockpin signalname="XLXN_10" name="I0" />
            <blockpin signalname="Carry_IN" name="I1" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_10">
            <blockpin signalname="XLXN_14" name="I0" />
            <blockpin signalname="XLXN_15" name="I1" />
            <blockpin signalname="Carry_OUT" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="336" y="416" name="XLXI_1" orien="R0" />
        <instance x="736" y="448" name="XLXI_6" orien="R0" />
        <instance x="1616" y="448" name="XLXI_2" orien="R0" />
        <instance x="496" y="576" name="XLXI_8" orien="R90" />
        <instance x="992" y="576" name="XLXI_9" orien="R90" />
        <branch name="XLXN_3">
            <wire x2="688" y1="320" y2="320" x1="592" />
            <wire x2="688" y1="320" y2="576" x1="688" />
            <wire x2="736" y1="320" y2="320" x1="688" />
        </branch>
        <branch name="In_A">
            <wire x2="624" y1="448" y2="448" x1="224" />
            <wire x2="624" y1="448" y2="576" x1="624" />
            <wire x2="624" y1="384" y2="448" x1="624" />
            <wire x2="736" y1="384" y2="384" x1="624" />
        </branch>
        <branch name="IN_B">
            <wire x2="336" y1="352" y2="352" x1="224" />
        </branch>
        <branch name="Enable_B">
            <wire x2="336" y1="288" y2="288" x1="224" />
        </branch>
        <branch name="Enable">
            <wire x2="560" y1="544" y2="544" x1="224" />
            <wire x2="560" y1="544" y2="576" x1="560" />
            <wire x2="1056" y1="544" y2="544" x1="560" />
            <wire x2="1056" y1="544" y2="576" x1="1056" />
            <wire x2="1520" y1="544" y2="544" x1="1056" />
            <wire x2="1520" y1="384" y2="544" x1="1520" />
            <wire x2="1616" y1="384" y2="384" x1="1520" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="1120" y1="352" y2="352" x1="992" />
            <wire x2="1120" y1="352" y2="576" x1="1120" />
            <wire x2="1216" y1="352" y2="352" x1="1120" />
        </branch>
        <branch name="Carry_IN">
            <wire x2="1184" y1="208" y2="288" x1="1184" />
            <wire x2="1216" y1="288" y2="288" x1="1184" />
            <wire x2="1184" y1="288" y2="576" x1="1184" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="1616" y1="320" y2="320" x1="1472" />
        </branch>
        <branch name="SUM">
            <wire x2="1984" y1="352" y2="352" x1="1872" />
        </branch>
        <branch name="Carry_OUT">
            <wire x2="848" y1="1104" y2="1120" x1="848" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="816" y1="832" y2="832" x1="624" />
            <wire x2="816" y1="832" y2="848" x1="816" />
        </branch>
        <instance x="1216" y="416" name="XLXI_7" orien="R0" />
        <instance x="752" y="848" name="XLXI_10" orien="R90" />
        <branch name="XLXN_15">
            <wire x2="880" y1="832" y2="848" x1="880" />
            <wire x2="1120" y1="832" y2="832" x1="880" />
        </branch>
        <iomarker fontsize="28" x="224" y="288" name="Enable_B" orien="R180" />
        <iomarker fontsize="28" x="224" y="352" name="IN_B" orien="R180" />
        <iomarker fontsize="28" x="224" y="448" name="In_A" orien="R180" />
        <iomarker fontsize="28" x="224" y="544" name="Enable" orien="R180" />
        <iomarker fontsize="28" x="1984" y="352" name="SUM" orien="R0" />
        <iomarker fontsize="28" x="1184" y="208" name="Carry_IN" orien="R270" />
        <iomarker fontsize="28" x="848" y="1120" name="Carry_OUT" orien="R90" />
    </sheet>
</drawing>