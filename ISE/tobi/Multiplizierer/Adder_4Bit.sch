<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="S3" />
        <signal name="S2" />
        <signal name="S1" />
        <signal name="S0" />
        <signal name="Carry_OUT" />
        <signal name="XLXN_43" />
        <signal name="XLXN_50" />
        <signal name="Enable" />
        <signal name="Enable_B" />
        <signal name="Carry_IN" />
        <signal name="ADD4_B0" />
        <signal name="ADD4_A0" />
        <signal name="ADD4_B1" />
        <signal name="ADD4_A1" />
        <signal name="ADD4_B2" />
        <signal name="ADD4_A2" />
        <signal name="ADD4_A3" />
        <signal name="ADD4_B3" />
        <port polarity="Output" name="S3" />
        <port polarity="Output" name="S2" />
        <port polarity="Output" name="S1" />
        <port polarity="Output" name="S0" />
        <port polarity="Output" name="Carry_OUT" />
        <port polarity="Input" name="Enable" />
        <port polarity="Input" name="Enable_B" />
        <port polarity="Input" name="Carry_IN" />
        <port polarity="Input" name="ADD4_B0" />
        <port polarity="Input" name="ADD4_A0" />
        <port polarity="Input" name="ADD4_B1" />
        <port polarity="Input" name="ADD4_A1" />
        <port polarity="Input" name="ADD4_B2" />
        <port polarity="Input" name="ADD4_A2" />
        <port polarity="Input" name="ADD4_A3" />
        <port polarity="Input" name="ADD4_B3" />
        <blockdef name="Adder_1Bit">
            <timestamp>2014-10-29T8:14:45</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <line x2="192" y1="48" y2="-12" x1="192" />
            <line x2="192" y1="-312" y2="-384" x1="192" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="0" y1="-192" y2="-192" x1="64" />
            <line x2="0" y1="-144" y2="-144" x1="64" />
            <line x2="384" y1="-176" y2="-176" x1="320" />
            <line x2="0" y1="-240" y2="-240" x1="64" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <block symbolname="Adder_1Bit" name="XLXI_2">
            <blockpin signalname="XLXN_43" name="Enable" />
            <blockpin signalname="ADD4_A3" name="In_A" />
            <blockpin signalname="ADD4_B3" name="IN_B" />
            <blockpin signalname="S3" name="SUM" />
            <blockpin signalname="XLXN_50" name="Enable_B" />
            <blockpin signalname="XLXN_3" name="Carry_IN" />
            <blockpin signalname="Carry_OUT" name="Carry_OUT" />
        </block>
        <block symbolname="Adder_1Bit" name="XLXI_3">
            <blockpin signalname="XLXN_43" name="Enable" />
            <blockpin signalname="ADD4_A2" name="In_A" />
            <blockpin signalname="ADD4_B2" name="IN_B" />
            <blockpin signalname="S2" name="SUM" />
            <blockpin signalname="XLXN_50" name="Enable_B" />
            <blockpin signalname="XLXN_4" name="Carry_IN" />
            <blockpin signalname="XLXN_3" name="Carry_OUT" />
        </block>
        <block symbolname="Adder_1Bit" name="XLXI_4">
            <blockpin signalname="XLXN_43" name="Enable" />
            <blockpin signalname="ADD4_A1" name="In_A" />
            <blockpin signalname="ADD4_B1" name="IN_B" />
            <blockpin signalname="S1" name="SUM" />
            <blockpin signalname="XLXN_50" name="Enable_B" />
            <blockpin signalname="XLXN_5" name="Carry_IN" />
            <blockpin signalname="XLXN_4" name="Carry_OUT" />
        </block>
        <block symbolname="Adder_1Bit" name="XLXI_5">
            <blockpin signalname="XLXN_43" name="Enable" />
            <blockpin signalname="ADD4_A0" name="In_A" />
            <blockpin signalname="ADD4_B0" name="IN_B" />
            <blockpin signalname="S0" name="SUM" />
            <blockpin signalname="XLXN_50" name="Enable_B" />
            <blockpin signalname="Carry_IN" name="Carry_IN" />
            <blockpin signalname="XLXN_5" name="Carry_OUT" />
        </block>
        <block symbolname="buf" name="XLXI_21">
            <blockpin signalname="Enable" name="I" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_20">
            <blockpin signalname="Enable_B" name="I" />
            <blockpin signalname="XLXN_50" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="688" y="560" name="XLXI_2" orien="R90">
        </instance>
        <instance x="1360" y="560" name="XLXI_3" orien="R90">
        </instance>
        <instance x="2032" y="560" name="XLXI_4" orien="R90">
        </instance>
        <instance x="2704" y="560" name="XLXI_5" orien="R90">
        </instance>
        <branch name="XLXN_3">
            <wire x2="1312" y1="752" y2="752" x1="1072" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1984" y1="752" y2="752" x1="1744" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="2656" y1="752" y2="752" x1="2416" />
        </branch>
        <branch name="S3">
            <wire x2="864" y1="944" y2="976" x1="864" />
        </branch>
        <branch name="S2">
            <wire x2="1536" y1="944" y2="976" x1="1536" />
        </branch>
        <branch name="S1">
            <wire x2="2208" y1="944" y2="976" x1="2208" />
        </branch>
        <branch name="S0">
            <wire x2="2880" y1="944" y2="976" x1="2880" />
        </branch>
        <branch name="Carry_OUT">
            <wire x2="640" y1="752" y2="752" x1="608" />
        </branch>
        <branch name="XLXN_43">
            <wire x2="752" y1="288" y2="288" x1="672" />
            <wire x2="752" y1="288" y2="480" x1="752" />
            <wire x2="768" y1="480" y2="480" x1="752" />
            <wire x2="768" y1="480" y2="560" x1="768" />
            <wire x2="1440" y1="480" y2="480" x1="768" />
            <wire x2="1440" y1="480" y2="560" x1="1440" />
            <wire x2="2112" y1="480" y2="480" x1="1440" />
            <wire x2="2112" y1="480" y2="560" x1="2112" />
            <wire x2="2784" y1="480" y2="480" x1="2112" />
            <wire x2="2784" y1="480" y2="560" x1="2784" />
        </branch>
        <instance x="448" y="320" name="XLXI_21" orien="R0" />
        <instance x="448" y="544" name="XLXI_20" orien="R0" />
        <branch name="XLXN_50">
            <wire x2="928" y1="512" y2="512" x1="672" />
            <wire x2="928" y1="512" y2="560" x1="928" />
            <wire x2="1600" y1="512" y2="512" x1="928" />
            <wire x2="1600" y1="512" y2="560" x1="1600" />
            <wire x2="2272" y1="512" y2="512" x1="1600" />
            <wire x2="2272" y1="512" y2="560" x1="2272" />
            <wire x2="2944" y1="512" y2="512" x1="2272" />
            <wire x2="2944" y1="512" y2="560" x1="2944" />
        </branch>
        <branch name="Enable">
            <wire x2="448" y1="288" y2="288" x1="416" />
        </branch>
        <branch name="Enable_B">
            <wire x2="448" y1="512" y2="512" x1="416" />
        </branch>
        <iomarker fontsize="28" x="864" y="976" name="S3" orien="R90" />
        <iomarker fontsize="28" x="1536" y="976" name="S2" orien="R90" />
        <iomarker fontsize="28" x="2208" y="976" name="S1" orien="R90" />
        <iomarker fontsize="28" x="2880" y="976" name="S0" orien="R90" />
        <iomarker fontsize="28" x="608" y="752" name="Carry_OUT" orien="R180" />
        <iomarker fontsize="28" x="416" y="288" name="Enable" orien="R180" />
        <iomarker fontsize="28" x="416" y="512" name="Enable_B" orien="R180" />
        <branch name="Carry_IN">
            <wire x2="3104" y1="752" y2="752" x1="3088" />
            <wire x2="3216" y1="512" y2="512" x1="3104" />
            <wire x2="3232" y1="512" y2="512" x1="3216" />
            <wire x2="3104" y1="512" y2="752" x1="3104" />
        </branch>
        <iomarker fontsize="28" x="3232" y="512" name="Carry_IN" orien="R0" />
        <branch name="ADD4_A0">
            <wire x2="2848" y1="400" y2="560" x1="2848" />
        </branch>
        <branch name="ADD4_A1">
            <wire x2="2176" y1="400" y2="560" x1="2176" />
        </branch>
        <branch name="ADD4_A2">
            <wire x2="1504" y1="400" y2="560" x1="1504" />
        </branch>
        <branch name="ADD4_A3">
            <wire x2="832" y1="400" y2="560" x1="832" />
        </branch>
        <iomarker fontsize="28" x="2928" y="400" name="ADD4_B0" orien="R270" />
        <branch name="ADD4_B0">
            <wire x2="2896" y1="544" y2="560" x1="2896" />
            <wire x2="2928" y1="544" y2="544" x1="2896" />
            <wire x2="2928" y1="400" y2="544" x1="2928" />
        </branch>
        <iomarker fontsize="28" x="2256" y="400" name="ADD4_B1" orien="R270" />
        <branch name="ADD4_B1">
            <wire x2="2224" y1="544" y2="560" x1="2224" />
            <wire x2="2256" y1="544" y2="544" x1="2224" />
            <wire x2="2256" y1="400" y2="544" x1="2256" />
        </branch>
        <iomarker fontsize="28" x="2848" y="400" name="ADD4_A0" orien="R270" />
        <iomarker fontsize="28" x="2176" y="400" name="ADD4_A1" orien="R270" />
        <iomarker fontsize="28" x="1584" y="400" name="ADD4_B2" orien="R270" />
        <branch name="ADD4_B2">
            <wire x2="1552" y1="544" y2="560" x1="1552" />
            <wire x2="1584" y1="544" y2="544" x1="1552" />
            <wire x2="1584" y1="400" y2="544" x1="1584" />
        </branch>
        <iomarker fontsize="28" x="912" y="400" name="ADD4_B3" orien="R270" />
        <branch name="ADD4_B3">
            <wire x2="880" y1="544" y2="560" x1="880" />
            <wire x2="912" y1="544" y2="544" x1="880" />
            <wire x2="912" y1="400" y2="544" x1="912" />
        </branch>
        <iomarker fontsize="28" x="832" y="400" name="ADD4_A3" orien="R270" />
        <iomarker fontsize="28" x="1504" y="400" name="ADD4_A2" orien="R270" />
    </sheet>
</drawing>