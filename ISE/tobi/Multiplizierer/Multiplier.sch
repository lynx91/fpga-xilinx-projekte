<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Enable" />
        <signal name="Y0" />
        <signal name="Y1" />
        <signal name="Y2" />
        <signal name="Y3" />
        <signal name="X0" />
        <signal name="X1" />
        <signal name="X2" />
        <signal name="X3" />
        <signal name="K0" />
        <signal name="K1" />
        <signal name="K2" />
        <signal name="K3" />
        <signal name="P0" />
        <signal name="P1" />
        <signal name="P2" />
        <signal name="P3" />
        <signal name="P4" />
        <signal name="P5" />
        <signal name="P6" />
        <signal name="P7" />
        <port polarity="Input" name="Enable" />
        <port polarity="Input" name="Y0" />
        <port polarity="Input" name="Y1" />
        <port polarity="Input" name="Y2" />
        <port polarity="Input" name="Y3" />
        <port polarity="Input" name="X0" />
        <port polarity="Input" name="X1" />
        <port polarity="Input" name="X2" />
        <port polarity="Input" name="X3" />
        <port polarity="Input" name="K0" />
        <port polarity="Input" name="K1" />
        <port polarity="Input" name="K2" />
        <port polarity="Input" name="K3" />
        <port polarity="Output" name="P0" />
        <port polarity="Output" name="P1" />
        <port polarity="Output" name="P2" />
        <port polarity="Output" name="P3" />
        <port polarity="Output" name="P4" />
        <port polarity="Output" name="P5" />
        <port polarity="Output" name="P6" />
        <port polarity="Output" name="P7" />
        <blockdef name="Multiplizierer_4Bit">
            <timestamp>2014-10-29T10:35:24</timestamp>
            <rect width="256" x="64" y="-800" height="720" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-704" y2="-704" x1="64" />
            <line x2="0" y1="-656" y2="-656" x1="64" />
            <line x2="0" y1="-560" y2="-560" x1="64" />
            <line x2="0" y1="-464" y2="-464" x1="64" />
            <line x2="0" y1="-368" y2="-368" x1="64" />
            <line x2="0" y1="-320" y2="-320" x1="64" />
            <line x2="0" y1="-256" y2="-256" x1="64" />
            <line x2="0" y1="-208" y2="-208" x1="64" />
            <line x2="0" y1="-112" y2="-112" x1="64" />
            <line x2="192" y1="-848" y2="-784" x1="192" />
            <line x2="384" y1="-512" y2="-512" x1="320" />
            <line x2="384" y1="-608" y2="-608" x1="320" />
            <line x2="384" y1="-560" y2="-560" x1="320" />
            <line x2="384" y1="-464" y2="-464" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-368" y2="-368" x1="320" />
            <line x2="384" y1="-320" y2="-320" x1="320" />
            <line x2="384" y1="-272" y2="-272" x1="320" />
        </blockdef>
        <block symbolname="Multiplizierer_4Bit" name="XLXI_1">
            <blockpin signalname="Y2" name="Mul_4_Y2" />
            <blockpin signalname="X1" name="Mul_4_X1" />
            <blockpin signalname="K2" name="Mul_4_K2" />
            <blockpin signalname="Y0" name="Mul_4_Y0" />
            <blockpin signalname="Y1" name="Mul_4_Y1" />
            <blockpin signalname="Y3" name="Mul_4_Y3" />
            <blockpin signalname="X0" name="Mul_4_X0" />
            <blockpin signalname="X2" name="Mul_4_X2" />
            <blockpin signalname="X3" name="Mul_4_X3" />
            <blockpin signalname="K0" name="Mul_4_K0" />
            <blockpin signalname="K1" name="Mul_4_K1" />
            <blockpin signalname="K3" name="Mul_4_K3" />
            <blockpin signalname="Enable" name="Enable" />
            <blockpin signalname="P2" name="Mul_4_P2" />
            <blockpin signalname="P0" name="Mul_4_P0" />
            <blockpin signalname="P1" name="Mul_4_P1" />
            <blockpin signalname="P3" name="Mul_4_P3" />
            <blockpin signalname="P4" name="Mul_4_P4" />
            <blockpin signalname="P5" name="Mul_4_P5" />
            <blockpin signalname="P6" name="Mul_4_P6" />
            <blockpin signalname="P7" name="Mul_4_P7" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="304" y="320" name="XLXI_1" orien="R90">
        </instance>
        <branch name="Enable">
            <wire x2="1392" y1="512" y2="512" x1="1152" />
        </branch>
        <iomarker fontsize="28" x="1392" y="512" name="Enable" orien="R0" />
        <branch name="Y0">
            <wire x2="1008" y1="288" y2="320" x1="1008" />
        </branch>
        <iomarker fontsize="28" x="1008" y="288" name="Y0" orien="R270" />
        <branch name="Y1">
            <wire x2="960" y1="288" y2="320" x1="960" />
        </branch>
        <iomarker fontsize="28" x="960" y="288" name="Y1" orien="R270" />
        <branch name="Y2">
            <wire x2="912" y1="288" y2="320" x1="912" />
        </branch>
        <iomarker fontsize="28" x="912" y="288" name="Y2" orien="R270" />
        <branch name="Y3">
            <wire x2="864" y1="288" y2="320" x1="864" />
        </branch>
        <iomarker fontsize="28" x="864" y="288" name="Y3" orien="R270" />
        <branch name="X0">
            <wire x2="768" y1="288" y2="320" x1="768" />
        </branch>
        <iomarker fontsize="28" x="768" y="288" name="X0" orien="R270" />
        <branch name="X1">
            <wire x2="720" y1="288" y2="320" x1="720" />
        </branch>
        <iomarker fontsize="28" x="720" y="288" name="X1" orien="R270" />
        <branch name="X2">
            <wire x2="672" y1="288" y2="320" x1="672" />
        </branch>
        <iomarker fontsize="28" x="672" y="288" name="X2" orien="R270" />
        <branch name="X3">
            <wire x2="624" y1="288" y2="320" x1="624" />
        </branch>
        <iomarker fontsize="28" x="624" y="288" name="X3" orien="R270" />
        <branch name="K0">
            <wire x2="560" y1="288" y2="320" x1="560" />
        </branch>
        <iomarker fontsize="28" x="560" y="288" name="K0" orien="R270" />
        <branch name="K1">
            <wire x2="512" y1="288" y2="320" x1="512" />
        </branch>
        <iomarker fontsize="28" x="512" y="288" name="K1" orien="R270" />
        <branch name="K2">
            <wire x2="464" y1="288" y2="320" x1="464" />
        </branch>
        <iomarker fontsize="28" x="464" y="288" name="K2" orien="R270" />
        <branch name="K3">
            <wire x2="416" y1="288" y2="320" x1="416" />
        </branch>
        <iomarker fontsize="28" x="416" y="288" name="K3" orien="R270" />
        <branch name="P0">
            <wire x2="912" y1="704" y2="736" x1="912" />
        </branch>
        <iomarker fontsize="28" x="912" y="736" name="P0" orien="R90" />
        <branch name="P1">
            <wire x2="864" y1="704" y2="736" x1="864" />
        </branch>
        <iomarker fontsize="28" x="864" y="736" name="P1" orien="R90" />
        <branch name="P2">
            <wire x2="816" y1="704" y2="736" x1="816" />
        </branch>
        <iomarker fontsize="28" x="816" y="736" name="P2" orien="R90" />
        <branch name="P3">
            <wire x2="768" y1="704" y2="736" x1="768" />
        </branch>
        <iomarker fontsize="28" x="768" y="736" name="P3" orien="R90" />
        <branch name="P4">
            <wire x2="720" y1="704" y2="736" x1="720" />
        </branch>
        <iomarker fontsize="28" x="720" y="736" name="P4" orien="R90" />
        <branch name="P5">
            <wire x2="672" y1="704" y2="736" x1="672" />
        </branch>
        <iomarker fontsize="28" x="672" y="736" name="P5" orien="R90" />
        <branch name="P6">
            <wire x2="624" y1="704" y2="736" x1="624" />
        </branch>
        <iomarker fontsize="28" x="624" y="736" name="P6" orien="R90" />
        <branch name="P7">
            <wire x2="576" y1="704" y2="736" x1="576" />
        </branch>
        <iomarker fontsize="28" x="576" y="736" name="P7" orien="R90" />
    </sheet>
</drawing>