--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : Adder_4Bit.vhf
-- /___/   /\     Timestamp : 10/29/2014 12:26:01
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl D:/Programme/Xilinx/Projekte/Multiplizierer/Adder_4Bit.vhf -w D:/Programme/Xilinx/Projekte/Multiplizierer/Adder_4Bit.sch
--Design Name: Adder_4Bit
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Adder_1Bit_MUSER_Adder_4Bit is
   port ( Carry_IN  : in    std_logic; 
          Enable    : in    std_logic; 
          Enable_B  : in    std_logic; 
          In_A      : in    std_logic; 
          IN_B      : in    std_logic; 
          Carry_OUT : out   std_logic; 
          SUM       : out   std_logic);
end Adder_1Bit_MUSER_Adder_4Bit;

architecture BEHAVIORAL of Adder_1Bit_MUSER_Adder_4Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_3    : std_logic;
   signal XLXN_10   : std_logic;
   signal XLXN_12   : std_logic;
   signal XLXN_14   : std_logic;
   signal XLXN_15   : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>IN_B,
                I1=>Enable_B,
                O=>XLXN_3);
   
   XLXI_2 : AND2
      port map (I0=>Enable,
                I1=>XLXN_12,
                O=>SUM);
   
   XLXI_6 : XOR2
      port map (I0=>In_A,
                I1=>XLXN_3,
                O=>XLXN_10);
   
   XLXI_7 : XOR2
      port map (I0=>XLXN_10,
                I1=>Carry_IN,
                O=>XLXN_12);
   
   XLXI_8 : AND3
      port map (I0=>Enable,
                I1=>In_A,
                I2=>XLXN_3,
                O=>XLXN_14);
   
   XLXI_9 : AND3
      port map (I0=>Enable,
                I1=>XLXN_10,
                I2=>Carry_IN,
                O=>XLXN_15);
   
   XLXI_10 : OR2
      port map (I0=>XLXN_14,
                I1=>XLXN_15,
                O=>Carry_OUT);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Adder_4Bit is
   port ( ADD4_A0   : in    std_logic; 
          ADD4_A1   : in    std_logic; 
          ADD4_A2   : in    std_logic; 
          ADD4_A3   : in    std_logic; 
          ADD4_B0   : in    std_logic; 
          ADD4_B1   : in    std_logic; 
          ADD4_B2   : in    std_logic; 
          ADD4_B3   : in    std_logic; 
          Carry_IN  : in    std_logic; 
          Enable    : in    std_logic; 
          Enable_B  : in    std_logic; 
          Carry_OUT : out   std_logic; 
          S0        : out   std_logic; 
          S1        : out   std_logic; 
          S2        : out   std_logic; 
          S3        : out   std_logic);
end Adder_4Bit;

architecture BEHAVIORAL of Adder_4Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_3    : std_logic;
   signal XLXN_4    : std_logic;
   signal XLXN_5    : std_logic;
   signal XLXN_43   : std_logic;
   signal XLXN_50   : std_logic;
   component Adder_1Bit_MUSER_Adder_4Bit
      port ( Enable    : in    std_logic; 
             In_A      : in    std_logic; 
             IN_B      : in    std_logic; 
             SUM       : out   std_logic; 
             Enable_B  : in    std_logic; 
             Carry_IN  : in    std_logic; 
             Carry_OUT : out   std_logic);
   end component;
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
begin
   XLXI_2 : Adder_1Bit_MUSER_Adder_4Bit
      port map (Carry_IN=>XLXN_3,
                Enable=>XLXN_43,
                Enable_B=>XLXN_50,
                In_A=>ADD4_A3,
                IN_B=>ADD4_B3,
                Carry_OUT=>Carry_OUT,
                SUM=>S3);
   
   XLXI_3 : Adder_1Bit_MUSER_Adder_4Bit
      port map (Carry_IN=>XLXN_4,
                Enable=>XLXN_43,
                Enable_B=>XLXN_50,
                In_A=>ADD4_A2,
                IN_B=>ADD4_B2,
                Carry_OUT=>XLXN_3,
                SUM=>S2);
   
   XLXI_4 : Adder_1Bit_MUSER_Adder_4Bit
      port map (Carry_IN=>XLXN_5,
                Enable=>XLXN_43,
                Enable_B=>XLXN_50,
                In_A=>ADD4_A1,
                IN_B=>ADD4_B1,
                Carry_OUT=>XLXN_4,
                SUM=>S1);
   
   XLXI_5 : Adder_1Bit_MUSER_Adder_4Bit
      port map (Carry_IN=>Carry_IN,
                Enable=>XLXN_43,
                Enable_B=>XLXN_50,
                In_A=>ADD4_A0,
                IN_B=>ADD4_B0,
                Carry_OUT=>XLXN_5,
                SUM=>S0);
   
   XLXI_20 : BUF
      port map (I=>Enable_B,
                O=>XLXN_50);
   
   XLXI_21 : BUF
      port map (I=>Enable,
                O=>XLXN_43);
   
end BEHAVIORAL;


