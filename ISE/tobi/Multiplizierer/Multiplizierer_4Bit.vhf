--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : Multiplizierer_4Bit.vhf
-- /___/   /\     Timestamp : 10/29/2014 12:26:02
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl D:/Programme/Xilinx/Projekte/Multiplizierer/Multiplizierer_4Bit.vhf -w D:/Programme/Xilinx/Projekte/Multiplizierer/Multiplizierer_4Bit.sch
--Design Name: Multiplizierer_4Bit
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Adder_1Bit_MUSER_Multiplizierer_4Bit is
   port ( Carry_IN  : in    std_logic; 
          Enable    : in    std_logic; 
          Enable_B  : in    std_logic; 
          In_A      : in    std_logic; 
          IN_B      : in    std_logic; 
          Carry_OUT : out   std_logic; 
          SUM       : out   std_logic);
end Adder_1Bit_MUSER_Multiplizierer_4Bit;

architecture BEHAVIORAL of Adder_1Bit_MUSER_Multiplizierer_4Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_3    : std_logic;
   signal XLXN_10   : std_logic;
   signal XLXN_12   : std_logic;
   signal XLXN_14   : std_logic;
   signal XLXN_15   : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>IN_B,
                I1=>Enable_B,
                O=>XLXN_3);
   
   XLXI_2 : AND2
      port map (I0=>Enable,
                I1=>XLXN_12,
                O=>SUM);
   
   XLXI_6 : XOR2
      port map (I0=>In_A,
                I1=>XLXN_3,
                O=>XLXN_10);
   
   XLXI_7 : XOR2
      port map (I0=>XLXN_10,
                I1=>Carry_IN,
                O=>XLXN_12);
   
   XLXI_8 : AND3
      port map (I0=>Enable,
                I1=>In_A,
                I2=>XLXN_3,
                O=>XLXN_14);
   
   XLXI_9 : AND3
      port map (I0=>Enable,
                I1=>XLXN_10,
                I2=>Carry_IN,
                O=>XLXN_15);
   
   XLXI_10 : OR2
      port map (I0=>XLXN_14,
                I1=>XLXN_15,
                O=>Carry_OUT);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Adder_4Bit_MUSER_Multiplizierer_4Bit is
   port ( ADD4_A0   : in    std_logic; 
          ADD4_A1   : in    std_logic; 
          ADD4_A2   : in    std_logic; 
          ADD4_A3   : in    std_logic; 
          ADD4_B0   : in    std_logic; 
          ADD4_B1   : in    std_logic; 
          ADD4_B2   : in    std_logic; 
          ADD4_B3   : in    std_logic; 
          Carry_IN  : in    std_logic; 
          Enable    : in    std_logic; 
          Enable_B  : in    std_logic; 
          Carry_OUT : out   std_logic; 
          S0        : out   std_logic; 
          S1        : out   std_logic; 
          S2        : out   std_logic; 
          S3        : out   std_logic);
end Adder_4Bit_MUSER_Multiplizierer_4Bit;

architecture BEHAVIORAL of Adder_4Bit_MUSER_Multiplizierer_4Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_3    : std_logic;
   signal XLXN_4    : std_logic;
   signal XLXN_5    : std_logic;
   signal XLXN_43   : std_logic;
   signal XLXN_50   : std_logic;
   component Adder_1Bit_MUSER_Multiplizierer_4Bit
      port ( Enable    : in    std_logic; 
             In_A      : in    std_logic; 
             IN_B      : in    std_logic; 
             SUM       : out   std_logic; 
             Enable_B  : in    std_logic; 
             Carry_IN  : in    std_logic; 
             Carry_OUT : out   std_logic);
   end component;
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
begin
   XLXI_2 : Adder_1Bit_MUSER_Multiplizierer_4Bit
      port map (Carry_IN=>XLXN_3,
                Enable=>XLXN_43,
                Enable_B=>XLXN_50,
                In_A=>ADD4_A3,
                IN_B=>ADD4_B3,
                Carry_OUT=>Carry_OUT,
                SUM=>S3);
   
   XLXI_3 : Adder_1Bit_MUSER_Multiplizierer_4Bit
      port map (Carry_IN=>XLXN_4,
                Enable=>XLXN_43,
                Enable_B=>XLXN_50,
                In_A=>ADD4_A2,
                IN_B=>ADD4_B2,
                Carry_OUT=>XLXN_3,
                SUM=>S2);
   
   XLXI_4 : Adder_1Bit_MUSER_Multiplizierer_4Bit
      port map (Carry_IN=>XLXN_5,
                Enable=>XLXN_43,
                Enable_B=>XLXN_50,
                In_A=>ADD4_A1,
                IN_B=>ADD4_B1,
                Carry_OUT=>XLXN_4,
                SUM=>S1);
   
   XLXI_5 : Adder_1Bit_MUSER_Multiplizierer_4Bit
      port map (Carry_IN=>Carry_IN,
                Enable=>XLXN_43,
                Enable_B=>XLXN_50,
                In_A=>ADD4_A0,
                IN_B=>ADD4_B0,
                Carry_OUT=>XLXN_5,
                SUM=>S0);
   
   XLXI_20 : BUF
      port map (I=>Enable_B,
                O=>XLXN_50);
   
   XLXI_21 : BUF
      port map (I=>Enable,
                O=>XLXN_43);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Multiplizierer_4Bit is
   port ( Enable   : in    std_logic; 
          Mul_4_K0 : in    std_logic; 
          Mul_4_K1 : in    std_logic; 
          Mul_4_K2 : in    std_logic; 
          Mul_4_K3 : in    std_logic; 
          Mul_4_X0 : in    std_logic; 
          Mul_4_X1 : in    std_logic; 
          Mul_4_X2 : in    std_logic; 
          Mul_4_X3 : in    std_logic; 
          Mul_4_Y0 : in    std_logic; 
          Mul_4_Y1 : in    std_logic; 
          Mul_4_Y2 : in    std_logic; 
          Mul_4_Y3 : in    std_logic; 
          Mul_4_P0 : out   std_logic; 
          Mul_4_P1 : out   std_logic; 
          Mul_4_P2 : out   std_logic; 
          Mul_4_P3 : out   std_logic; 
          Mul_4_P4 : out   std_logic; 
          Mul_4_P5 : out   std_logic; 
          Mul_4_P6 : out   std_logic; 
          Mul_4_P7 : out   std_logic);
end Multiplizierer_4Bit;

architecture BEHAVIORAL of Multiplizierer_4Bit is
   attribute BOX_TYPE   : string ;
   signal Carry_IN : std_logic;
   signal XLXN_1   : std_logic;
   signal XLXN_2   : std_logic;
   signal XLXN_3   : std_logic;
   signal XLXN_10  : std_logic;
   signal XLXN_11  : std_logic;
   signal XLXN_12  : std_logic;
   signal XLXN_13  : std_logic;
   signal XLXN_14  : std_logic;
   signal XLXN_15  : std_logic;
   signal XLXN_16  : std_logic;
   signal XLXN_17  : std_logic;
   signal XLXN_18  : std_logic;
   signal XLXN_101 : std_logic;
   signal XLXN_105 : std_logic;
   signal XLXN_109 : std_logic;
   signal XLXN_113 : std_logic;
   signal XLXN_118 : std_logic;
   signal XLXN_121 : std_logic;
   component Adder_4Bit_MUSER_Multiplizierer_4Bit
      port ( Enable    : in    std_logic; 
             Enable_B  : in    std_logic; 
             Carry_IN  : in    std_logic; 
             ADD4_B0   : in    std_logic; 
             ADD4_A0   : in    std_logic; 
             ADD4_B1   : in    std_logic; 
             ADD4_A1   : in    std_logic; 
             ADD4_B2   : in    std_logic; 
             ADD4_A2   : in    std_logic; 
             ADD4_A3   : in    std_logic; 
             ADD4_B3   : in    std_logic; 
             S3        : out   std_logic; 
             S2        : out   std_logic; 
             S1        : out   std_logic; 
             S0        : out   std_logic; 
             Carry_OUT : out   std_logic);
   end component;
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
begin
   XLXI_1 : Adder_4Bit_MUSER_Multiplizierer_4Bit
      port map (ADD4_A0=>Mul_4_K0,
                ADD4_A1=>Mul_4_K1,
                ADD4_A2=>Mul_4_K2,
                ADD4_A3=>Mul_4_K3,
                ADD4_B0=>XLXN_105,
                ADD4_B1=>XLXN_109,
                ADD4_B2=>XLXN_113,
                ADD4_B3=>XLXN_101,
                Carry_IN=>XLXN_118,
                Enable=>XLXN_121,
                Enable_B=>Mul_4_Y0,
                Carry_OUT=>XLXN_10,
                S0=>Mul_4_P0,
                S1=>XLXN_1,
                S2=>XLXN_2,
                S3=>XLXN_3);
   
   XLXI_2 : Adder_4Bit_MUSER_Multiplizierer_4Bit
      port map (ADD4_A0=>XLXN_1,
                ADD4_A1=>XLXN_2,
                ADD4_A2=>XLXN_3,
                ADD4_A3=>XLXN_10,
                ADD4_B0=>XLXN_105,
                ADD4_B1=>XLXN_109,
                ADD4_B2=>XLXN_113,
                ADD4_B3=>XLXN_101,
                Carry_IN=>XLXN_118,
                Enable=>XLXN_121,
                Enable_B=>Mul_4_Y1,
                Carry_OUT=>XLXN_11,
                S0=>Mul_4_P1,
                S1=>XLXN_14,
                S2=>XLXN_13,
                S3=>XLXN_12);
   
   XLXI_3 : Adder_4Bit_MUSER_Multiplizierer_4Bit
      port map (ADD4_A0=>XLXN_14,
                ADD4_A1=>XLXN_13,
                ADD4_A2=>XLXN_12,
                ADD4_A3=>XLXN_11,
                ADD4_B0=>XLXN_105,
                ADD4_B1=>XLXN_109,
                ADD4_B2=>XLXN_113,
                ADD4_B3=>XLXN_101,
                Carry_IN=>XLXN_118,
                Enable=>XLXN_121,
                Enable_B=>Mul_4_Y2,
                Carry_OUT=>XLXN_15,
                S0=>Mul_4_P2,
                S1=>XLXN_18,
                S2=>XLXN_17,
                S3=>XLXN_16);
   
   XLXI_4 : Adder_4Bit_MUSER_Multiplizierer_4Bit
      port map (ADD4_A0=>XLXN_18,
                ADD4_A1=>XLXN_17,
                ADD4_A2=>XLXN_16,
                ADD4_A3=>XLXN_15,
                ADD4_B0=>XLXN_105,
                ADD4_B1=>XLXN_109,
                ADD4_B2=>XLXN_113,
                ADD4_B3=>XLXN_101,
                Carry_IN=>XLXN_118,
                Enable=>XLXN_121,
                Enable_B=>Mul_4_Y3,
                Carry_OUT=>Mul_4_P7,
                S0=>Mul_4_P3,
                S1=>Mul_4_P4,
                S2=>Mul_4_P5,
                S3=>Mul_4_P6);
   
   XLXI_5 : BUF
      port map (I=>Mul_4_X0,
                O=>XLXN_105);
   
   XLXI_6 : BUF
      port map (I=>Mul_4_X1,
                O=>XLXN_109);
   
   XLXI_7 : BUF
      port map (I=>Mul_4_X2,
                O=>XLXN_113);
   
   XLXI_8 : BUF
      port map (I=>Mul_4_X3,
                O=>XLXN_101);
   
   XLXI_9 : BUF
      port map (I=>Carry_IN,
                O=>XLXN_118);
   
   XLXI_10 : BUF
      port map (I=>Enable,
                O=>XLXN_121);
   
   XLXI_11 : GND
      port map (G=>Carry_IN);
   
end BEHAVIORAL;


