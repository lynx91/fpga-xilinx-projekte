<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="Mul_4_Y1" />
        <signal name="Carry_IN" />
        <signal name="Enable" />
        <signal name="Mul_4_Y0" />
        <signal name="Mul_4_Y2" />
        <signal name="Mul_4_Y3" />
        <signal name="Mul_4_X2" />
        <signal name="Mul_4_X1" />
        <signal name="Mul_4_X0" />
        <signal name="Mul_4_X3" />
        <signal name="Mul_4_K3" />
        <signal name="Mul_4_K2" />
        <signal name="Mul_4_K1" />
        <signal name="Mul_4_K0" />
        <signal name="Mul_4_P1" />
        <signal name="Mul_4_P0" />
        <signal name="Mul_4_P2" />
        <signal name="Mul_4_P3" />
        <signal name="Mul_4_P4" />
        <signal name="Mul_4_P5" />
        <signal name="Mul_4_P6" />
        <signal name="Mul_4_P7" />
        <signal name="XLXN_101" />
        <signal name="XLXN_105" />
        <signal name="XLXN_109" />
        <signal name="XLXN_113" />
        <signal name="XLXN_118" />
        <signal name="XLXN_120" />
        <signal name="XLXN_121" />
        <signal name="XLXN_122" />
        <signal name="XLXN_123" />
        <signal name="XLXN_124" />
        <signal name="XLXN_125" />
        <port polarity="Input" name="Mul_4_Y1" />
        <port polarity="Input" name="Enable" />
        <port polarity="Input" name="Mul_4_Y0" />
        <port polarity="Input" name="Mul_4_Y2" />
        <port polarity="Input" name="Mul_4_Y3" />
        <port polarity="Input" name="Mul_4_X2" />
        <port polarity="Input" name="Mul_4_X1" />
        <port polarity="Input" name="Mul_4_X0" />
        <port polarity="Input" name="Mul_4_X3" />
        <port polarity="Input" name="Mul_4_K3" />
        <port polarity="Input" name="Mul_4_K2" />
        <port polarity="Input" name="Mul_4_K1" />
        <port polarity="Input" name="Mul_4_K0" />
        <port polarity="Output" name="Mul_4_P1" />
        <port polarity="Output" name="Mul_4_P0" />
        <port polarity="Output" name="Mul_4_P2" />
        <port polarity="Output" name="Mul_4_P3" />
        <port polarity="Output" name="Mul_4_P4" />
        <port polarity="Output" name="Mul_4_P5" />
        <port polarity="Output" name="Mul_4_P6" />
        <port polarity="Output" name="Mul_4_P7" />
        <blockdef name="Adder_4Bit">
            <timestamp>2014-10-29T11:24:39</timestamp>
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="176" y1="-752" y2="-696" x1="176" />
            <line x2="0" y1="-640" y2="-640" x1="64" />
            <line x2="0" y1="-592" y2="-592" x1="64" />
            <line x2="0" y1="-528" y2="-528" x1="64" />
            <line x2="0" y1="-464" y2="-464" x1="64" />
            <line x2="384" y1="-496" y2="-496" x1="320" />
            <line x2="0" y1="-384" y2="-384" x1="64" />
            <line x2="0" y1="-320" y2="-320" x1="64" />
            <line x2="0" y1="-240" y2="-240" x1="64" />
            <line x2="0" y1="-176" y2="-176" x1="64" />
            <line x2="384" y1="-208" y2="-208" x1="320" />
            <rect width="256" x="68" y="-708" height="776" />
            <line x2="208" y1="112" y2="56" x1="208" />
            <line x2="384" y1="-64" y2="-64" x1="320" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-48" y2="-48" x1="64" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="Adder_4Bit" name="XLXI_1">
            <blockpin signalname="XLXN_121" name="Enable" />
            <blockpin signalname="Mul_4_Y0" name="Enable_B" />
            <blockpin signalname="XLXN_118" name="Carry_IN" />
            <blockpin signalname="XLXN_105" name="ADD4_B0" />
            <blockpin signalname="Mul_4_K0" name="ADD4_A0" />
            <blockpin signalname="XLXN_109" name="ADD4_B1" />
            <blockpin signalname="Mul_4_K1" name="ADD4_A1" />
            <blockpin signalname="XLXN_113" name="ADD4_B2" />
            <blockpin signalname="Mul_4_K2" name="ADD4_A2" />
            <blockpin signalname="Mul_4_K3" name="ADD4_A3" />
            <blockpin signalname="XLXN_101" name="ADD4_B3" />
            <blockpin signalname="XLXN_3" name="S3" />
            <blockpin signalname="XLXN_2" name="S2" />
            <blockpin signalname="XLXN_1" name="S1" />
            <blockpin signalname="Mul_4_P0" name="S0" />
            <blockpin signalname="XLXN_10" name="Carry_OUT" />
        </block>
        <block symbolname="Adder_4Bit" name="XLXI_2">
            <blockpin signalname="XLXN_121" name="Enable" />
            <blockpin signalname="Mul_4_Y1" name="Enable_B" />
            <blockpin signalname="XLXN_118" name="Carry_IN" />
            <blockpin signalname="XLXN_105" name="ADD4_B0" />
            <blockpin signalname="XLXN_1" name="ADD4_A0" />
            <blockpin signalname="XLXN_109" name="ADD4_B1" />
            <blockpin signalname="XLXN_2" name="ADD4_A1" />
            <blockpin signalname="XLXN_113" name="ADD4_B2" />
            <blockpin signalname="XLXN_3" name="ADD4_A2" />
            <blockpin signalname="XLXN_10" name="ADD4_A3" />
            <blockpin signalname="XLXN_101" name="ADD4_B3" />
            <blockpin signalname="XLXN_12" name="S3" />
            <blockpin signalname="XLXN_13" name="S2" />
            <blockpin signalname="XLXN_14" name="S1" />
            <blockpin signalname="Mul_4_P1" name="S0" />
            <blockpin signalname="XLXN_11" name="Carry_OUT" />
        </block>
        <block symbolname="Adder_4Bit" name="XLXI_3">
            <blockpin signalname="XLXN_121" name="Enable" />
            <blockpin signalname="Mul_4_Y2" name="Enable_B" />
            <blockpin signalname="XLXN_118" name="Carry_IN" />
            <blockpin signalname="XLXN_105" name="ADD4_B0" />
            <blockpin signalname="XLXN_14" name="ADD4_A0" />
            <blockpin signalname="XLXN_109" name="ADD4_B1" />
            <blockpin signalname="XLXN_13" name="ADD4_A1" />
            <blockpin signalname="XLXN_113" name="ADD4_B2" />
            <blockpin signalname="XLXN_12" name="ADD4_A2" />
            <blockpin signalname="XLXN_11" name="ADD4_A3" />
            <blockpin signalname="XLXN_101" name="ADD4_B3" />
            <blockpin signalname="XLXN_16" name="S3" />
            <blockpin signalname="XLXN_17" name="S2" />
            <blockpin signalname="XLXN_18" name="S1" />
            <blockpin signalname="Mul_4_P2" name="S0" />
            <blockpin signalname="XLXN_15" name="Carry_OUT" />
        </block>
        <block symbolname="Adder_4Bit" name="XLXI_4">
            <blockpin signalname="XLXN_121" name="Enable" />
            <blockpin signalname="Mul_4_Y3" name="Enable_B" />
            <blockpin signalname="XLXN_118" name="Carry_IN" />
            <blockpin signalname="XLXN_105" name="ADD4_B0" />
            <blockpin signalname="XLXN_18" name="ADD4_A0" />
            <blockpin signalname="XLXN_109" name="ADD4_B1" />
            <blockpin signalname="XLXN_17" name="ADD4_A1" />
            <blockpin signalname="XLXN_113" name="ADD4_B2" />
            <blockpin signalname="XLXN_16" name="ADD4_A2" />
            <blockpin signalname="XLXN_15" name="ADD4_A3" />
            <blockpin signalname="XLXN_101" name="ADD4_B3" />
            <blockpin signalname="Mul_4_P6" name="S3" />
            <blockpin signalname="Mul_4_P5" name="S2" />
            <blockpin signalname="Mul_4_P4" name="S1" />
            <blockpin signalname="Mul_4_P3" name="S0" />
            <blockpin signalname="Mul_4_P7" name="Carry_OUT" />
        </block>
        <block symbolname="buf" name="XLXI_5">
            <blockpin signalname="Mul_4_X0" name="I" />
            <blockpin signalname="XLXN_105" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_6">
            <blockpin signalname="Mul_4_X1" name="I" />
            <blockpin signalname="XLXN_109" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_7">
            <blockpin signalname="Mul_4_X2" name="I" />
            <blockpin signalname="XLXN_113" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_8">
            <blockpin signalname="Mul_4_X3" name="I" />
            <blockpin signalname="XLXN_101" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_9">
            <blockpin signalname="Carry_IN" name="I" />
            <blockpin signalname="XLXN_118" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_10">
            <blockpin signalname="Enable" name="I" />
            <blockpin signalname="XLXN_121" name="O" />
        </block>
        <block symbolname="gnd" name="XLXI_11">
            <blockpin signalname="Carry_IN" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="2416" y="288" name="XLXI_1" orien="R90">
        </instance>
        <instance x="1920" y="880" name="XLXI_2" orien="R90">
        </instance>
        <instance x="1408" y="1440" name="XLXI_3" orien="R90">
        </instance>
        <instance x="864" y="2000" name="XLXI_4" orien="R90">
        </instance>
        <branch name="XLXN_1">
            <wire x2="2768" y1="704" y2="704" x1="2448" />
            <wire x2="2448" y1="704" y2="880" x1="2448" />
            <wire x2="2768" y1="672" y2="704" x1="2768" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="2624" y1="688" y2="688" x1="2304" />
            <wire x2="2304" y1="688" y2="880" x1="2304" />
            <wire x2="2624" y1="672" y2="688" x1="2624" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="2480" y1="672" y2="672" x1="2160" />
            <wire x2="2160" y1="672" y2="880" x1="2160" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="2304" y1="656" y2="656" x1="2016" />
            <wire x2="2016" y1="656" y2="880" x1="2016" />
            <wire x2="2304" y1="496" y2="656" x1="2304" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="1504" y1="1248" y2="1440" x1="1504" />
            <wire x2="1808" y1="1248" y2="1248" x1="1504" />
            <wire x2="1808" y1="1088" y2="1248" x1="1808" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="1984" y1="1264" y2="1264" x1="1648" />
            <wire x2="1648" y1="1264" y2="1440" x1="1648" />
        </branch>
        <branch name="XLXN_13">
            <wire x2="2128" y1="1280" y2="1280" x1="1792" />
            <wire x2="1792" y1="1280" y2="1440" x1="1792" />
            <wire x2="2128" y1="1264" y2="1280" x1="2128" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="2272" y1="1296" y2="1296" x1="1936" />
            <wire x2="1936" y1="1296" y2="1440" x1="1936" />
            <wire x2="2272" y1="1264" y2="1296" x1="2272" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="960" y1="1808" y2="2000" x1="960" />
            <wire x2="1296" y1="1808" y2="1808" x1="960" />
            <wire x2="1296" y1="1648" y2="1808" x1="1296" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="1472" y1="1824" y2="1824" x1="1104" />
            <wire x2="1104" y1="1824" y2="2000" x1="1104" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="1616" y1="1840" y2="1840" x1="1248" />
            <wire x2="1248" y1="1840" y2="2000" x1="1248" />
            <wire x2="1616" y1="1824" y2="1840" x1="1616" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="1760" y1="1856" y2="1856" x1="1392" />
            <wire x2="1392" y1="1856" y2="2000" x1="1392" />
            <wire x2="1760" y1="1824" y2="1856" x1="1760" />
        </branch>
        <branch name="Mul_4_Y1">
            <wire x2="2560" y1="832" y2="880" x1="2560" />
            <wire x2="2832" y1="832" y2="832" x1="2560" />
            <wire x2="2832" y1="832" y2="1504" x1="2832" />
            <wire x2="4496" y1="1504" y2="1504" x1="2832" />
        </branch>
        <branch name="Mul_4_Y0">
            <wire x2="3056" y1="240" y2="288" x1="3056" />
            <wire x2="3376" y1="240" y2="240" x1="3056" />
            <wire x2="3376" y1="240" y2="1440" x1="3376" />
            <wire x2="4496" y1="1440" y2="1440" x1="3376" />
        </branch>
        <branch name="Mul_4_Y2">
            <wire x2="2048" y1="1392" y2="1440" x1="2048" />
            <wire x2="2544" y1="1392" y2="1392" x1="2048" />
            <wire x2="2544" y1="1392" y2="1568" x1="2544" />
            <wire x2="4496" y1="1568" y2="1568" x1="2544" />
        </branch>
        <branch name="Mul_4_Y3">
            <wire x2="1504" y1="1936" y2="2000" x1="1504" />
            <wire x2="4240" y1="1936" y2="1936" x1="1504" />
            <wire x2="4240" y1="1632" y2="1936" x1="4240" />
            <wire x2="4496" y1="1632" y2="1632" x1="4240" />
        </branch>
        <instance x="368" y="208" name="XLXI_5" orien="R90" />
        <instance x="288" y="208" name="XLXI_6" orien="R90" />
        <instance x="208" y="208" name="XLXI_7" orien="R90" />
        <instance x="128" y="208" name="XLXI_8" orien="R90" />
        <branch name="Mul_4_X0">
            <wire x2="400" y1="176" y2="208" x1="400" />
        </branch>
        <iomarker fontsize="28" x="400" y="176" name="Mul_4_X0" orien="R270" />
        <branch name="Mul_4_X1">
            <wire x2="320" y1="176" y2="208" x1="320" />
        </branch>
        <iomarker fontsize="28" x="320" y="176" name="Mul_4_X1" orien="R270" />
        <branch name="Mul_4_X2">
            <wire x2="240" y1="176" y2="208" x1="240" />
        </branch>
        <iomarker fontsize="28" x="240" y="176" name="Mul_4_X2" orien="R270" />
        <branch name="Mul_4_X3">
            <wire x2="160" y1="176" y2="208" x1="160" />
        </branch>
        <iomarker fontsize="28" x="160" y="176" name="Mul_4_X3" orien="R270" />
        <branch name="Mul_4_K3">
            <wire x2="2512" y1="160" y2="288" x1="2512" />
        </branch>
        <branch name="Mul_4_K2">
            <wire x2="2656" y1="160" y2="288" x1="2656" />
        </branch>
        <branch name="Mul_4_K1">
            <wire x2="2800" y1="160" y2="288" x1="2800" />
        </branch>
        <branch name="Mul_4_K0">
            <wire x2="2944" y1="160" y2="288" x1="2944" />
        </branch>
        <iomarker fontsize="28" x="2944" y="160" name="Mul_4_K0" orien="R270" />
        <iomarker fontsize="28" x="2800" y="160" name="Mul_4_K1" orien="R270" />
        <iomarker fontsize="28" x="2656" y="160" name="Mul_4_K2" orien="R270" />
        <iomarker fontsize="28" x="2512" y="160" name="Mul_4_K3" orien="R270" />
        <branch name="Enable">
            <wire x2="4208" y1="224" y2="224" x1="4112" />
        </branch>
        <branch name="Carry_IN">
            <wire x2="4192" y1="464" y2="464" x1="4112" />
        </branch>
        <branch name="Mul_4_P0">
            <wire x2="2432" y1="2864" y2="2944" x1="2432" />
            <wire x2="2912" y1="2864" y2="2864" x1="2432" />
            <wire x2="2912" y1="672" y2="2864" x1="2912" />
        </branch>
        <branch name="Mul_4_P2">
            <wire x2="1904" y1="1824" y2="1840" x1="1904" />
            <wire x2="2304" y1="1840" y2="1840" x1="1904" />
            <wire x2="2304" y1="1840" y2="2944" x1="2304" />
        </branch>
        <branch name="Mul_4_P3">
            <wire x2="1360" y1="2384" y2="2400" x1="1360" />
            <wire x2="2240" y1="2400" y2="2400" x1="1360" />
            <wire x2="2240" y1="2400" y2="2944" x1="2240" />
        </branch>
        <branch name="Mul_4_P4">
            <wire x2="1216" y1="2384" y2="2672" x1="1216" />
            <wire x2="2176" y1="2672" y2="2672" x1="1216" />
            <wire x2="2176" y1="2672" y2="2944" x1="2176" />
        </branch>
        <branch name="Mul_4_P5">
            <wire x2="1072" y1="2384" y2="2688" x1="1072" />
            <wire x2="2112" y1="2688" y2="2688" x1="1072" />
            <wire x2="2112" y1="2688" y2="2944" x1="2112" />
        </branch>
        <branch name="Mul_4_P6">
            <wire x2="928" y1="2384" y2="2704" x1="928" />
            <wire x2="2048" y1="2704" y2="2704" x1="928" />
            <wire x2="2048" y1="2704" y2="2944" x1="2048" />
        </branch>
        <branch name="Mul_4_P7">
            <wire x2="752" y1="2224" y2="2224" x1="672" />
            <wire x2="672" y1="2224" y2="2720" x1="672" />
            <wire x2="1984" y1="2720" y2="2720" x1="672" />
            <wire x2="1984" y1="2720" y2="2944" x1="1984" />
            <wire x2="752" y1="2208" y2="2224" x1="752" />
        </branch>
        <iomarker fontsize="28" x="4496" y="1440" name="Mul_4_Y0" orien="R0" />
        <iomarker fontsize="28" x="4496" y="1504" name="Mul_4_Y1" orien="R0" />
        <iomarker fontsize="28" x="4496" y="1568" name="Mul_4_Y2" orien="R0" />
        <iomarker fontsize="28" x="4496" y="1632" name="Mul_4_Y3" orien="R0" />
        <instance x="4112" y="432" name="XLXI_9" orien="R180" />
        <instance x="4320" y="528" name="XLXI_11" orien="R270" />
        <instance x="4112" y="192" name="XLXI_10" orien="R180" />
        <iomarker fontsize="28" x="4208" y="224" name="Enable" orien="R0" />
        <iomarker fontsize="28" x="2432" y="2944" name="Mul_4_P0" orien="R90" />
        <iomarker fontsize="28" x="2304" y="2944" name="Mul_4_P2" orien="R90" />
        <iomarker fontsize="28" x="2240" y="2944" name="Mul_4_P3" orien="R90" />
        <iomarker fontsize="28" x="2176" y="2944" name="Mul_4_P4" orien="R90" />
        <iomarker fontsize="28" x="2112" y="2944" name="Mul_4_P5" orien="R90" />
        <iomarker fontsize="28" x="2048" y="2944" name="Mul_4_P6" orien="R90" />
        <iomarker fontsize="28" x="1984" y="2944" name="Mul_4_P7" orien="R90" />
        <branch name="Mul_4_P1">
            <wire x2="2368" y1="2800" y2="2944" x1="2368" />
            <wire x2="2416" y1="2800" y2="2800" x1="2368" />
            <wire x2="2416" y1="1264" y2="2800" x1="2416" />
        </branch>
        <iomarker fontsize="28" x="2368" y="2944" name="Mul_4_P1" orien="R90" />
        <branch name="XLXN_101">
            <wire x2="160" y1="432" y2="560" x1="160" />
            <wire x2="160" y1="560" y2="880" x1="160" />
            <wire x2="160" y1="880" y2="1440" x1="160" />
            <wire x2="160" y1="1440" y2="2000" x1="160" />
            <wire x2="912" y1="2000" y2="2000" x1="160" />
            <wire x2="1456" y1="1440" y2="1440" x1="160" />
            <wire x2="1968" y1="880" y2="880" x1="160" />
            <wire x2="1360" y1="560" y2="560" x1="160" />
            <wire x2="2464" y1="288" y2="288" x1="1360" />
            <wire x2="1360" y1="288" y2="560" x1="1360" />
        </branch>
        <branch name="XLXN_105">
            <wire x2="400" y1="432" y2="464" x1="400" />
            <wire x2="400" y1="464" y2="784" x1="400" />
            <wire x2="400" y1="784" y2="1344" x1="400" />
            <wire x2="400" y1="1344" y2="1872" x1="400" />
            <wire x2="1328" y1="1872" y2="1872" x1="400" />
            <wire x2="1328" y1="1872" y2="2000" x1="1328" />
            <wire x2="1872" y1="1344" y2="1344" x1="400" />
            <wire x2="1872" y1="1344" y2="1440" x1="1872" />
            <wire x2="2384" y1="784" y2="784" x1="400" />
            <wire x2="2384" y1="784" y2="880" x1="2384" />
            <wire x2="1264" y1="464" y2="464" x1="400" />
            <wire x2="2880" y1="192" y2="192" x1="1264" />
            <wire x2="2880" y1="192" y2="288" x1="2880" />
            <wire x2="1264" y1="192" y2="464" x1="1264" />
        </branch>
        <branch name="XLXN_109">
            <wire x2="320" y1="432" y2="496" x1="320" />
            <wire x2="320" y1="496" y2="816" x1="320" />
            <wire x2="320" y1="816" y2="1376" x1="320" />
            <wire x2="320" y1="1376" y2="1904" x1="320" />
            <wire x2="1184" y1="1904" y2="1904" x1="320" />
            <wire x2="1184" y1="1904" y2="2000" x1="1184" />
            <wire x2="1728" y1="1376" y2="1376" x1="320" />
            <wire x2="1728" y1="1376" y2="1440" x1="1728" />
            <wire x2="2240" y1="816" y2="816" x1="320" />
            <wire x2="2240" y1="816" y2="880" x1="2240" />
            <wire x2="1296" y1="496" y2="496" x1="320" />
            <wire x2="2736" y1="224" y2="224" x1="1296" />
            <wire x2="2736" y1="224" y2="288" x1="2736" />
            <wire x2="1296" y1="224" y2="496" x1="1296" />
        </branch>
        <branch name="XLXN_113">
            <wire x2="240" y1="432" y2="528" x1="240" />
            <wire x2="240" y1="528" y2="848" x1="240" />
            <wire x2="240" y1="848" y2="1408" x1="240" />
            <wire x2="240" y1="1408" y2="1952" x1="240" />
            <wire x2="1040" y1="1952" y2="1952" x1="240" />
            <wire x2="1040" y1="1952" y2="2000" x1="1040" />
            <wire x2="1584" y1="1408" y2="1408" x1="240" />
            <wire x2="1584" y1="1408" y2="1440" x1="1584" />
            <wire x2="2096" y1="848" y2="848" x1="240" />
            <wire x2="2096" y1="848" y2="880" x1="2096" />
            <wire x2="1328" y1="528" y2="528" x1="240" />
            <wire x2="2592" y1="256" y2="256" x1="1328" />
            <wire x2="2592" y1="256" y2="288" x1="2592" />
            <wire x2="1328" y1="256" y2="528" x1="1328" />
        </branch>
        <branch name="XLXN_118">
            <wire x2="3856" y1="2176" y2="2176" x1="1616" />
            <wire x2="3856" y1="1616" y2="1616" x1="2160" />
            <wire x2="3856" y1="1616" y2="2176" x1="3856" />
            <wire x2="3856" y1="1056" y2="1056" x1="2672" />
            <wire x2="3856" y1="1056" y2="1616" x1="3856" />
            <wire x2="3856" y1="464" y2="464" x1="3168" />
            <wire x2="3888" y1="464" y2="464" x1="3856" />
            <wire x2="3856" y1="464" y2="1056" x1="3856" />
        </branch>
        <branch name="XLXN_121">
            <wire x2="1456" y1="1904" y2="2000" x1="1456" />
            <wire x2="3728" y1="1904" y2="1904" x1="1456" />
            <wire x2="2000" y1="1360" y2="1440" x1="2000" />
            <wire x2="3728" y1="1360" y2="1360" x1="2000" />
            <wire x2="3728" y1="1360" y2="1904" x1="3728" />
            <wire x2="2512" y1="800" y2="880" x1="2512" />
            <wire x2="3728" y1="800" y2="800" x1="2512" />
            <wire x2="3728" y1="800" y2="1360" x1="3728" />
            <wire x2="3008" y1="224" y2="288" x1="3008" />
            <wire x2="3728" y1="224" y2="224" x1="3008" />
            <wire x2="3888" y1="224" y2="224" x1="3728" />
            <wire x2="3728" y1="224" y2="800" x1="3728" />
        </branch>
    </sheet>
</drawing>