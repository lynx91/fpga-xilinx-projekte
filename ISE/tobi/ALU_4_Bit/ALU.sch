<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="Carry_IN" />
        <signal name="Carry_OUT" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16">
        </signal>
        <signal name="IN_A" />
        <signal name="EN_A" />
        <signal name="IN_B" />
        <signal name="EN_B" />
        <signal name="INVA" />
        <signal name="OUTPUT" />
        <signal name="IN_F1" />
        <signal name="IN_F0" />
        <port polarity="Input" name="Carry_IN" />
        <port polarity="Output" name="Carry_OUT" />
        <port polarity="Input" name="IN_A" />
        <port polarity="Input" name="EN_A" />
        <port polarity="Input" name="IN_B" />
        <port polarity="Input" name="EN_B" />
        <port polarity="Input" name="INVA" />
        <port polarity="Output" name="OUTPUT" />
        <port polarity="Input" name="IN_F1" />
        <port polarity="Input" name="IN_F0" />
        <blockdef name="Logical_Unit">
            <timestamp>2014-11-6T8:22:21</timestamp>
            <line x2="496" y1="-80" y2="-20" x1="496" />
            <line x2="400" y1="-16" y2="-80" x1="400" />
            <line x2="304" y1="-16" y2="-76" x1="304" />
            <rect width="496" x="60" y="-324" height="272" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="192" y1="-80" y2="-20" x1="192" />
            <line x2="96" y1="-80" y2="-20" x1="96" />
            <line x2="608" y1="-288" y2="-288" x1="544" />
            <line x2="608" y1="-224" y2="-224" x1="544" />
            <line x2="608" y1="-160" y2="-160" x1="544" />
        </blockdef>
        <blockdef name="Decoder">
            <timestamp>2014-11-6T7:57:44</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Adder_1Bit">
            <timestamp>2014-11-6T7:32:15</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <line x2="192" y1="48" y2="-12" x1="192" />
            <line x2="192" y1="-312" y2="-384" x1="192" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="0" y1="-192" y2="-192" x1="64" />
            <line x2="0" y1="-144" y2="-144" x1="64" />
            <line x2="384" y1="-176" y2="-176" x1="320" />
            <line x2="0" y1="-240" y2="-240" x1="64" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="Logical_Unit" name="XLXI_3">
            <blockpin signalname="XLXN_2" name="IN_A" />
            <blockpin signalname="XLXN_3" name="IN_B" />
            <blockpin signalname="XLXN_15" name="OUT_A" />
            <blockpin signalname="XLXN_14" name="OUT_B" />
            <blockpin signalname="XLXN_8" name="OUT_0" />
            <blockpin signalname="XLXN_9" name="OUT_1" />
            <blockpin signalname="XLXN_10" name="OUT_2" />
            <blockpin signalname="XLXN_5" name="IN_EN0" />
            <blockpin signalname="XLXN_6" name="IN_EN1" />
            <blockpin signalname="XLXN_7" name="IN_EN2" />
        </block>
        <block symbolname="Decoder" name="XLXI_4">
            <blockpin signalname="IN_F1" name="IN_F1" />
            <blockpin signalname="IN_F0" name="IN_F0" />
            <blockpin signalname="XLXN_5" name="OUT_EN0" />
            <blockpin signalname="XLXN_6" name="OUT_EN1" />
            <blockpin signalname="XLXN_7" name="OUT_EN2" />
            <blockpin signalname="XLXN_4" name="OUT_EN3" />
        </block>
        <block symbolname="Adder_1Bit" name="XLXI_5">
            <blockpin signalname="XLXN_15" name="In_A" />
            <blockpin signalname="XLXN_14" name="IN_B" />
            <blockpin signalname="XLXN_16" name="Enable_B" />
            <blockpin signalname="XLXN_4" name="Enable" />
            <blockpin signalname="Carry_IN" name="Carry_IN" />
            <blockpin signalname="XLXN_11" name="SUM" />
            <blockpin signalname="Carry_OUT" name="Carry_OUT" />
        </block>
        <block symbolname="xor2" name="XLXI_6">
            <blockpin signalname="XLXN_1" name="I0" />
            <blockpin signalname="INVA" name="I1" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="EN_A" name="I0" />
            <blockpin signalname="IN_A" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_8">
            <blockpin signalname="EN_B" name="I0" />
            <blockpin signalname="IN_B" name="I1" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_9">
            <blockpin signalname="XLXN_11" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="XLXN_9" name="I2" />
            <blockpin signalname="XLXN_8" name="I3" />
            <blockpin signalname="OUTPUT" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_12">
            <blockpin signalname="XLXN_16" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="976" y="608" name="XLXI_3" orien="R0">
        </instance>
        <instance x="576" y="1136" name="XLXI_4" orien="R0">
        </instance>
        <instance x="1584" y="1136" name="XLXI_5" orien="R0">
        </instance>
        <instance x="576" y="416" name="XLXI_6" orien="R0" />
        <instance x="240" y="448" name="XLXI_7" orien="R0" />
        <instance x="240" y="608" name="XLXI_8" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="576" y1="352" y2="352" x1="496" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="976" y1="320" y2="320" x1="832" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="736" y1="512" y2="512" x1="496" />
            <wire x2="736" y1="384" y2="512" x1="736" />
            <wire x2="976" y1="384" y2="384" x1="736" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1264" y1="1104" y2="1104" x1="960" />
            <wire x2="1264" y1="1056" y2="1104" x1="1264" />
            <wire x2="1584" y1="1056" y2="1056" x1="1264" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1280" y1="912" y2="912" x1="960" />
            <wire x2="1280" y1="592" y2="912" x1="1280" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1376" y1="976" y2="976" x1="960" />
            <wire x2="1376" y1="592" y2="976" x1="1376" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1472" y1="1040" y2="1040" x1="960" />
            <wire x2="1472" y1="592" y2="1040" x1="1472" />
        </branch>
        <instance x="2288" y="576" name="XLXI_9" orien="R0" />
        <branch name="XLXN_8">
            <wire x2="2288" y1="320" y2="320" x1="1584" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="2288" y1="384" y2="384" x1="1584" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="2288" y1="448" y2="448" x1="1584" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="2288" y1="960" y2="960" x1="1968" />
            <wire x2="2288" y1="512" y2="960" x1="2288" />
        </branch>
        <branch name="Carry_IN">
            <wire x2="1776" y1="176" y2="752" x1="1776" />
        </branch>
        <branch name="Carry_OUT">
            <wire x2="1776" y1="1184" y2="1280" x1="1776" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="1168" y1="592" y2="944" x1="1168" />
            <wire x2="1584" y1="944" y2="944" x1="1168" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="1072" y1="592" y2="992" x1="1072" />
            <wire x2="1584" y1="992" y2="992" x1="1072" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="1568" y1="608" y2="896" x1="1568" />
            <wire x2="1584" y1="896" y2="896" x1="1568" />
            <wire x2="1648" y1="608" y2="608" x1="1568" />
            <wire x2="1648" y1="208" y2="608" x1="1648" />
        </branch>
        <branch name="IN_A">
            <wire x2="240" y1="320" y2="320" x1="176" />
        </branch>
        <branch name="EN_A">
            <wire x2="240" y1="384" y2="384" x1="176" />
        </branch>
        <branch name="IN_B">
            <wire x2="240" y1="480" y2="480" x1="176" />
        </branch>
        <branch name="EN_B">
            <wire x2="240" y1="544" y2="544" x1="176" />
        </branch>
        <branch name="INVA">
            <wire x2="464" y1="256" y2="256" x1="176" />
            <wire x2="464" y1="256" y2="288" x1="464" />
            <wire x2="576" y1="288" y2="288" x1="464" />
        </branch>
        <branch name="OUTPUT">
            <wire x2="2576" y1="416" y2="416" x1="2544" />
        </branch>
        <iomarker fontsize="28" x="2576" y="416" name="OUTPUT" orien="R0" />
        <iomarker fontsize="28" x="1776" y="1280" name="Carry_OUT" orien="R90" />
        <iomarker fontsize="28" x="1776" y="176" name="Carry_IN" orien="R270" />
        <iomarker fontsize="28" x="176" y="256" name="INVA" orien="R180" />
        <iomarker fontsize="28" x="176" y="320" name="IN_A" orien="R180" />
        <iomarker fontsize="28" x="176" y="384" name="EN_A" orien="R180" />
        <iomarker fontsize="28" x="176" y="480" name="IN_B" orien="R180" />
        <iomarker fontsize="28" x="176" y="544" name="EN_B" orien="R180" />
        <branch name="IN_F1">
            <wire x2="576" y1="912" y2="912" x1="544" />
        </branch>
        <iomarker fontsize="28" x="544" y="912" name="IN_F1" orien="R180" />
        <branch name="IN_F0">
            <wire x2="576" y1="1104" y2="1104" x1="544" />
        </branch>
        <iomarker fontsize="28" x="544" y="1104" name="IN_F0" orien="R180" />
        <instance x="1584" y="208" name="XLXI_12" orien="R0" />
    </sheet>
</drawing>