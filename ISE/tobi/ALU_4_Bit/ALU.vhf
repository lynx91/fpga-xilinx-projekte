--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : ALU.vhf
-- /___/   /\     Timestamp : 11/06/2014 09:41:24
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl D:/Programme/Xilinx/Projekte/ALU_4_Bit/ALU.vhf -w D:/Programme/Xilinx/Projekte/ALU_4_Bit/ALU.sch
--Design Name: ALU
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Decoder_MUSER_ALU is
   port ( IN_F0   : in    std_logic; 
          IN_F1   : in    std_logic; 
          OUT_EN0 : out   std_logic; 
          OUT_EN1 : out   std_logic; 
          OUT_EN2 : out   std_logic; 
          OUT_EN3 : out   std_logic);
end Decoder_MUSER_ALU;

architecture BEHAVIORAL of Decoder_MUSER_ALU is
   attribute BOX_TYPE   : string ;
   signal XLXN_1  : std_logic;
   signal XLXN_2  : std_logic;
   signal XLXN_3  : std_logic;
   signal XLXN_4  : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>XLXN_1,
                I1=>XLXN_2,
                O=>OUT_EN0);
   
   XLXI_2 : AND2
      port map (I0=>IN_F1,
                I1=>XLXN_3,
                O=>OUT_EN1);
   
   XLXI_3 : AND2
      port map (I0=>XLXN_4,
                I1=>IN_F0,
                O=>OUT_EN2);
   
   XLXI_4 : AND2
      port map (I0=>IN_F1,
                I1=>IN_F0,
                O=>OUT_EN3);
   
   XLXI_5 : INV
      port map (I=>IN_F0,
                O=>XLXN_2);
   
   XLXI_6 : INV
      port map (I=>IN_F1,
                O=>XLXN_1);
   
   XLXI_7 : INV
      port map (I=>IN_F0,
                O=>XLXN_3);
   
   XLXI_8 : INV
      port map (I=>IN_F1,
                O=>XLXN_4);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Logical_Unit_MUSER_ALU is
   port ( IN_A   : in    std_logic; 
          IN_B   : in    std_logic; 
          IN_EN0 : in    std_logic; 
          IN_EN1 : in    std_logic; 
          IN_EN2 : in    std_logic; 
          OUT_A  : out   std_logic; 
          OUT_B  : out   std_logic; 
          OUT_0  : out   std_logic; 
          OUT_1  : out   std_logic; 
          OUT_2  : out   std_logic);
end Logical_Unit_MUSER_ALU;

architecture BEHAVIORAL of Logical_Unit_MUSER_ALU is
   attribute BOX_TYPE   : string ;
   signal XLXN_1 : std_logic;
   signal XLXN_2 : std_logic;
   signal XLXN_3 : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>IN_B,
                I1=>IN_A,
                O=>XLXN_1);
   
   XLXI_2 : AND2
      port map (I0=>IN_EN0,
                I1=>XLXN_1,
                O=>OUT_0);
   
   XLXI_3 : AND2
      port map (I0=>IN_EN1,
                I1=>XLXN_2,
                O=>OUT_1);
   
   XLXI_4 : AND2
      port map (I0=>IN_EN2,
                I1=>XLXN_3,
                O=>OUT_2);
   
   XLXI_5 : OR2
      port map (I0=>IN_B,
                I1=>IN_A,
                O=>XLXN_2);
   
   XLXI_6 : INV
      port map (I=>IN_B,
                O=>XLXN_3);
   
   XLXI_7 : BUF
      port map (I=>IN_A,
                O=>OUT_A);
   
   XLXI_8 : BUF
      port map (I=>IN_B,
                O=>OUT_B);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ALU is
   port ( Carry_IN  : in    std_logic; 
          EN_A      : in    std_logic; 
          EN_B      : in    std_logic; 
          INVA      : in    std_logic; 
          IN_A      : in    std_logic; 
          IN_B      : in    std_logic; 
          IN_F0     : in    std_logic; 
          IN_F1     : in    std_logic; 
          Carry_OUT : out   std_logic; 
          OUTPUT    : out   std_logic);
end ALU;

architecture BEHAVIORAL of ALU is
   attribute BOX_TYPE   : string ;
   signal XLXN_1    : std_logic;
   signal XLXN_2    : std_logic;
   signal XLXN_3    : std_logic;
   signal XLXN_4    : std_logic;
   signal XLXN_5    : std_logic;
   signal XLXN_6    : std_logic;
   signal XLXN_7    : std_logic;
   signal XLXN_8    : std_logic;
   signal XLXN_9    : std_logic;
   signal XLXN_10   : std_logic;
   signal XLXN_11   : std_logic;
   signal XLXN_14   : std_logic;
   signal XLXN_15   : std_logic;
   signal XLXN_16   : std_logic;
   component Logical_Unit_MUSER_ALU
      port ( IN_A   : in    std_logic; 
             IN_B   : in    std_logic; 
             OUT_A  : out   std_logic; 
             OUT_B  : out   std_logic; 
             OUT_0  : out   std_logic; 
             OUT_1  : out   std_logic; 
             OUT_2  : out   std_logic; 
             IN_EN0 : in    std_logic; 
             IN_EN1 : in    std_logic; 
             IN_EN2 : in    std_logic);
   end component;
   
   component Decoder_MUSER_ALU
      port ( IN_F1   : in    std_logic; 
             IN_F0   : in    std_logic; 
             OUT_EN0 : out   std_logic; 
             OUT_EN1 : out   std_logic; 
             OUT_EN2 : out   std_logic; 
             OUT_EN3 : out   std_logic);
   end component;
   
   component Adder_1Bit
      port ( In_A      : in    std_logic; 
             IN_B      : in    std_logic; 
             Enable_B  : in    std_logic; 
             Enable    : in    std_logic; 
             Carry_IN  : in    std_logic; 
             SUM       : out   std_logic; 
             Carry_OUT : out   std_logic);
   end component;
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
begin
   XLXI_3 : Logical_Unit_MUSER_ALU
      port map (IN_A=>XLXN_2,
                IN_B=>XLXN_3,
                IN_EN0=>XLXN_5,
                IN_EN1=>XLXN_6,
                IN_EN2=>XLXN_7,
                OUT_A=>XLXN_15,
                OUT_B=>XLXN_14,
                OUT_0=>XLXN_8,
                OUT_1=>XLXN_9,
                OUT_2=>XLXN_10);
   
   XLXI_4 : Decoder_MUSER_ALU
      port map (IN_F0=>IN_F0,
                IN_F1=>IN_F1,
                OUT_EN0=>XLXN_5,
                OUT_EN1=>XLXN_6,
                OUT_EN2=>XLXN_7,
                OUT_EN3=>XLXN_4);
   
   XLXI_5 : Adder_1Bit
      port map (Carry_IN=>Carry_IN,
                Enable=>XLXN_4,
                Enable_B=>XLXN_16,
                In_A=>XLXN_15,
                IN_B=>XLXN_14,
                Carry_OUT=>Carry_OUT,
                SUM=>XLXN_11);
   
   XLXI_6 : XOR2
      port map (I0=>XLXN_1,
                I1=>INVA,
                O=>XLXN_2);
   
   XLXI_7 : AND2
      port map (I0=>EN_A,
                I1=>IN_A,
                O=>XLXN_1);
   
   XLXI_8 : AND2
      port map (I0=>EN_B,
                I1=>IN_B,
                O=>XLXN_3);
   
   XLXI_9 : OR4
      port map (I0=>XLXN_11,
                I1=>XLXN_10,
                I2=>XLXN_9,
                I3=>XLXN_8,
                O=>OUTPUT);
   
   XLXI_12 : VCC
      port map (P=>XLXN_16);
   
end BEHAVIORAL;


