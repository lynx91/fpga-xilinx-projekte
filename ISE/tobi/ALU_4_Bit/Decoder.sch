<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="IN_F1" />
        <signal name="OUT_EN0" />
        <signal name="OUT_EN1" />
        <signal name="OUT_EN2" />
        <signal name="OUT_EN3" />
        <signal name="IN_F0" />
        <port polarity="Input" name="IN_F1" />
        <port polarity="Output" name="OUT_EN0" />
        <port polarity="Output" name="OUT_EN1" />
        <port polarity="Output" name="OUT_EN2" />
        <port polarity="Output" name="OUT_EN3" />
        <port polarity="Input" name="IN_F0" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="XLXN_1" name="I0" />
            <blockpin signalname="XLXN_2" name="I1" />
            <blockpin signalname="OUT_EN0" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="IN_F1" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="OUT_EN1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="XLXN_4" name="I0" />
            <blockpin signalname="IN_F0" name="I1" />
            <blockpin signalname="OUT_EN2" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="IN_F1" name="I0" />
            <blockpin signalname="IN_F0" name="I1" />
            <blockpin signalname="OUT_EN3" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="IN_F0" name="I" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="IN_F1" name="I" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_7">
            <blockpin signalname="IN_F0" name="I" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_8">
            <blockpin signalname="IN_F1" name="I" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="800" y="304" name="XLXI_1" orien="R0" />
        <instance x="800" y="464" name="XLXI_2" orien="R0" />
        <instance x="800" y="624" name="XLXI_3" orien="R0" />
        <instance x="800" y="784" name="XLXI_4" orien="R0" />
        <instance x="400" y="208" name="XLXI_5" orien="R0" />
        <instance x="528" y="272" name="XLXI_6" orien="R0" />
        <instance x="528" y="368" name="XLXI_7" orien="R0" />
        <instance x="528" y="592" name="XLXI_8" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="800" y1="240" y2="240" x1="752" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="800" y1="176" y2="176" x1="624" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="800" y1="336" y2="336" x1="752" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="800" y1="560" y2="560" x1="752" />
        </branch>
        <branch name="IN_F1">
            <wire x2="432" y1="560" y2="560" x1="304" />
            <wire x2="528" y1="560" y2="560" x1="432" />
            <wire x2="432" y1="560" y2="720" x1="432" />
            <wire x2="800" y1="720" y2="720" x1="432" />
            <wire x2="528" y1="240" y2="240" x1="432" />
            <wire x2="432" y1="240" y2="400" x1="432" />
            <wire x2="800" y1="400" y2="400" x1="432" />
            <wire x2="432" y1="400" y2="560" x1="432" />
        </branch>
        <branch name="OUT_EN0">
            <wire x2="1088" y1="208" y2="208" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="1088" y="208" name="OUT_EN0" orien="R0" />
        <branch name="OUT_EN1">
            <wire x2="1088" y1="368" y2="368" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="1088" y="368" name="OUT_EN1" orien="R0" />
        <branch name="OUT_EN2">
            <wire x2="1088" y1="528" y2="528" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="1088" y="528" name="OUT_EN2" orien="R0" />
        <branch name="OUT_EN3">
            <wire x2="1088" y1="688" y2="688" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="1088" y="688" name="OUT_EN3" orien="R0" />
        <branch name="IN_F0">
            <wire x2="400" y1="336" y2="336" x1="304" />
            <wire x2="400" y1="336" y2="496" x1="400" />
            <wire x2="800" y1="496" y2="496" x1="400" />
            <wire x2="400" y1="496" y2="656" x1="400" />
            <wire x2="800" y1="656" y2="656" x1="400" />
            <wire x2="528" y1="336" y2="336" x1="400" />
            <wire x2="400" y1="176" y2="336" x1="400" />
        </branch>
        <iomarker fontsize="28" x="304" y="336" name="IN_F0" orien="R180" />
        <iomarker fontsize="28" x="304" y="560" name="IN_F1" orien="R180" />
    </sheet>
</drawing>