--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : Decoder.vhf
-- /___/   /\     Timestamp : 11/06/2014 09:41:24
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl D:/Programme/Xilinx/Projekte/ALU_4_Bit/Decoder.vhf -w D:/Programme/Xilinx/Projekte/ALU_4_Bit/Decoder.sch
--Design Name: Decoder
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Decoder is
   port ( IN_F0   : in    std_logic; 
          IN_F1   : in    std_logic; 
          OUT_EN0 : out   std_logic; 
          OUT_EN1 : out   std_logic; 
          OUT_EN2 : out   std_logic; 
          OUT_EN3 : out   std_logic);
end Decoder;

architecture BEHAVIORAL of Decoder is
   attribute BOX_TYPE   : string ;
   signal XLXN_1  : std_logic;
   signal XLXN_2  : std_logic;
   signal XLXN_3  : std_logic;
   signal XLXN_4  : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>XLXN_1,
                I1=>XLXN_2,
                O=>OUT_EN0);
   
   XLXI_2 : AND2
      port map (I0=>IN_F1,
                I1=>XLXN_3,
                O=>OUT_EN1);
   
   XLXI_3 : AND2
      port map (I0=>XLXN_4,
                I1=>IN_F0,
                O=>OUT_EN2);
   
   XLXI_4 : AND2
      port map (I0=>IN_F1,
                I1=>IN_F0,
                O=>OUT_EN3);
   
   XLXI_5 : INV
      port map (I=>IN_F0,
                O=>XLXN_2);
   
   XLXI_6 : INV
      port map (I=>IN_F1,
                O=>XLXN_1);
   
   XLXI_7 : INV
      port map (I=>IN_F0,
                O=>XLXN_3);
   
   XLXI_8 : INV
      port map (I=>IN_F1,
                O=>XLXN_4);
   
end BEHAVIORAL;


