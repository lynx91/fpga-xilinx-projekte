--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : Logical_Unit.vhf
-- /___/   /\     Timestamp : 11/06/2014 09:41:24
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl D:/Programme/Xilinx/Projekte/ALU_4_Bit/Logical_Unit.vhf -w D:/Programme/Xilinx/Projekte/ALU_4_Bit/Logical_Unit.sch
--Design Name: Logical_Unit
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Logical_Unit is
   port ( IN_A   : in    std_logic; 
          IN_B   : in    std_logic; 
          IN_EN0 : in    std_logic; 
          IN_EN1 : in    std_logic; 
          IN_EN2 : in    std_logic; 
          OUT_A  : out   std_logic; 
          OUT_B  : out   std_logic; 
          OUT_0  : out   std_logic; 
          OUT_1  : out   std_logic; 
          OUT_2  : out   std_logic);
end Logical_Unit;

architecture BEHAVIORAL of Logical_Unit is
   attribute BOX_TYPE   : string ;
   signal XLXN_1 : std_logic;
   signal XLXN_2 : std_logic;
   signal XLXN_3 : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>IN_B,
                I1=>IN_A,
                O=>XLXN_1);
   
   XLXI_2 : AND2
      port map (I0=>IN_EN0,
                I1=>XLXN_1,
                O=>OUT_0);
   
   XLXI_3 : AND2
      port map (I0=>IN_EN1,
                I1=>XLXN_2,
                O=>OUT_1);
   
   XLXI_4 : AND2
      port map (I0=>IN_EN2,
                I1=>XLXN_3,
                O=>OUT_2);
   
   XLXI_5 : OR2
      port map (I0=>IN_B,
                I1=>IN_A,
                O=>XLXN_2);
   
   XLXI_6 : INV
      port map (I=>IN_B,
                O=>XLXN_3);
   
   XLXI_7 : BUF
      port map (I=>IN_A,
                O=>OUT_A);
   
   XLXI_8 : BUF
      port map (I=>IN_B,
                O=>OUT_B);
   
end BEHAVIORAL;


