<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="ALU_ADDMUL_IN_A0" />
        <signal name="ALU_ADDMUL_IN_A1" />
        <signal name="ALU_ADDMUL_IN_A2" />
        <signal name="ALU_ADDMUL_IN_A3" />
        <signal name="ALU_ADDMUL_IN_B0" />
        <signal name="ALU_ADDMUL_IN_B1" />
        <signal name="ALU_ADDMUL_IN_B2" />
        <signal name="ALU_ADDMUL_IN_B3" />
        <signal name="ALU_ADDMUL_IN_F0" />
        <signal name="ALU_ADDMUL_IN_F1" />
        <signal name="ALU_ADDMUL_IN_F2" />
        <signal name="ALU_ADDMUL_IN_C" />
        <signal name="ALU_ADDMUL_OUT_R7" />
        <signal name="ALU_ADDMUL_OUT_R6" />
        <signal name="ALU_ADDMUL_OUT_R5" />
        <signal name="XLXN_21" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="ALU_ADDMUL_OUT_R0" />
        <signal name="ALU_ADDMUL_OUT_R1" />
        <signal name="ALU_ADDMUL_OUT_R2" />
        <signal name="ALU_ADDMUL_OUT_R3" />
        <signal name="ALU_ADDMUL_OUT_R4" />
        <signal name="XLXN_36" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <port polarity="Input" name="ALU_ADDMUL_IN_A0" />
        <port polarity="Input" name="ALU_ADDMUL_IN_A1" />
        <port polarity="Input" name="ALU_ADDMUL_IN_A2" />
        <port polarity="Input" name="ALU_ADDMUL_IN_A3" />
        <port polarity="Input" name="ALU_ADDMUL_IN_B0" />
        <port polarity="Input" name="ALU_ADDMUL_IN_B1" />
        <port polarity="Input" name="ALU_ADDMUL_IN_B2" />
        <port polarity="Input" name="ALU_ADDMUL_IN_B3" />
        <port polarity="Input" name="ALU_ADDMUL_IN_F0" />
        <port polarity="Input" name="ALU_ADDMUL_IN_F1" />
        <port polarity="Input" name="ALU_ADDMUL_IN_F2" />
        <port polarity="Input" name="ALU_ADDMUL_IN_C" />
        <port polarity="Output" name="ALU_ADDMUL_OUT_R7" />
        <port polarity="Output" name="ALU_ADDMUL_OUT_R6" />
        <port polarity="Output" name="ALU_ADDMUL_OUT_R5" />
        <port polarity="Output" name="ALU_ADDMUL_OUT_R0" />
        <port polarity="Output" name="ALU_ADDMUL_OUT_R1" />
        <port polarity="Output" name="ALU_ADDMUL_OUT_R2" />
        <port polarity="Output" name="ALU_ADDMUL_OUT_R3" />
        <port polarity="Output" name="ALU_ADDMUL_OUT_R4" />
        <blockdef name="ALU_4Bit">
            <timestamp>2014-11-6T11:51:40</timestamp>
            <rect width="320" x="64" y="-832" height="832" />
            <line x2="0" y1="-800" y2="-800" x1="64" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="448" y1="-800" y2="-800" x1="384" />
            <line x2="448" y1="-608" y2="-608" x1="384" />
            <line x2="448" y1="-416" y2="-416" x1="384" />
            <line x2="448" y1="-224" y2="-224" x1="384" />
            <line x2="448" y1="-32" y2="-32" x1="384" />
        </blockdef>
        <blockdef name="Multiplizierer_4Bit">
            <timestamp>2014-11-6T12:3:41</timestamp>
            <rect width="256" x="64" y="-832" height="832" />
            <line x2="0" y1="-800" y2="-800" x1="64" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-800" y2="-800" x1="320" />
            <line x2="384" y1="-704" y2="-704" x1="320" />
            <line x2="384" y1="-608" y2="-608" x1="320" />
            <line x2="384" y1="-512" y2="-512" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-320" y2="-320" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-128" y2="-128" x1="320" />
        </blockdef>
        <blockdef name="Decoder">
            <timestamp>2014-11-6T12:4:57</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-256" height="320" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="ALU_4Bit" name="XLXI_1">
            <blockpin signalname="ALU_ADDMUL_IN_C" name="ALU_4_IN_C" />
            <blockpin signalname="ALU_ADDMUL_IN_A0" name="ALU_4_IN_A0" />
            <blockpin signalname="ALU_ADDMUL_IN_B0" name="ALU_4_IN_B0" />
            <blockpin signalname="ALU_ADDMUL_IN_A1" name="ALU_4_IN_A1" />
            <blockpin signalname="ALU_ADDMUL_IN_B1" name="ALU_4_IN_B1" />
            <blockpin signalname="ALU_ADDMUL_IN_A2" name="ALU_4_IN_A2" />
            <blockpin signalname="ALU_ADDMUL_IN_B2" name="ALU_4_IN_B2" />
            <blockpin signalname="ALU_ADDMUL_IN_A3" name="ALU_4_IN_A3" />
            <blockpin signalname="ALU_ADDMUL_IN_B3" name="ALU_4_IN_B3" />
            <blockpin signalname="XLXN_1" name="ALU_4_IN_EN0" />
            <blockpin signalname="XLXN_2" name="ALU_4_IN_EN1" />
            <blockpin signalname="XLXN_3" name="ALU_4_IN_EN2" />
            <blockpin signalname="XLXN_4" name="ALU_4_IN_EN3" />
            <blockpin signalname="XLXN_21" name="ALU_4_OUT0" />
            <blockpin signalname="XLXN_22" name="ALU_4_OUT1" />
            <blockpin signalname="XLXN_23" name="ALU_4_OUT2" />
            <blockpin signalname="XLXN_24" name="ALU_4_OUT3" />
            <blockpin signalname="XLXN_25" name="ALU_4_OUTC" />
        </block>
        <block symbolname="Multiplizierer_4Bit" name="XLXI_2">
            <blockpin signalname="ALU_ADDMUL_IN_B0" name="MUL_4_Y0" />
            <blockpin signalname="ALU_ADDMUL_IN_A0" name="MUL_4_X0" />
            <blockpin signalname="ALU_ADDMUL_IN_A1" name="MUL_4_X1" />
            <blockpin signalname="ALU_ADDMUL_IN_A2" name="MUL_4_X2" />
            <blockpin signalname="ALU_ADDMUL_IN_A3" name="MUL_4_X3" />
            <blockpin signalname="XLXN_5" name="MUL_4_En" />
            <blockpin signalname="XLXN_36" name="MUL_4_K3" />
            <blockpin signalname="XLXN_37" name="MUL_4_K2" />
            <blockpin signalname="XLXN_38" name="MUL_4_K1" />
            <blockpin signalname="XLXN_39" name="MUL_4_K0" />
            <blockpin signalname="ALU_ADDMUL_IN_B1" name="MUL_4_Y1" />
            <blockpin signalname="ALU_ADDMUL_IN_B2" name="MUL_4_Y2" />
            <blockpin signalname="ALU_ADDMUL_IN_B3" name="MUL_4_Y3" />
            <blockpin signalname="XLXN_30" name="MUL_4_P0" />
            <blockpin signalname="XLXN_29" name="MUL_4_P1" />
            <blockpin signalname="XLXN_28" name="MUL_4_P2" />
            <blockpin signalname="XLXN_27" name="MUL_4_P3" />
            <blockpin signalname="XLXN_26" name="MUL_4_P4" />
            <blockpin signalname="ALU_ADDMUL_OUT_R5" name="MUL_4_P5" />
            <blockpin signalname="ALU_ADDMUL_OUT_R6" name="MUL_4_P6" />
            <blockpin signalname="ALU_ADDMUL_OUT_R7" name="MUL_4_P7" />
        </block>
        <block symbolname="Decoder" name="XLXI_4">
            <blockpin signalname="XLXN_1" name="OUT_EN0" />
            <blockpin signalname="XLXN_2" name="OUT_EN1" />
            <blockpin signalname="XLXN_3" name="OUT_EN2" />
            <blockpin signalname="XLXN_4" name="OUT_EN3" />
            <blockpin signalname="ALU_ADDMUL_IN_F2" name="IN_F2" />
            <blockpin signalname="XLXN_5" name="OUT_EN4" />
            <blockpin signalname="ALU_ADDMUL_IN_F0" name="IN_F0" />
            <blockpin signalname="ALU_ADDMUL_IN_F1" name="IN_F1" />
        </block>
        <block symbolname="or2" name="XLXI_5">
            <blockpin signalname="XLXN_30" name="I0" />
            <blockpin signalname="XLXN_21" name="I1" />
            <blockpin signalname="ALU_ADDMUL_OUT_R0" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_6">
            <blockpin signalname="XLXN_29" name="I0" />
            <blockpin signalname="XLXN_22" name="I1" />
            <blockpin signalname="ALU_ADDMUL_OUT_R1" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_7">
            <blockpin signalname="XLXN_28" name="I0" />
            <blockpin signalname="XLXN_23" name="I1" />
            <blockpin signalname="ALU_ADDMUL_OUT_R2" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_8">
            <blockpin signalname="XLXN_27" name="I0" />
            <blockpin signalname="XLXN_24" name="I1" />
            <blockpin signalname="ALU_ADDMUL_OUT_R3" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_9">
            <blockpin signalname="XLXN_26" name="I0" />
            <blockpin signalname="XLXN_25" name="I1" />
            <blockpin signalname="ALU_ADDMUL_OUT_R4" name="O" />
        </block>
        <block symbolname="gnd" name="XLXI_10">
            <blockpin signalname="XLXN_36" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_11">
            <blockpin signalname="XLXN_37" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_12">
            <blockpin signalname="XLXN_38" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_13">
            <blockpin signalname="XLXN_39" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1472" y="1184" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1488" y="2144" name="XLXI_2" orien="R0">
        </instance>
        <instance x="624" y="1296" name="XLXI_4" orien="R0">
        </instance>
        <branch name="XLXN_1">
            <wire x2="1232" y1="1072" y2="1072" x1="1008" />
            <wire x2="1232" y1="960" y2="1072" x1="1232" />
            <wire x2="1472" y1="960" y2="960" x1="1232" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="1248" y1="1136" y2="1136" x1="1008" />
            <wire x2="1248" y1="1024" y2="1136" x1="1248" />
            <wire x2="1472" y1="1024" y2="1024" x1="1248" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1264" y1="1200" y2="1200" x1="1008" />
            <wire x2="1264" y1="1088" y2="1200" x1="1264" />
            <wire x2="1472" y1="1088" y2="1088" x1="1264" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1280" y1="1264" y2="1264" x1="1008" />
            <wire x2="1280" y1="1152" y2="1264" x1="1280" />
            <wire x2="1472" y1="1152" y2="1152" x1="1280" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1248" y1="1328" y2="1328" x1="1008" />
            <wire x2="1248" y1="1328" y2="1664" x1="1248" />
            <wire x2="1488" y1="1664" y2="1664" x1="1248" />
        </branch>
        <branch name="ALU_ADDMUL_IN_A0">
            <wire x2="1456" y1="448" y2="448" x1="1136" />
            <wire x2="1472" y1="448" y2="448" x1="1456" />
            <wire x2="1456" y1="448" y2="1408" x1="1456" />
            <wire x2="1488" y1="1408" y2="1408" x1="1456" />
        </branch>
        <branch name="ALU_ADDMUL_IN_A1">
            <wire x2="1440" y1="576" y2="576" x1="1136" />
            <wire x2="1472" y1="576" y2="576" x1="1440" />
            <wire x2="1440" y1="576" y2="1472" x1="1440" />
            <wire x2="1488" y1="1472" y2="1472" x1="1440" />
        </branch>
        <branch name="ALU_ADDMUL_IN_A2">
            <wire x2="1424" y1="704" y2="704" x1="1120" />
            <wire x2="1472" y1="704" y2="704" x1="1424" />
            <wire x2="1424" y1="704" y2="1536" x1="1424" />
            <wire x2="1488" y1="1536" y2="1536" x1="1424" />
        </branch>
        <branch name="ALU_ADDMUL_IN_A3">
            <wire x2="1408" y1="832" y2="832" x1="1120" />
            <wire x2="1472" y1="832" y2="832" x1="1408" />
            <wire x2="1408" y1="832" y2="1600" x1="1408" />
            <wire x2="1488" y1="1600" y2="1600" x1="1408" />
        </branch>
        <branch name="ALU_ADDMUL_IN_B0">
            <wire x2="1392" y1="512" y2="512" x1="1136" />
            <wire x2="1472" y1="512" y2="512" x1="1392" />
            <wire x2="1392" y1="512" y2="1344" x1="1392" />
            <wire x2="1488" y1="1344" y2="1344" x1="1392" />
        </branch>
        <branch name="ALU_ADDMUL_IN_B1">
            <wire x2="1376" y1="640" y2="640" x1="1136" />
            <wire x2="1472" y1="640" y2="640" x1="1376" />
            <wire x2="1376" y1="640" y2="1984" x1="1376" />
            <wire x2="1488" y1="1984" y2="1984" x1="1376" />
        </branch>
        <branch name="ALU_ADDMUL_IN_B2">
            <wire x2="1312" y1="768" y2="768" x1="1120" />
            <wire x2="1472" y1="768" y2="768" x1="1312" />
            <wire x2="1312" y1="768" y2="2048" x1="1312" />
            <wire x2="1488" y1="2048" y2="2048" x1="1312" />
        </branch>
        <branch name="ALU_ADDMUL_IN_B3">
            <wire x2="1328" y1="896" y2="896" x1="1120" />
            <wire x2="1472" y1="896" y2="896" x1="1328" />
            <wire x2="1328" y1="896" y2="2112" x1="1328" />
            <wire x2="1488" y1="2112" y2="2112" x1="1328" />
        </branch>
        <branch name="ALU_ADDMUL_IN_F0">
            <wire x2="624" y1="1200" y2="1200" x1="592" />
        </branch>
        <iomarker fontsize="28" x="592" y="1200" name="ALU_ADDMUL_IN_F0" orien="R180" />
        <branch name="ALU_ADDMUL_IN_F1">
            <wire x2="624" y1="1264" y2="1264" x1="592" />
        </branch>
        <iomarker fontsize="28" x="592" y="1264" name="ALU_ADDMUL_IN_F1" orien="R180" />
        <branch name="ALU_ADDMUL_IN_F2">
            <wire x2="624" y1="1328" y2="1328" x1="592" />
        </branch>
        <iomarker fontsize="28" x="592" y="1328" name="ALU_ADDMUL_IN_F2" orien="R180" />
        <branch name="ALU_ADDMUL_IN_C">
            <wire x2="1472" y1="384" y2="384" x1="1440" />
        </branch>
        <iomarker fontsize="28" x="1440" y="384" name="ALU_ADDMUL_IN_C" orien="R180" />
        <iomarker fontsize="28" x="1136" y="448" name="ALU_ADDMUL_IN_A0" orien="R180" />
        <iomarker fontsize="28" x="1136" y="512" name="ALU_ADDMUL_IN_B0" orien="R180" />
        <iomarker fontsize="28" x="1136" y="576" name="ALU_ADDMUL_IN_A1" orien="R180" />
        <iomarker fontsize="28" x="1136" y="640" name="ALU_ADDMUL_IN_B1" orien="R180" />
        <iomarker fontsize="28" x="1120" y="704" name="ALU_ADDMUL_IN_A2" orien="R180" />
        <iomarker fontsize="28" x="1120" y="768" name="ALU_ADDMUL_IN_B2" orien="R180" />
        <iomarker fontsize="28" x="1120" y="832" name="ALU_ADDMUL_IN_A3" orien="R180" />
        <iomarker fontsize="28" x="1120" y="896" name="ALU_ADDMUL_IN_B3" orien="R180" />
        <branch name="ALU_ADDMUL_OUT_R7">
            <wire x2="1904" y1="2016" y2="2016" x1="1872" />
        </branch>
        <iomarker fontsize="28" x="1904" y="2016" name="ALU_ADDMUL_OUT_R7" orien="R0" />
        <branch name="ALU_ADDMUL_OUT_R6">
            <wire x2="1904" y1="1920" y2="1920" x1="1872" />
        </branch>
        <iomarker fontsize="28" x="1904" y="1920" name="ALU_ADDMUL_OUT_R6" orien="R0" />
        <branch name="ALU_ADDMUL_OUT_R5">
            <wire x2="1904" y1="1824" y2="1824" x1="1872" />
        </branch>
        <iomarker fontsize="28" x="1904" y="1824" name="ALU_ADDMUL_OUT_R5" orien="R0" />
        <instance x="2416" y="752" name="XLXI_5" orien="R0" />
        <instance x="2416" y="928" name="XLXI_6" orien="R0" />
        <instance x="2416" y="1104" name="XLXI_7" orien="R0" />
        <instance x="2416" y="1280" name="XLXI_8" orien="R0" />
        <instance x="2416" y="1440" name="XLXI_9" orien="R0" />
        <branch name="XLXN_21">
            <wire x2="2160" y1="384" y2="384" x1="1920" />
            <wire x2="2160" y1="384" y2="624" x1="2160" />
            <wire x2="2416" y1="624" y2="624" x1="2160" />
        </branch>
        <branch name="XLXN_22">
            <wire x2="2144" y1="576" y2="576" x1="1920" />
            <wire x2="2144" y1="576" y2="800" x1="2144" />
            <wire x2="2416" y1="800" y2="800" x1="2144" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="2128" y1="768" y2="768" x1="1920" />
            <wire x2="2128" y1="768" y2="976" x1="2128" />
            <wire x2="2416" y1="976" y2="976" x1="2128" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="2112" y1="960" y2="960" x1="1920" />
            <wire x2="2112" y1="960" y2="1152" x1="2112" />
            <wire x2="2416" y1="1152" y2="1152" x1="2112" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="2096" y1="1152" y2="1152" x1="1920" />
            <wire x2="2096" y1="1152" y2="1312" x1="2096" />
            <wire x2="2416" y1="1312" y2="1312" x1="2096" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="2144" y1="1728" y2="1728" x1="1872" />
            <wire x2="2144" y1="1376" y2="1728" x1="2144" />
            <wire x2="2416" y1="1376" y2="1376" x1="2144" />
        </branch>
        <branch name="XLXN_27">
            <wire x2="2080" y1="1632" y2="1632" x1="1872" />
            <wire x2="2080" y1="1216" y2="1632" x1="2080" />
            <wire x2="2416" y1="1216" y2="1216" x1="2080" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="2128" y1="1536" y2="1536" x1="1872" />
            <wire x2="2128" y1="1040" y2="1536" x1="2128" />
            <wire x2="2416" y1="1040" y2="1040" x1="2128" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="2160" y1="1440" y2="1440" x1="1872" />
            <wire x2="2160" y1="864" y2="1440" x1="2160" />
            <wire x2="2416" y1="864" y2="864" x1="2160" />
        </branch>
        <branch name="XLXN_30">
            <wire x2="2176" y1="1344" y2="1344" x1="1872" />
            <wire x2="2176" y1="688" y2="1344" x1="2176" />
            <wire x2="2416" y1="688" y2="688" x1="2176" />
        </branch>
        <branch name="ALU_ADDMUL_OUT_R0">
            <wire x2="2704" y1="656" y2="656" x1="2672" />
        </branch>
        <iomarker fontsize="28" x="2704" y="656" name="ALU_ADDMUL_OUT_R0" orien="R0" />
        <branch name="ALU_ADDMUL_OUT_R1">
            <wire x2="2704" y1="832" y2="832" x1="2672" />
        </branch>
        <iomarker fontsize="28" x="2704" y="832" name="ALU_ADDMUL_OUT_R1" orien="R0" />
        <branch name="ALU_ADDMUL_OUT_R2">
            <wire x2="2704" y1="1008" y2="1008" x1="2672" />
        </branch>
        <iomarker fontsize="28" x="2704" y="1008" name="ALU_ADDMUL_OUT_R2" orien="R0" />
        <branch name="ALU_ADDMUL_OUT_R3">
            <wire x2="2704" y1="1184" y2="1184" x1="2672" />
        </branch>
        <iomarker fontsize="28" x="2704" y="1184" name="ALU_ADDMUL_OUT_R3" orien="R0" />
        <branch name="ALU_ADDMUL_OUT_R4">
            <wire x2="2704" y1="1344" y2="1344" x1="2672" />
        </branch>
        <iomarker fontsize="28" x="2704" y="1344" name="ALU_ADDMUL_OUT_R4" orien="R0" />
        <branch name="XLXN_36">
            <wire x2="1488" y1="1728" y2="1728" x1="1456" />
        </branch>
        <instance x="1328" y="1664" name="XLXI_10" orien="R90" />
        <branch name="XLXN_37">
            <wire x2="1488" y1="1792" y2="1792" x1="1456" />
        </branch>
        <instance x="1328" y="1728" name="XLXI_11" orien="R90" />
        <branch name="XLXN_38">
            <wire x2="1488" y1="1856" y2="1856" x1="1456" />
        </branch>
        <instance x="1328" y="1792" name="XLXI_12" orien="R90" />
        <branch name="XLXN_39">
            <wire x2="1488" y1="1920" y2="1920" x1="1456" />
        </branch>
        <instance x="1328" y="1856" name="XLXI_13" orien="R90" />
    </sheet>
</drawing>