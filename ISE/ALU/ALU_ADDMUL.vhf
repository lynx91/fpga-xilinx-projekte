--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : ALU_ADDMUL.vhf
-- /___/   /\     Timestamp : 11/06/2014 13:15:51
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl C:/Users/heringm/ownCloud/BA/FPGA/ISE/ALU/ALU_ADDMUL.vhf -w C:/Users/heringm/ownCloud/BA/FPGA/ISE/ALU/ALU_ADDMUL.sch
--Design Name: ALU_ADDMUL
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Decoder_MUSER_ALU_ADDMUL is
   port ( IN_F0   : in    std_logic; 
          IN_F1   : in    std_logic; 
          IN_F2   : in    std_logic; 
          OUT_EN0 : out   std_logic; 
          OUT_EN1 : out   std_logic; 
          OUT_EN2 : out   std_logic; 
          OUT_EN3 : out   std_logic; 
          OUT_EN4 : out   std_logic);
end Decoder_MUSER_ALU_ADDMUL;

architecture BEHAVIORAL of Decoder_MUSER_ALU_ADDMUL is
   attribute BOX_TYPE   : string ;
   signal XLXN_9  : std_logic;
   signal XLXN_10 : std_logic;
   signal XLXN_11 : std_logic;
   signal XLXN_14 : std_logic;
   signal XLXN_18 : std_logic;
   signal XLXN_27 : std_logic;
   signal XLXN_28 : std_logic;
   signal XLXN_29 : std_logic;
   signal XLXN_30 : std_logic;
   signal XLXN_43 : std_logic;
   signal XLXN_44 : std_logic;
   signal XLXN_46 : std_logic;
   signal XLXN_47 : std_logic;
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
begin
   XLXI_5 : INV
      port map (I=>IN_F0,
                O=>XLXN_9);
   
   XLXI_6 : INV
      port map (I=>IN_F1,
                O=>XLXN_10);
   
   XLXI_7 : INV
      port map (I=>IN_F0,
                O=>XLXN_11);
   
   XLXI_8 : INV
      port map (I=>IN_F1,
                O=>XLXN_14);
   
   XLXI_9 : AND3
      port map (I0=>XLXN_18,
                I1=>XLXN_10,
                I2=>XLXN_9,
                O=>OUT_EN0);
   
   XLXI_10 : AND3
      port map (I0=>XLXN_18,
                I1=>IN_F1,
                I2=>XLXN_11,
                O=>OUT_EN1);
   
   XLXI_11 : AND3
      port map (I0=>XLXN_18,
                I1=>XLXN_14,
                I2=>IN_F0,
                O=>OUT_EN2);
   
   XLXI_12 : AND3
      port map (I0=>XLXN_18,
                I1=>IN_F1,
                I2=>IN_F0,
                O=>OUT_EN3);
   
   XLXI_13 : INV
      port map (I=>IN_F2,
                O=>XLXN_18);
   
   XLXI_17 : AND3
      port map (I0=>IN_F2,
                I1=>XLXN_28,
                I2=>XLXN_27,
                O=>XLXN_43);
   
   XLXI_18 : AND3
      port map (I0=>IN_F2,
                I1=>IN_F1,
                I2=>XLXN_29,
                O=>XLXN_44);
   
   XLXI_19 : AND3
      port map (I0=>IN_F2,
                I1=>XLXN_30,
                I2=>IN_F0,
                O=>XLXN_46);
   
   XLXI_20 : AND3
      port map (I0=>IN_F2,
                I1=>IN_F1,
                I2=>IN_F0,
                O=>XLXN_47);
   
   XLXI_21 : INV
      port map (I=>IN_F0,
                O=>XLXN_27);
   
   XLXI_22 : INV
      port map (I=>IN_F1,
                O=>XLXN_28);
   
   XLXI_23 : INV
      port map (I=>IN_F0,
                O=>XLXN_29);
   
   XLXI_24 : INV
      port map (I=>IN_F1,
                O=>XLXN_30);
   
   XLXI_25 : OR4
      port map (I0=>XLXN_47,
                I1=>XLXN_46,
                I2=>XLXN_44,
                I3=>XLXN_43,
                O=>OUT_EN4);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Addierer_1Bit_MUSER_ALU_ADDMUL is
   port ( Add_1_A     : in    std_logic; 
          Add_1_B     : in    std_logic; 
          Add_1_En    : in    std_logic; 
          Add_1_En_B  : in    std_logic; 
          Add_1_In_C  : in    std_logic; 
          Add_1_Out_C : out   std_logic; 
          Add_1_S     : out   std_logic);
end Addierer_1Bit_MUSER_ALU_ADDMUL;

architecture BEHAVIORAL of Addierer_1Bit_MUSER_ALU_ADDMUL is
   attribute BOX_TYPE   : string ;
   signal XLXN_2      : std_logic;
   signal XLXN_8      : std_logic;
   signal XLXN_9      : std_logic;
   signal XLXN_17     : std_logic;
   signal XLXN_18     : std_logic;
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : XOR2
      port map (I0=>Add_1_In_C,
                I1=>XLXN_9,
                O=>XLXN_2);
   
   XLXI_2 : XOR2
      port map (I0=>Add_1_A,
                I1=>XLXN_8,
                O=>XLXN_9);
   
   XLXI_4 : AND3
      port map (I0=>Add_1_En,
                I1=>XLXN_8,
                I2=>Add_1_A,
                O=>XLXN_17);
   
   XLXI_5 : AND3
      port map (I0=>Add_1_En,
                I1=>XLXN_9,
                I2=>Add_1_In_C,
                O=>XLXN_18);
   
   XLXI_6 : AND2
      port map (I0=>Add_1_B,
                I1=>Add_1_En_B,
                O=>XLXN_8);
   
   XLXI_7 : AND2
      port map (I0=>Add_1_En,
                I1=>XLXN_2,
                O=>Add_1_S);
   
   XLXI_8 : OR2
      port map (I0=>XLXN_17,
                I1=>XLXN_18,
                O=>Add_1_Out_C);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Addierer_4bit_MUSER_ALU_ADDMUL is
   port ( Add_4_A0    : in    std_logic; 
          Add_4_A1    : in    std_logic; 
          Add_4_A2    : in    std_logic; 
          Add_4_A3    : in    std_logic; 
          Add_4_B0    : in    std_logic; 
          Add_4_B1    : in    std_logic; 
          Add_4_B2    : in    std_logic; 
          Add_4_B3    : in    std_logic; 
          Add_4_En    : in    std_logic; 
          Add_4_En_B  : in    std_logic; 
          Add_4_In_C  : in    std_logic; 
          Add_4_Out_C : out   std_logic; 
          Add_4_S_1   : out   std_logic; 
          Add_4_S0    : out   std_logic; 
          Add_4_S2    : out   std_logic; 
          Add_4_S3    : out   std_logic);
end Addierer_4bit_MUSER_ALU_ADDMUL;

architecture BEHAVIORAL of Addierer_4bit_MUSER_ALU_ADDMUL is
   attribute BOX_TYPE   : string ;
   signal XLXN_11     : std_logic;
   signal XLXN_12     : std_logic;
   signal XLXN_16     : std_logic;
   signal XLXN_21     : std_logic;
   signal XLXN_24     : std_logic;
   component Addierer_1Bit_MUSER_ALU_ADDMUL
      port ( Add_1_A     : in    std_logic; 
             Add_1_En_B  : in    std_logic; 
             Add_1_B     : in    std_logic; 
             Add_1_S     : out   std_logic; 
             Add_1_Out_C : out   std_logic; 
             Add_1_En    : in    std_logic; 
             Add_1_In_C  : in    std_logic);
   end component;
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
begin
   XLXI_1 : Addierer_1Bit_MUSER_ALU_ADDMUL
      port map (Add_1_A=>Add_4_A3,
                Add_1_B=>Add_4_B3,
                Add_1_En=>XLXN_24,
                Add_1_En_B=>XLXN_21,
                Add_1_In_C=>XLXN_11,
                Add_1_Out_C=>Add_4_Out_C,
                Add_1_S=>Add_4_S3);
   
   XLXI_2 : Addierer_1Bit_MUSER_ALU_ADDMUL
      port map (Add_1_A=>Add_4_A2,
                Add_1_B=>Add_4_B2,
                Add_1_En=>XLXN_24,
                Add_1_En_B=>XLXN_21,
                Add_1_In_C=>XLXN_12,
                Add_1_Out_C=>XLXN_11,
                Add_1_S=>Add_4_S2);
   
   XLXI_3 : Addierer_1Bit_MUSER_ALU_ADDMUL
      port map (Add_1_A=>Add_4_A1,
                Add_1_B=>Add_4_B1,
                Add_1_En=>XLXN_24,
                Add_1_En_B=>XLXN_21,
                Add_1_In_C=>XLXN_16,
                Add_1_Out_C=>XLXN_12,
                Add_1_S=>Add_4_S_1);
   
   XLXI_4 : Addierer_1Bit_MUSER_ALU_ADDMUL
      port map (Add_1_A=>Add_4_A0,
                Add_1_B=>Add_4_B0,
                Add_1_En=>XLXN_24,
                Add_1_En_B=>XLXN_21,
                Add_1_In_C=>Add_4_In_C,
                Add_1_Out_C=>XLXN_16,
                Add_1_S=>Add_4_S0);
   
   XLXI_5 : BUF
      port map (I=>Add_4_En_B,
                O=>XLXN_21);
   
   XLXI_10 : BUF
      port map (I=>Add_4_En,
                O=>XLXN_24);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Multiplizierer_4Bit_MUSER_ALU_ADDMUL is
   port ( MUL_4_En : in    std_logic; 
          MUL_4_K0 : in    std_logic; 
          MUL_4_K1 : in    std_logic; 
          MUL_4_K2 : in    std_logic; 
          MUL_4_K3 : in    std_logic; 
          MUL_4_X0 : in    std_logic; 
          MUL_4_X1 : in    std_logic; 
          MUL_4_X2 : in    std_logic; 
          MUL_4_X3 : in    std_logic; 
          MUL_4_Y0 : in    std_logic; 
          MUL_4_Y1 : in    std_logic; 
          MUL_4_Y2 : in    std_logic; 
          MUL_4_Y3 : in    std_logic; 
          MUL_4_P0 : out   std_logic; 
          MUL_4_P1 : out   std_logic; 
          MUL_4_P2 : out   std_logic; 
          MUL_4_P3 : out   std_logic; 
          MUL_4_P4 : out   std_logic; 
          MUL_4_P5 : out   std_logic; 
          MUL_4_P6 : out   std_logic; 
          MUL_4_P7 : out   std_logic);
end Multiplizierer_4Bit_MUSER_ALU_ADDMUL;

architecture BEHAVIORAL of Multiplizierer_4Bit_MUSER_ALU_ADDMUL is
   attribute BOX_TYPE   : string ;
   signal XLXN_5   : std_logic;
   signal XLXN_6   : std_logic;
   signal XLXN_7   : std_logic;
   signal XLXN_8   : std_logic;
   signal XLXN_18  : std_logic;
   signal XLXN_19  : std_logic;
   signal XLXN_20  : std_logic;
   signal XLXN_21  : std_logic;
   signal XLXN_179 : std_logic;
   signal XLXN_180 : std_logic;
   signal XLXN_181 : std_logic;
   signal XLXN_182 : std_logic;
   signal XLXN_287 : std_logic;
   signal XLXN_293 : std_logic;
   signal XLXN_297 : std_logic;
   signal XLXN_298 : std_logic;
   signal XLXN_299 : std_logic;
   signal XLXN_300 : std_logic;
   signal XLXN_301 : std_logic;
   component Addierer_4bit_MUSER_ALU_ADDMUL
      port ( Add_4_B3    : in    std_logic; 
             Add_4_Out_C : out   std_logic; 
             Add_4_B2    : in    std_logic; 
             Add_4_B1    : in    std_logic; 
             Add_4_B0    : in    std_logic; 
             Add_4_A0    : in    std_logic; 
             Add_4_A1    : in    std_logic; 
             Add_4_A2    : in    std_logic; 
             Add_4_A3    : in    std_logic; 
             Add_4_En    : in    std_logic; 
             Add_4_S_1   : out   std_logic; 
             Add_4_S2    : out   std_logic; 
             Add_4_S3    : out   std_logic; 
             Add_4_In_C  : in    std_logic; 
             Add_4_S0    : out   std_logic; 
             Add_4_En_B  : in    std_logic);
   end component;
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
begin
   XLXI_1 : Addierer_4bit_MUSER_ALU_ADDMUL
      port map (Add_4_A0=>MUL_4_K0,
                Add_4_A1=>MUL_4_K1,
                Add_4_A2=>MUL_4_K2,
                Add_4_A3=>MUL_4_K3,
                Add_4_B0=>XLXN_179,
                Add_4_B1=>XLXN_180,
                Add_4_B2=>XLXN_181,
                Add_4_B3=>XLXN_182,
                Add_4_En=>XLXN_293,
                Add_4_En_B=>MUL_4_Y0,
                Add_4_In_C=>XLXN_297,
                Add_4_Out_C=>XLXN_5,
                Add_4_S_1=>XLXN_8,
                Add_4_S0=>MUL_4_P0,
                Add_4_S2=>XLXN_7,
                Add_4_S3=>XLXN_6);
   
   XLXI_2 : Addierer_4bit_MUSER_ALU_ADDMUL
      port map (Add_4_A0=>XLXN_8,
                Add_4_A1=>XLXN_7,
                Add_4_A2=>XLXN_6,
                Add_4_A3=>XLXN_5,
                Add_4_B0=>XLXN_179,
                Add_4_B1=>XLXN_180,
                Add_4_B2=>XLXN_181,
                Add_4_B3=>XLXN_182,
                Add_4_En=>XLXN_293,
                Add_4_En_B=>MUL_4_Y1,
                Add_4_In_C=>XLXN_297,
                Add_4_Out_C=>XLXN_21,
                Add_4_S_1=>XLXN_18,
                Add_4_S0=>MUL_4_P1,
                Add_4_S2=>XLXN_19,
                Add_4_S3=>XLXN_20);
   
   XLXI_3 : Addierer_4bit_MUSER_ALU_ADDMUL
      port map (Add_4_A0=>XLXN_18,
                Add_4_A1=>XLXN_19,
                Add_4_A2=>XLXN_20,
                Add_4_A3=>XLXN_21,
                Add_4_B0=>XLXN_179,
                Add_4_B1=>XLXN_180,
                Add_4_B2=>XLXN_181,
                Add_4_B3=>XLXN_182,
                Add_4_En=>XLXN_293,
                Add_4_En_B=>MUL_4_Y2,
                Add_4_In_C=>XLXN_297,
                Add_4_Out_C=>XLXN_298,
                Add_4_S_1=>XLXN_301,
                Add_4_S0=>MUL_4_P2,
                Add_4_S2=>XLXN_300,
                Add_4_S3=>XLXN_299);
   
   XLXI_4 : Addierer_4bit_MUSER_ALU_ADDMUL
      port map (Add_4_A0=>XLXN_301,
                Add_4_A1=>XLXN_300,
                Add_4_A2=>XLXN_299,
                Add_4_A3=>XLXN_298,
                Add_4_B0=>XLXN_179,
                Add_4_B1=>XLXN_180,
                Add_4_B2=>XLXN_181,
                Add_4_B3=>XLXN_182,
                Add_4_En=>XLXN_293,
                Add_4_En_B=>MUL_4_Y3,
                Add_4_In_C=>XLXN_297,
                Add_4_Out_C=>MUL_4_P7,
                Add_4_S_1=>MUL_4_P4,
                Add_4_S0=>MUL_4_P3,
                Add_4_S2=>MUL_4_P5,
                Add_4_S3=>MUL_4_P6);
   
   XLXI_27 : BUF
      port map (I=>MUL_4_X0,
                O=>XLXN_179);
   
   XLXI_28 : BUF
      port map (I=>MUL_4_X1,
                O=>XLXN_180);
   
   XLXI_29 : BUF
      port map (I=>MUL_4_X2,
                O=>XLXN_181);
   
   XLXI_30 : BUF
      port map (I=>MUL_4_X3,
                O=>XLXN_182);
   
   XLXI_44 : BUF
      port map (I=>MUL_4_En,
                O=>XLXN_293);
   
   XLXI_45 : BUF
      port map (I=>XLXN_287,
                O=>XLXN_297);
   
   XLXI_46 : GND
      port map (G=>XLXN_287);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Logical_Unit_MUSER_ALU_ADDMUL is
   port ( IN_A   : in    std_logic; 
          IN_B   : in    std_logic; 
          IN_EN0 : in    std_logic; 
          IN_EN1 : in    std_logic; 
          IN_EN2 : in    std_logic; 
          OUT_A  : out   std_logic; 
          OUT_B  : out   std_logic; 
          OUT_0  : out   std_logic; 
          OUT_1  : out   std_logic; 
          OUT_2  : out   std_logic);
end Logical_Unit_MUSER_ALU_ADDMUL;

architecture BEHAVIORAL of Logical_Unit_MUSER_ALU_ADDMUL is
   attribute BOX_TYPE   : string ;
   signal XLXN_1 : std_logic;
   signal XLXN_2 : std_logic;
   signal XLXN_3 : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>IN_B,
                I1=>IN_A,
                O=>XLXN_1);
   
   XLXI_2 : AND2
      port map (I0=>IN_EN0,
                I1=>XLXN_1,
                O=>OUT_0);
   
   XLXI_3 : AND2
      port map (I0=>IN_EN1,
                I1=>XLXN_2,
                O=>OUT_1);
   
   XLXI_4 : AND2
      port map (I0=>IN_EN2,
                I1=>XLXN_3,
                O=>OUT_2);
   
   XLXI_5 : OR2
      port map (I0=>IN_B,
                I1=>IN_A,
                O=>XLXN_2);
   
   XLXI_6 : INV
      port map (I=>IN_B,
                O=>XLXN_3);
   
   XLXI_7 : BUF
      port map (I=>IN_A,
                O=>OUT_A);
   
   XLXI_8 : BUF
      port map (I=>IN_B,
                O=>OUT_B);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ALU_1Bit_MUSER_ALU_ADDMUL is
   port ( ALU_1_IN_A    : in    std_logic; 
          ALU_1_IN_B    : in    std_logic; 
          ALU_1_IN_C    : in    std_logic; 
          ALU_1_IN_EN0  : in    std_logic; 
          ALU_1_IN_EN1  : in    std_logic; 
          ALU_1_IN_EN2  : in    std_logic; 
          ALU_1_IN_EN3  : in    std_logic; 
          ALU_1_IN_INVA : in    std_logic; 
          ALU_1_OUT     : out   std_logic; 
          ALU_1_OUT_C   : out   std_logic);
end ALU_1Bit_MUSER_ALU_ADDMUL;

architecture BEHAVIORAL of ALU_1Bit_MUSER_ALU_ADDMUL is
   attribute BOX_TYPE   : string ;
   signal ALU_1_IN_ENA  : std_logic;
   signal ALU_1_IN_ENB  : std_logic;
   signal XLXN_1        : std_logic;
   signal XLXN_28       : std_logic;
   signal XLXN_29       : std_logic;
   signal XLXN_30       : std_logic;
   signal XLXN_31       : std_logic;
   signal XLXN_32       : std_logic;
   signal XLXN_33       : std_logic;
   signal XLXN_34       : std_logic;
   signal XLXN_35       : std_logic;
   signal XLXN_36       : std_logic;
   component Addierer_1Bit_MUSER_ALU_ADDMUL
      port ( Add_1_A     : in    std_logic; 
             Add_1_En_B  : in    std_logic; 
             Add_1_B     : in    std_logic; 
             Add_1_S     : out   std_logic; 
             Add_1_Out_C : out   std_logic; 
             Add_1_En    : in    std_logic; 
             Add_1_In_C  : in    std_logic);
   end component;
   
   component Logical_Unit_MUSER_ALU_ADDMUL
      port ( IN_A   : in    std_logic; 
             IN_B   : in    std_logic; 
             OUT_A  : out   std_logic; 
             OUT_B  : out   std_logic; 
             OUT_0  : out   std_logic; 
             OUT_1  : out   std_logic; 
             OUT_2  : out   std_logic; 
             IN_EN0 : in    std_logic; 
             IN_EN1 : in    std_logic; 
             IN_EN2 : in    std_logic);
   end component;
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
begin
   XLXI_1 : Addierer_1Bit_MUSER_ALU_ADDMUL
      port map (Add_1_A=>XLXN_31,
                Add_1_B=>XLXN_32,
                Add_1_En=>ALU_1_IN_EN3,
                Add_1_En_B=>XLXN_28,
                Add_1_In_C=>ALU_1_IN_C,
                Add_1_Out_C=>ALU_1_OUT_C,
                Add_1_S=>XLXN_36);
   
   XLXI_3 : Logical_Unit_MUSER_ALU_ADDMUL
      port map (IN_A=>XLXN_29,
                IN_B=>XLXN_30,
                IN_EN0=>ALU_1_IN_EN0,
                IN_EN1=>ALU_1_IN_EN1,
                IN_EN2=>ALU_1_IN_EN2,
                OUT_A=>XLXN_31,
                OUT_B=>XLXN_32,
                OUT_0=>XLXN_33,
                OUT_1=>XLXN_34,
                OUT_2=>XLXN_35);
   
   XLXI_4 : XOR2
      port map (I0=>XLXN_1,
                I1=>ALU_1_IN_INVA,
                O=>XLXN_29);
   
   XLXI_5 : AND2
      port map (I0=>ALU_1_IN_ENA,
                I1=>ALU_1_IN_A,
                O=>XLXN_1);
   
   XLXI_6 : AND2
      port map (I0=>ALU_1_IN_ENB,
                I1=>ALU_1_IN_B,
                O=>XLXN_30);
   
   XLXI_10 : VCC
      port map (P=>XLXN_28);
   
   XLXI_11 : OR4
      port map (I0=>XLXN_36,
                I1=>XLXN_35,
                I2=>XLXN_34,
                I3=>XLXN_33,
                O=>ALU_1_OUT);
   
   XLXI_12 : VCC
      port map (P=>ALU_1_IN_ENA);
   
   XLXI_13 : VCC
      port map (P=>ALU_1_IN_ENB);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ALU_4Bit_MUSER_ALU_ADDMUL is
   port ( ALU_4_IN_A0  : in    std_logic; 
          ALU_4_IN_A1  : in    std_logic; 
          ALU_4_IN_A2  : in    std_logic; 
          ALU_4_IN_A3  : in    std_logic; 
          ALU_4_IN_B0  : in    std_logic; 
          ALU_4_IN_B1  : in    std_logic; 
          ALU_4_IN_B2  : in    std_logic; 
          ALU_4_IN_B3  : in    std_logic; 
          ALU_4_IN_C   : in    std_logic; 
          ALU_4_IN_EN0 : in    std_logic; 
          ALU_4_IN_EN1 : in    std_logic; 
          ALU_4_IN_EN2 : in    std_logic; 
          ALU_4_IN_EN3 : in    std_logic; 
          ALU_4_OUTC   : out   std_logic; 
          ALU_4_OUT0   : out   std_logic; 
          ALU_4_OUT1   : out   std_logic; 
          ALU_4_OUT2   : out   std_logic; 
          ALU_4_OUT3   : out   std_logic);
end ALU_4Bit_MUSER_ALU_ADDMUL;

architecture BEHAVIORAL of ALU_4Bit_MUSER_ALU_ADDMUL is
   attribute BOX_TYPE   : string ;
   signal XLXN_12      : std_logic;
   signal XLXN_13      : std_logic;
   signal XLXN_14      : std_logic;
   signal XLXN_27      : std_logic;
   signal XLXN_28      : std_logic;
   signal XLXN_29      : std_logic;
   signal XLXN_30      : std_logic;
   signal XLXN_31      : std_logic;
   signal XLXN_32      : std_logic;
   signal XLXN_33      : std_logic;
   signal XLXN_34      : std_logic;
   component ALU_1Bit_MUSER_ALU_ADDMUL
      port ( ALU_1_IN_INVA : in    std_logic; 
             ALU_1_IN_A    : in    std_logic; 
             ALU_1_IN_B    : in    std_logic; 
             ALU_1_IN_EN0  : in    std_logic; 
             ALU_1_IN_EN1  : in    std_logic; 
             ALU_1_IN_EN2  : in    std_logic; 
             ALU_1_IN_C    : in    std_logic; 
             ALU_1_IN_EN3  : in    std_logic; 
             ALU_1_OUT_C   : out   std_logic; 
             ALU_1_OUT     : out   std_logic);
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
begin
   XLXI_1 : ALU_1Bit_MUSER_ALU_ADDMUL
      port map (ALU_1_IN_A=>ALU_4_IN_A0,
                ALU_1_IN_B=>ALU_4_IN_B0,
                ALU_1_IN_C=>ALU_4_IN_C,
                ALU_1_IN_EN0=>XLXN_30,
                ALU_1_IN_EN1=>XLXN_29,
                ALU_1_IN_EN2=>XLXN_28,
                ALU_1_IN_EN3=>XLXN_27,
                ALU_1_IN_INVA=>XLXN_31,
                ALU_1_OUT=>ALU_4_OUT0,
                ALU_1_OUT_C=>XLXN_12);
   
   XLXI_2 : ALU_1Bit_MUSER_ALU_ADDMUL
      port map (ALU_1_IN_A=>ALU_4_IN_A1,
                ALU_1_IN_B=>ALU_4_IN_B1,
                ALU_1_IN_C=>XLXN_12,
                ALU_1_IN_EN0=>XLXN_30,
                ALU_1_IN_EN1=>XLXN_29,
                ALU_1_IN_EN2=>XLXN_28,
                ALU_1_IN_EN3=>XLXN_27,
                ALU_1_IN_INVA=>XLXN_32,
                ALU_1_OUT=>ALU_4_OUT1,
                ALU_1_OUT_C=>XLXN_13);
   
   XLXI_3 : ALU_1Bit_MUSER_ALU_ADDMUL
      port map (ALU_1_IN_A=>ALU_4_IN_A2,
                ALU_1_IN_B=>ALU_4_IN_B2,
                ALU_1_IN_C=>XLXN_13,
                ALU_1_IN_EN0=>XLXN_30,
                ALU_1_IN_EN1=>XLXN_29,
                ALU_1_IN_EN2=>XLXN_28,
                ALU_1_IN_EN3=>XLXN_27,
                ALU_1_IN_INVA=>XLXN_33,
                ALU_1_OUT=>ALU_4_OUT2,
                ALU_1_OUT_C=>XLXN_14);
   
   XLXI_4 : ALU_1Bit_MUSER_ALU_ADDMUL
      port map (ALU_1_IN_A=>ALU_4_IN_A3,
                ALU_1_IN_B=>ALU_4_IN_B3,
                ALU_1_IN_C=>XLXN_14,
                ALU_1_IN_EN0=>XLXN_30,
                ALU_1_IN_EN1=>XLXN_29,
                ALU_1_IN_EN2=>XLXN_28,
                ALU_1_IN_EN3=>XLXN_27,
                ALU_1_IN_INVA=>XLXN_34,
                ALU_1_OUT=>ALU_4_OUT3,
                ALU_1_OUT_C=>ALU_4_OUTC);
   
   XLXI_11 : GND
      port map (G=>XLXN_31);
   
   XLXI_12 : GND
      port map (G=>XLXN_32);
   
   XLXI_13 : GND
      port map (G=>XLXN_33);
   
   XLXI_14 : GND
      port map (G=>XLXN_34);
   
   XLXI_15 : BUF
      port map (I=>ALU_4_IN_EN0,
                O=>XLXN_30);
   
   XLXI_16 : BUF
      port map (I=>ALU_4_IN_EN1,
                O=>XLXN_29);
   
   XLXI_17 : BUF
      port map (I=>ALU_4_IN_EN2,
                O=>XLXN_28);
   
   XLXI_18 : BUF
      port map (I=>ALU_4_IN_EN3,
                O=>XLXN_27);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ALU_ADDMUL is
   port ( ALU_ADDMUL_IN_A0  : in    std_logic; 
          ALU_ADDMUL_IN_A1  : in    std_logic; 
          ALU_ADDMUL_IN_A2  : in    std_logic; 
          ALU_ADDMUL_IN_A3  : in    std_logic; 
          ALU_ADDMUL_IN_B0  : in    std_logic; 
          ALU_ADDMUL_IN_B1  : in    std_logic; 
          ALU_ADDMUL_IN_B2  : in    std_logic; 
          ALU_ADDMUL_IN_B3  : in    std_logic; 
          ALU_ADDMUL_IN_C   : in    std_logic; 
          ALU_ADDMUL_IN_F0  : in    std_logic; 
          ALU_ADDMUL_IN_F1  : in    std_logic; 
          ALU_ADDMUL_IN_F2  : in    std_logic; 
          ALU_ADDMUL_OUT_R0 : out   std_logic; 
          ALU_ADDMUL_OUT_R1 : out   std_logic; 
          ALU_ADDMUL_OUT_R2 : out   std_logic; 
          ALU_ADDMUL_OUT_R3 : out   std_logic; 
          ALU_ADDMUL_OUT_R4 : out   std_logic; 
          ALU_ADDMUL_OUT_R5 : out   std_logic; 
          ALU_ADDMUL_OUT_R6 : out   std_logic; 
          ALU_ADDMUL_OUT_R7 : out   std_logic);
end ALU_ADDMUL;

architecture BEHAVIORAL of ALU_ADDMUL is
   attribute BOX_TYPE   : string ;
   signal XLXN_1            : std_logic;
   signal XLXN_2            : std_logic;
   signal XLXN_3            : std_logic;
   signal XLXN_4            : std_logic;
   signal XLXN_5            : std_logic;
   signal XLXN_21           : std_logic;
   signal XLXN_22           : std_logic;
   signal XLXN_23           : std_logic;
   signal XLXN_24           : std_logic;
   signal XLXN_25           : std_logic;
   signal XLXN_26           : std_logic;
   signal XLXN_27           : std_logic;
   signal XLXN_28           : std_logic;
   signal XLXN_29           : std_logic;
   signal XLXN_30           : std_logic;
   signal XLXN_36           : std_logic;
   signal XLXN_37           : std_logic;
   signal XLXN_38           : std_logic;
   signal XLXN_39           : std_logic;
   component ALU_4Bit_MUSER_ALU_ADDMUL
      port ( ALU_4_IN_C   : in    std_logic; 
             ALU_4_IN_A0  : in    std_logic; 
             ALU_4_IN_B0  : in    std_logic; 
             ALU_4_IN_A1  : in    std_logic; 
             ALU_4_IN_B1  : in    std_logic; 
             ALU_4_IN_A2  : in    std_logic; 
             ALU_4_IN_B2  : in    std_logic; 
             ALU_4_IN_A3  : in    std_logic; 
             ALU_4_IN_B3  : in    std_logic; 
             ALU_4_IN_EN0 : in    std_logic; 
             ALU_4_IN_EN1 : in    std_logic; 
             ALU_4_IN_EN2 : in    std_logic; 
             ALU_4_IN_EN3 : in    std_logic; 
             ALU_4_OUT0   : out   std_logic; 
             ALU_4_OUT1   : out   std_logic; 
             ALU_4_OUT2   : out   std_logic; 
             ALU_4_OUT3   : out   std_logic; 
             ALU_4_OUTC   : out   std_logic);
   end component;
   
   component Multiplizierer_4Bit_MUSER_ALU_ADDMUL
      port ( MUL_4_Y0 : in    std_logic; 
             MUL_4_X0 : in    std_logic; 
             MUL_4_X1 : in    std_logic; 
             MUL_4_X2 : in    std_logic; 
             MUL_4_X3 : in    std_logic; 
             MUL_4_En : in    std_logic; 
             MUL_4_K3 : in    std_logic; 
             MUL_4_K2 : in    std_logic; 
             MUL_4_K1 : in    std_logic; 
             MUL_4_K0 : in    std_logic; 
             MUL_4_Y1 : in    std_logic; 
             MUL_4_Y2 : in    std_logic; 
             MUL_4_Y3 : in    std_logic; 
             MUL_4_P0 : out   std_logic; 
             MUL_4_P1 : out   std_logic; 
             MUL_4_P2 : out   std_logic; 
             MUL_4_P3 : out   std_logic; 
             MUL_4_P4 : out   std_logic; 
             MUL_4_P5 : out   std_logic; 
             MUL_4_P6 : out   std_logic; 
             MUL_4_P7 : out   std_logic);
   end component;
   
   component Decoder_MUSER_ALU_ADDMUL
      port ( OUT_EN0 : out   std_logic; 
             OUT_EN1 : out   std_logic; 
             OUT_EN2 : out   std_logic; 
             OUT_EN3 : out   std_logic; 
             IN_F2   : in    std_logic; 
             OUT_EN4 : out   std_logic; 
             IN_F0   : in    std_logic; 
             IN_F1   : in    std_logic);
   end component;
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
begin
   XLXI_1 : ALU_4Bit_MUSER_ALU_ADDMUL
      port map (ALU_4_IN_A0=>ALU_ADDMUL_IN_A0,
                ALU_4_IN_A1=>ALU_ADDMUL_IN_A1,
                ALU_4_IN_A2=>ALU_ADDMUL_IN_A2,
                ALU_4_IN_A3=>ALU_ADDMUL_IN_A3,
                ALU_4_IN_B0=>ALU_ADDMUL_IN_B0,
                ALU_4_IN_B1=>ALU_ADDMUL_IN_B1,
                ALU_4_IN_B2=>ALU_ADDMUL_IN_B2,
                ALU_4_IN_B3=>ALU_ADDMUL_IN_B3,
                ALU_4_IN_C=>ALU_ADDMUL_IN_C,
                ALU_4_IN_EN0=>XLXN_1,
                ALU_4_IN_EN1=>XLXN_2,
                ALU_4_IN_EN2=>XLXN_3,
                ALU_4_IN_EN3=>XLXN_4,
                ALU_4_OUTC=>XLXN_25,
                ALU_4_OUT0=>XLXN_21,
                ALU_4_OUT1=>XLXN_22,
                ALU_4_OUT2=>XLXN_23,
                ALU_4_OUT3=>XLXN_24);
   
   XLXI_2 : Multiplizierer_4Bit_MUSER_ALU_ADDMUL
      port map (MUL_4_En=>XLXN_5,
                MUL_4_K0=>XLXN_39,
                MUL_4_K1=>XLXN_38,
                MUL_4_K2=>XLXN_37,
                MUL_4_K3=>XLXN_36,
                MUL_4_X0=>ALU_ADDMUL_IN_A0,
                MUL_4_X1=>ALU_ADDMUL_IN_A1,
                MUL_4_X2=>ALU_ADDMUL_IN_A2,
                MUL_4_X3=>ALU_ADDMUL_IN_A3,
                MUL_4_Y0=>ALU_ADDMUL_IN_B0,
                MUL_4_Y1=>ALU_ADDMUL_IN_B1,
                MUL_4_Y2=>ALU_ADDMUL_IN_B2,
                MUL_4_Y3=>ALU_ADDMUL_IN_B3,
                MUL_4_P0=>XLXN_30,
                MUL_4_P1=>XLXN_29,
                MUL_4_P2=>XLXN_28,
                MUL_4_P3=>XLXN_27,
                MUL_4_P4=>XLXN_26,
                MUL_4_P5=>ALU_ADDMUL_OUT_R5,
                MUL_4_P6=>ALU_ADDMUL_OUT_R6,
                MUL_4_P7=>ALU_ADDMUL_OUT_R7);
   
   XLXI_4 : Decoder_MUSER_ALU_ADDMUL
      port map (IN_F0=>ALU_ADDMUL_IN_F0,
                IN_F1=>ALU_ADDMUL_IN_F1,
                IN_F2=>ALU_ADDMUL_IN_F2,
                OUT_EN0=>XLXN_1,
                OUT_EN1=>XLXN_2,
                OUT_EN2=>XLXN_3,
                OUT_EN3=>XLXN_4,
                OUT_EN4=>XLXN_5);
   
   XLXI_5 : OR2
      port map (I0=>XLXN_30,
                I1=>XLXN_21,
                O=>ALU_ADDMUL_OUT_R0);
   
   XLXI_6 : OR2
      port map (I0=>XLXN_29,
                I1=>XLXN_22,
                O=>ALU_ADDMUL_OUT_R1);
   
   XLXI_7 : OR2
      port map (I0=>XLXN_28,
                I1=>XLXN_23,
                O=>ALU_ADDMUL_OUT_R2);
   
   XLXI_8 : OR2
      port map (I0=>XLXN_27,
                I1=>XLXN_24,
                O=>ALU_ADDMUL_OUT_R3);
   
   XLXI_9 : OR2
      port map (I0=>XLXN_26,
                I1=>XLXN_25,
                O=>ALU_ADDMUL_OUT_R4);
   
   XLXI_10 : GND
      port map (G=>XLXN_36);
   
   XLXI_11 : GND
      port map (G=>XLXN_37);
   
   XLXI_12 : GND
      port map (G=>XLXN_38);
   
   XLXI_13 : GND
      port map (G=>XLXN_39);
   
end BEHAVIORAL;


