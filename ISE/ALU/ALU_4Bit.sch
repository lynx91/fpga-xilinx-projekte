<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="ALU_4_IN_C" />
        <signal name="ALU_4_IN_A0" />
        <signal name="ALU_4_IN_B0" />
        <signal name="ALU_4_IN_A1" />
        <signal name="ALU_4_IN_B1" />
        <signal name="ALU_4_OUT0" />
        <signal name="ALU_4_OUT1" />
        <signal name="ALU_4_IN_A2" />
        <signal name="ALU_4_IN_B2" />
        <signal name="ALU_4_OUT2" />
        <signal name="ALU_4_IN_A3" />
        <signal name="ALU_4_IN_B3" />
        <signal name="ALU_4_OUT3" />
        <signal name="ALU_4_OUTC" />
        <signal name="ALU_4_IN_EN0" />
        <signal name="ALU_4_IN_EN1" />
        <signal name="ALU_4_IN_EN2" />
        <signal name="ALU_4_IN_EN3" />
        <port polarity="Input" name="ALU_4_IN_C" />
        <port polarity="Input" name="ALU_4_IN_A0" />
        <port polarity="Input" name="ALU_4_IN_B0" />
        <port polarity="Input" name="ALU_4_IN_A1" />
        <port polarity="Input" name="ALU_4_IN_B1" />
        <port polarity="Output" name="ALU_4_OUT0" />
        <port polarity="Output" name="ALU_4_OUT1" />
        <port polarity="Input" name="ALU_4_IN_A2" />
        <port polarity="Input" name="ALU_4_IN_B2" />
        <port polarity="Output" name="ALU_4_OUT2" />
        <port polarity="Input" name="ALU_4_IN_A3" />
        <port polarity="Input" name="ALU_4_IN_B3" />
        <port polarity="Output" name="ALU_4_OUT3" />
        <port polarity="Output" name="ALU_4_OUTC" />
        <port polarity="Input" name="ALU_4_IN_EN0" />
        <port polarity="Input" name="ALU_4_IN_EN1" />
        <port polarity="Input" name="ALU_4_IN_EN2" />
        <port polarity="Input" name="ALU_4_IN_EN3" />
        <blockdef name="ALU_1Bit">
            <timestamp>2014-11-6T11:45:31</timestamp>
            <rect width="352" x="64" y="-512" height="512" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="480" y1="-480" y2="-480" x1="416" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <block symbolname="ALU_1Bit" name="XLXI_1">
            <blockpin signalname="XLXN_31" name="ALU_1_IN_INVA" />
            <blockpin signalname="ALU_4_IN_A0" name="ALU_1_IN_A" />
            <blockpin signalname="ALU_4_IN_B0" name="ALU_1_IN_B" />
            <blockpin signalname="XLXN_30" name="ALU_1_IN_EN0" />
            <blockpin signalname="XLXN_29" name="ALU_1_IN_EN1" />
            <blockpin signalname="XLXN_28" name="ALU_1_IN_EN2" />
            <blockpin signalname="ALU_4_IN_C" name="ALU_1_IN_C" />
            <blockpin signalname="XLXN_27" name="ALU_1_IN_EN3" />
            <blockpin signalname="XLXN_12" name="ALU_1_OUT_C" />
            <blockpin signalname="ALU_4_OUT0" name="ALU_1_OUT" />
        </block>
        <block symbolname="ALU_1Bit" name="XLXI_2">
            <blockpin signalname="XLXN_32" name="ALU_1_IN_INVA" />
            <blockpin signalname="ALU_4_IN_A1" name="ALU_1_IN_A" />
            <blockpin signalname="ALU_4_IN_B1" name="ALU_1_IN_B" />
            <blockpin signalname="XLXN_30" name="ALU_1_IN_EN0" />
            <blockpin signalname="XLXN_29" name="ALU_1_IN_EN1" />
            <blockpin signalname="XLXN_28" name="ALU_1_IN_EN2" />
            <blockpin signalname="XLXN_12" name="ALU_1_IN_C" />
            <blockpin signalname="XLXN_27" name="ALU_1_IN_EN3" />
            <blockpin signalname="XLXN_13" name="ALU_1_OUT_C" />
            <blockpin signalname="ALU_4_OUT1" name="ALU_1_OUT" />
        </block>
        <block symbolname="ALU_1Bit" name="XLXI_3">
            <blockpin signalname="XLXN_33" name="ALU_1_IN_INVA" />
            <blockpin signalname="ALU_4_IN_A2" name="ALU_1_IN_A" />
            <blockpin signalname="ALU_4_IN_B2" name="ALU_1_IN_B" />
            <blockpin signalname="XLXN_30" name="ALU_1_IN_EN0" />
            <blockpin signalname="XLXN_29" name="ALU_1_IN_EN1" />
            <blockpin signalname="XLXN_28" name="ALU_1_IN_EN2" />
            <blockpin signalname="XLXN_13" name="ALU_1_IN_C" />
            <blockpin signalname="XLXN_27" name="ALU_1_IN_EN3" />
            <blockpin signalname="XLXN_14" name="ALU_1_OUT_C" />
            <blockpin signalname="ALU_4_OUT2" name="ALU_1_OUT" />
        </block>
        <block symbolname="ALU_1Bit" name="XLXI_4">
            <blockpin signalname="XLXN_34" name="ALU_1_IN_INVA" />
            <blockpin signalname="ALU_4_IN_A3" name="ALU_1_IN_A" />
            <blockpin signalname="ALU_4_IN_B3" name="ALU_1_IN_B" />
            <blockpin signalname="XLXN_30" name="ALU_1_IN_EN0" />
            <blockpin signalname="XLXN_29" name="ALU_1_IN_EN1" />
            <blockpin signalname="XLXN_28" name="ALU_1_IN_EN2" />
            <blockpin signalname="XLXN_14" name="ALU_1_IN_C" />
            <blockpin signalname="XLXN_27" name="ALU_1_IN_EN3" />
            <blockpin signalname="ALU_4_OUTC" name="ALU_1_OUT_C" />
            <blockpin signalname="ALU_4_OUT3" name="ALU_1_OUT" />
        </block>
        <block symbolname="gnd" name="XLXI_11">
            <blockpin signalname="XLXN_31" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_12">
            <blockpin signalname="XLXN_32" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_13">
            <blockpin signalname="XLXN_33" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_14">
            <blockpin signalname="XLXN_34" name="G" />
        </block>
        <block symbolname="buf" name="XLXI_15">
            <blockpin signalname="ALU_4_IN_EN0" name="I" />
            <blockpin signalname="XLXN_30" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_16">
            <blockpin signalname="ALU_4_IN_EN1" name="I" />
            <blockpin signalname="XLXN_29" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_17">
            <blockpin signalname="ALU_4_IN_EN2" name="I" />
            <blockpin signalname="XLXN_28" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_18">
            <blockpin signalname="ALU_4_IN_EN3" name="I" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="864" y="784" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_12">
            <wire x2="1376" y1="752" y2="752" x1="1344" />
        </branch>
        <instance x="1376" y="1232" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_13">
            <wire x2="1888" y1="1200" y2="1200" x1="1856" />
        </branch>
        <instance x="1888" y="1680" name="XLXI_3" orien="R0">
        </instance>
        <branch name="XLXN_14">
            <wire x2="2400" y1="1648" y2="1648" x1="2368" />
        </branch>
        <instance x="2400" y="2128" name="XLXI_4" orien="R0">
        </instance>
        <branch name="XLXN_27">
            <wire x2="832" y1="752" y2="752" x1="720" />
            <wire x2="848" y1="752" y2="752" x1="832" />
            <wire x2="864" y1="752" y2="752" x1="848" />
            <wire x2="832" y1="752" y2="1200" x1="832" />
            <wire x2="1376" y1="1200" y2="1200" x1="832" />
            <wire x2="832" y1="1200" y2="1648" x1="832" />
            <wire x2="832" y1="1648" y2="2096" x1="832" />
            <wire x2="2400" y1="2096" y2="2096" x1="832" />
            <wire x2="1888" y1="1648" y2="1648" x1="832" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="800" y1="688" y2="688" x1="720" />
            <wire x2="816" y1="688" y2="688" x1="800" />
            <wire x2="864" y1="688" y2="688" x1="816" />
            <wire x2="800" y1="688" y2="1136" x1="800" />
            <wire x2="1376" y1="1136" y2="1136" x1="800" />
            <wire x2="800" y1="1136" y2="1584" x1="800" />
            <wire x2="800" y1="1584" y2="2032" x1="800" />
            <wire x2="2400" y1="2032" y2="2032" x1="800" />
            <wire x2="1888" y1="1584" y2="1584" x1="800" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="768" y1="624" y2="624" x1="720" />
            <wire x2="784" y1="624" y2="624" x1="768" />
            <wire x2="864" y1="624" y2="624" x1="784" />
            <wire x2="768" y1="624" y2="1072" x1="768" />
            <wire x2="1376" y1="1072" y2="1072" x1="768" />
            <wire x2="768" y1="1072" y2="1520" x1="768" />
            <wire x2="768" y1="1520" y2="1968" x1="768" />
            <wire x2="2400" y1="1968" y2="1968" x1="768" />
            <wire x2="1888" y1="1520" y2="1520" x1="768" />
        </branch>
        <branch name="XLXN_30">
            <wire x2="736" y1="560" y2="560" x1="720" />
            <wire x2="752" y1="560" y2="560" x1="736" />
            <wire x2="864" y1="560" y2="560" x1="752" />
            <wire x2="736" y1="560" y2="1008" x1="736" />
            <wire x2="1376" y1="1008" y2="1008" x1="736" />
            <wire x2="736" y1="1008" y2="1456" x1="736" />
            <wire x2="736" y1="1456" y2="1904" x1="736" />
            <wire x2="2400" y1="1904" y2="1904" x1="736" />
            <wire x2="1888" y1="1456" y2="1456" x1="736" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="864" y1="496" y2="496" x1="832" />
        </branch>
        <instance x="704" y="432" name="XLXI_11" orien="R90" />
        <branch name="XLXN_32">
            <wire x2="1376" y1="944" y2="944" x1="1344" />
        </branch>
        <instance x="1216" y="880" name="XLXI_12" orien="R90" />
        <branch name="XLXN_33">
            <wire x2="1888" y1="1392" y2="1392" x1="1856" />
        </branch>
        <instance x="1728" y="1328" name="XLXI_13" orien="R90" />
        <branch name="XLXN_34">
            <wire x2="2400" y1="1840" y2="1840" x1="2368" />
        </branch>
        <instance x="2240" y="1776" name="XLXI_14" orien="R90" />
        <branch name="ALU_4_IN_C">
            <wire x2="864" y1="304" y2="304" x1="832" />
        </branch>
        <iomarker fontsize="28" x="832" y="304" name="ALU_4_IN_C" orien="R180" />
        <branch name="ALU_4_IN_A0">
            <wire x2="864" y1="368" y2="368" x1="832" />
        </branch>
        <iomarker fontsize="28" x="832" y="368" name="ALU_4_IN_A0" orien="R180" />
        <branch name="ALU_4_IN_B0">
            <wire x2="864" y1="432" y2="432" x1="832" />
        </branch>
        <iomarker fontsize="28" x="832" y="432" name="ALU_4_IN_B0" orien="R180" />
        <branch name="ALU_4_IN_A1">
            <wire x2="1376" y1="816" y2="816" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="1344" y="816" name="ALU_4_IN_A1" orien="R180" />
        <branch name="ALU_4_IN_B1">
            <wire x2="1376" y1="880" y2="880" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="1344" y="880" name="ALU_4_IN_B1" orien="R180" />
        <branch name="ALU_4_OUT0">
            <wire x2="1376" y1="304" y2="304" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="1376" y="304" name="ALU_4_OUT0" orien="R0" />
        <branch name="ALU_4_OUT1">
            <wire x2="1888" y1="752" y2="752" x1="1856" />
        </branch>
        <iomarker fontsize="28" x="1888" y="752" name="ALU_4_OUT1" orien="R0" />
        <branch name="ALU_4_IN_A2">
            <wire x2="1888" y1="1264" y2="1264" x1="1856" />
        </branch>
        <iomarker fontsize="28" x="1856" y="1264" name="ALU_4_IN_A2" orien="R180" />
        <branch name="ALU_4_IN_B2">
            <wire x2="1888" y1="1328" y2="1328" x1="1856" />
        </branch>
        <iomarker fontsize="28" x="1856" y="1328" name="ALU_4_IN_B2" orien="R180" />
        <branch name="ALU_4_OUT2">
            <wire x2="2400" y1="1200" y2="1200" x1="2368" />
        </branch>
        <iomarker fontsize="28" x="2400" y="1200" name="ALU_4_OUT2" orien="R0" />
        <branch name="ALU_4_IN_A3">
            <wire x2="2400" y1="1712" y2="1712" x1="2368" />
        </branch>
        <iomarker fontsize="28" x="2368" y="1712" name="ALU_4_IN_A3" orien="R180" />
        <branch name="ALU_4_IN_B3">
            <wire x2="2400" y1="1776" y2="1776" x1="2368" />
        </branch>
        <iomarker fontsize="28" x="2368" y="1776" name="ALU_4_IN_B3" orien="R180" />
        <branch name="ALU_4_OUT3">
            <wire x2="2912" y1="1648" y2="1648" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2912" y="1648" name="ALU_4_OUT3" orien="R0" />
        <branch name="ALU_4_OUTC">
            <wire x2="2912" y1="2096" y2="2096" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2912" y="2096" name="ALU_4_OUTC" orien="R0" />
        <instance x="496" y="592" name="XLXI_15" orien="R0" />
        <instance x="496" y="656" name="XLXI_16" orien="R0" />
        <instance x="496" y="720" name="XLXI_17" orien="R0" />
        <instance x="496" y="784" name="XLXI_18" orien="R0" />
        <branch name="ALU_4_IN_EN0">
            <wire x2="496" y1="560" y2="560" x1="464" />
        </branch>
        <iomarker fontsize="28" x="464" y="560" name="ALU_4_IN_EN0" orien="R180" />
        <branch name="ALU_4_IN_EN1">
            <wire x2="496" y1="624" y2="624" x1="464" />
        </branch>
        <iomarker fontsize="28" x="464" y="624" name="ALU_4_IN_EN1" orien="R180" />
        <branch name="ALU_4_IN_EN2">
            <wire x2="496" y1="688" y2="688" x1="464" />
        </branch>
        <iomarker fontsize="28" x="464" y="688" name="ALU_4_IN_EN2" orien="R180" />
        <branch name="ALU_4_IN_EN3">
            <wire x2="496" y1="752" y2="752" x1="464" />
        </branch>
        <iomarker fontsize="28" x="464" y="752" name="ALU_4_IN_EN3" orien="R180" />
    </sheet>
</drawing>