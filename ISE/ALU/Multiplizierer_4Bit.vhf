--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : Multiplizierer_4Bit.vhf
-- /___/   /\     Timestamp : 11/06/2014 13:14:58
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl C:/Users/heringm/ownCloud/BA/FPGA/ISE/ALU/Multiplizierer_4Bit.vhf -w C:/Users/heringm/ownCloud/BA/FPGA/ISE/ALU/Multiplizierer_4Bit.sch
--Design Name: Multiplizierer_4Bit
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Addierer_1Bit_MUSER_Multiplizierer_4Bit is
   port ( Add_1_A     : in    std_logic; 
          Add_1_B     : in    std_logic; 
          Add_1_En    : in    std_logic; 
          Add_1_En_B  : in    std_logic; 
          Add_1_In_C  : in    std_logic; 
          Add_1_Out_C : out   std_logic; 
          Add_1_S     : out   std_logic);
end Addierer_1Bit_MUSER_Multiplizierer_4Bit;

architecture BEHAVIORAL of Addierer_1Bit_MUSER_Multiplizierer_4Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_2      : std_logic;
   signal XLXN_8      : std_logic;
   signal XLXN_9      : std_logic;
   signal XLXN_17     : std_logic;
   signal XLXN_18     : std_logic;
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : XOR2
      port map (I0=>Add_1_In_C,
                I1=>XLXN_9,
                O=>XLXN_2);
   
   XLXI_2 : XOR2
      port map (I0=>Add_1_A,
                I1=>XLXN_8,
                O=>XLXN_9);
   
   XLXI_4 : AND3
      port map (I0=>Add_1_En,
                I1=>XLXN_8,
                I2=>Add_1_A,
                O=>XLXN_17);
   
   XLXI_5 : AND3
      port map (I0=>Add_1_En,
                I1=>XLXN_9,
                I2=>Add_1_In_C,
                O=>XLXN_18);
   
   XLXI_6 : AND2
      port map (I0=>Add_1_B,
                I1=>Add_1_En_B,
                O=>XLXN_8);
   
   XLXI_7 : AND2
      port map (I0=>Add_1_En,
                I1=>XLXN_2,
                O=>Add_1_S);
   
   XLXI_8 : OR2
      port map (I0=>XLXN_17,
                I1=>XLXN_18,
                O=>Add_1_Out_C);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Addierer_4bit_MUSER_Multiplizierer_4Bit is
   port ( Add_4_A0    : in    std_logic; 
          Add_4_A1    : in    std_logic; 
          Add_4_A2    : in    std_logic; 
          Add_4_A3    : in    std_logic; 
          Add_4_B0    : in    std_logic; 
          Add_4_B1    : in    std_logic; 
          Add_4_B2    : in    std_logic; 
          Add_4_B3    : in    std_logic; 
          Add_4_En    : in    std_logic; 
          Add_4_En_B  : in    std_logic; 
          Add_4_In_C  : in    std_logic; 
          Add_4_Out_C : out   std_logic; 
          Add_4_S_1   : out   std_logic; 
          Add_4_S0    : out   std_logic; 
          Add_4_S2    : out   std_logic; 
          Add_4_S3    : out   std_logic);
end Addierer_4bit_MUSER_Multiplizierer_4Bit;

architecture BEHAVIORAL of Addierer_4bit_MUSER_Multiplizierer_4Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_11     : std_logic;
   signal XLXN_12     : std_logic;
   signal XLXN_16     : std_logic;
   signal XLXN_21     : std_logic;
   signal XLXN_24     : std_logic;
   component Addierer_1Bit_MUSER_Multiplizierer_4Bit
      port ( Add_1_A     : in    std_logic; 
             Add_1_En_B  : in    std_logic; 
             Add_1_B     : in    std_logic; 
             Add_1_S     : out   std_logic; 
             Add_1_Out_C : out   std_logic; 
             Add_1_En    : in    std_logic; 
             Add_1_In_C  : in    std_logic);
   end component;
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
begin
   XLXI_1 : Addierer_1Bit_MUSER_Multiplizierer_4Bit
      port map (Add_1_A=>Add_4_A3,
                Add_1_B=>Add_4_B3,
                Add_1_En=>XLXN_24,
                Add_1_En_B=>XLXN_21,
                Add_1_In_C=>XLXN_11,
                Add_1_Out_C=>Add_4_Out_C,
                Add_1_S=>Add_4_S3);
   
   XLXI_2 : Addierer_1Bit_MUSER_Multiplizierer_4Bit
      port map (Add_1_A=>Add_4_A2,
                Add_1_B=>Add_4_B2,
                Add_1_En=>XLXN_24,
                Add_1_En_B=>XLXN_21,
                Add_1_In_C=>XLXN_12,
                Add_1_Out_C=>XLXN_11,
                Add_1_S=>Add_4_S2);
   
   XLXI_3 : Addierer_1Bit_MUSER_Multiplizierer_4Bit
      port map (Add_1_A=>Add_4_A1,
                Add_1_B=>Add_4_B1,
                Add_1_En=>XLXN_24,
                Add_1_En_B=>XLXN_21,
                Add_1_In_C=>XLXN_16,
                Add_1_Out_C=>XLXN_12,
                Add_1_S=>Add_4_S_1);
   
   XLXI_4 : Addierer_1Bit_MUSER_Multiplizierer_4Bit
      port map (Add_1_A=>Add_4_A0,
                Add_1_B=>Add_4_B0,
                Add_1_En=>XLXN_24,
                Add_1_En_B=>XLXN_21,
                Add_1_In_C=>Add_4_In_C,
                Add_1_Out_C=>XLXN_16,
                Add_1_S=>Add_4_S0);
   
   XLXI_5 : BUF
      port map (I=>Add_4_En_B,
                O=>XLXN_21);
   
   XLXI_10 : BUF
      port map (I=>Add_4_En,
                O=>XLXN_24);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Multiplizierer_4Bit is
   port ( MUL_4_En : in    std_logic; 
          MUL_4_K0 : in    std_logic; 
          MUL_4_K1 : in    std_logic; 
          MUL_4_K2 : in    std_logic; 
          MUL_4_K3 : in    std_logic; 
          MUL_4_X0 : in    std_logic; 
          MUL_4_X1 : in    std_logic; 
          MUL_4_X2 : in    std_logic; 
          MUL_4_X3 : in    std_logic; 
          MUL_4_Y0 : in    std_logic; 
          MUL_4_Y1 : in    std_logic; 
          MUL_4_Y2 : in    std_logic; 
          MUL_4_Y3 : in    std_logic; 
          MUL_4_P0 : out   std_logic; 
          MUL_4_P1 : out   std_logic; 
          MUL_4_P2 : out   std_logic; 
          MUL_4_P3 : out   std_logic; 
          MUL_4_P4 : out   std_logic; 
          MUL_4_P5 : out   std_logic; 
          MUL_4_P6 : out   std_logic; 
          MUL_4_P7 : out   std_logic);
end Multiplizierer_4Bit;

architecture BEHAVIORAL of Multiplizierer_4Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_5   : std_logic;
   signal XLXN_6   : std_logic;
   signal XLXN_7   : std_logic;
   signal XLXN_8   : std_logic;
   signal XLXN_18  : std_logic;
   signal XLXN_19  : std_logic;
   signal XLXN_20  : std_logic;
   signal XLXN_21  : std_logic;
   signal XLXN_179 : std_logic;
   signal XLXN_180 : std_logic;
   signal XLXN_181 : std_logic;
   signal XLXN_182 : std_logic;
   signal XLXN_287 : std_logic;
   signal XLXN_293 : std_logic;
   signal XLXN_297 : std_logic;
   signal XLXN_298 : std_logic;
   signal XLXN_299 : std_logic;
   signal XLXN_300 : std_logic;
   signal XLXN_301 : std_logic;
   component Addierer_4bit_MUSER_Multiplizierer_4Bit
      port ( Add_4_B3    : in    std_logic; 
             Add_4_Out_C : out   std_logic; 
             Add_4_B2    : in    std_logic; 
             Add_4_B1    : in    std_logic; 
             Add_4_B0    : in    std_logic; 
             Add_4_A0    : in    std_logic; 
             Add_4_A1    : in    std_logic; 
             Add_4_A2    : in    std_logic; 
             Add_4_A3    : in    std_logic; 
             Add_4_En    : in    std_logic; 
             Add_4_S_1   : out   std_logic; 
             Add_4_S2    : out   std_logic; 
             Add_4_S3    : out   std_logic; 
             Add_4_In_C  : in    std_logic; 
             Add_4_S0    : out   std_logic; 
             Add_4_En_B  : in    std_logic);
   end component;
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
begin
   XLXI_1 : Addierer_4bit_MUSER_Multiplizierer_4Bit
      port map (Add_4_A0=>MUL_4_K0,
                Add_4_A1=>MUL_4_K1,
                Add_4_A2=>MUL_4_K2,
                Add_4_A3=>MUL_4_K3,
                Add_4_B0=>XLXN_179,
                Add_4_B1=>XLXN_180,
                Add_4_B2=>XLXN_181,
                Add_4_B3=>XLXN_182,
                Add_4_En=>XLXN_293,
                Add_4_En_B=>MUL_4_Y0,
                Add_4_In_C=>XLXN_297,
                Add_4_Out_C=>XLXN_5,
                Add_4_S_1=>XLXN_8,
                Add_4_S0=>MUL_4_P0,
                Add_4_S2=>XLXN_7,
                Add_4_S3=>XLXN_6);
   
   XLXI_2 : Addierer_4bit_MUSER_Multiplizierer_4Bit
      port map (Add_4_A0=>XLXN_8,
                Add_4_A1=>XLXN_7,
                Add_4_A2=>XLXN_6,
                Add_4_A3=>XLXN_5,
                Add_4_B0=>XLXN_179,
                Add_4_B1=>XLXN_180,
                Add_4_B2=>XLXN_181,
                Add_4_B3=>XLXN_182,
                Add_4_En=>XLXN_293,
                Add_4_En_B=>MUL_4_Y1,
                Add_4_In_C=>XLXN_297,
                Add_4_Out_C=>XLXN_21,
                Add_4_S_1=>XLXN_18,
                Add_4_S0=>MUL_4_P1,
                Add_4_S2=>XLXN_19,
                Add_4_S3=>XLXN_20);
   
   XLXI_3 : Addierer_4bit_MUSER_Multiplizierer_4Bit
      port map (Add_4_A0=>XLXN_18,
                Add_4_A1=>XLXN_19,
                Add_4_A2=>XLXN_20,
                Add_4_A3=>XLXN_21,
                Add_4_B0=>XLXN_179,
                Add_4_B1=>XLXN_180,
                Add_4_B2=>XLXN_181,
                Add_4_B3=>XLXN_182,
                Add_4_En=>XLXN_293,
                Add_4_En_B=>MUL_4_Y2,
                Add_4_In_C=>XLXN_297,
                Add_4_Out_C=>XLXN_298,
                Add_4_S_1=>XLXN_301,
                Add_4_S0=>MUL_4_P2,
                Add_4_S2=>XLXN_300,
                Add_4_S3=>XLXN_299);
   
   XLXI_4 : Addierer_4bit_MUSER_Multiplizierer_4Bit
      port map (Add_4_A0=>XLXN_301,
                Add_4_A1=>XLXN_300,
                Add_4_A2=>XLXN_299,
                Add_4_A3=>XLXN_298,
                Add_4_B0=>XLXN_179,
                Add_4_B1=>XLXN_180,
                Add_4_B2=>XLXN_181,
                Add_4_B3=>XLXN_182,
                Add_4_En=>XLXN_293,
                Add_4_En_B=>MUL_4_Y3,
                Add_4_In_C=>XLXN_297,
                Add_4_Out_C=>MUL_4_P7,
                Add_4_S_1=>MUL_4_P4,
                Add_4_S0=>MUL_4_P3,
                Add_4_S2=>MUL_4_P5,
                Add_4_S3=>MUL_4_P6);
   
   XLXI_27 : BUF
      port map (I=>MUL_4_X0,
                O=>XLXN_179);
   
   XLXI_28 : BUF
      port map (I=>MUL_4_X1,
                O=>XLXN_180);
   
   XLXI_29 : BUF
      port map (I=>MUL_4_X2,
                O=>XLXN_181);
   
   XLXI_30 : BUF
      port map (I=>MUL_4_X3,
                O=>XLXN_182);
   
   XLXI_44 : BUF
      port map (I=>MUL_4_En,
                O=>XLXN_293);
   
   XLXI_45 : BUF
      port map (I=>XLXN_287,
                O=>XLXN_297);
   
   XLXI_46 : GND
      port map (G=>XLXN_287);
   
end BEHAVIORAL;


