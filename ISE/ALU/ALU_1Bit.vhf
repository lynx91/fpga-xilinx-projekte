--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : ALU_1Bit.vhf
-- /___/   /\     Timestamp : 11/06/2014 12:46:10
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl C:/Users/heringm/ownCloud/BA/FPGA/ISE/ALU/ALU_1Bit.vhf -w C:/Users/heringm/ownCloud/BA/FPGA/ISE/ALU/ALU_1Bit.sch
--Design Name: ALU_1Bit
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Logical_Unit_MUSER_ALU_1Bit is
   port ( IN_A   : in    std_logic; 
          IN_B   : in    std_logic; 
          IN_EN0 : in    std_logic; 
          IN_EN1 : in    std_logic; 
          IN_EN2 : in    std_logic; 
          OUT_A  : out   std_logic; 
          OUT_B  : out   std_logic; 
          OUT_0  : out   std_logic; 
          OUT_1  : out   std_logic; 
          OUT_2  : out   std_logic);
end Logical_Unit_MUSER_ALU_1Bit;

architecture BEHAVIORAL of Logical_Unit_MUSER_ALU_1Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_1 : std_logic;
   signal XLXN_2 : std_logic;
   signal XLXN_3 : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>IN_B,
                I1=>IN_A,
                O=>XLXN_1);
   
   XLXI_2 : AND2
      port map (I0=>IN_EN0,
                I1=>XLXN_1,
                O=>OUT_0);
   
   XLXI_3 : AND2
      port map (I0=>IN_EN1,
                I1=>XLXN_2,
                O=>OUT_1);
   
   XLXI_4 : AND2
      port map (I0=>IN_EN2,
                I1=>XLXN_3,
                O=>OUT_2);
   
   XLXI_5 : OR2
      port map (I0=>IN_B,
                I1=>IN_A,
                O=>XLXN_2);
   
   XLXI_6 : INV
      port map (I=>IN_B,
                O=>XLXN_3);
   
   XLXI_7 : BUF
      port map (I=>IN_A,
                O=>OUT_A);
   
   XLXI_8 : BUF
      port map (I=>IN_B,
                O=>OUT_B);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Addierer_1Bit_MUSER_ALU_1Bit is
   port ( Add_1_A     : in    std_logic; 
          Add_1_B     : in    std_logic; 
          Add_1_En    : in    std_logic; 
          Add_1_En_B  : in    std_logic; 
          Add_1_In_C  : in    std_logic; 
          Add_1_Out_C : out   std_logic; 
          Add_1_S     : out   std_logic);
end Addierer_1Bit_MUSER_ALU_1Bit;

architecture BEHAVIORAL of Addierer_1Bit_MUSER_ALU_1Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_2      : std_logic;
   signal XLXN_8      : std_logic;
   signal XLXN_9      : std_logic;
   signal XLXN_17     : std_logic;
   signal XLXN_18     : std_logic;
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : XOR2
      port map (I0=>Add_1_In_C,
                I1=>XLXN_9,
                O=>XLXN_2);
   
   XLXI_2 : XOR2
      port map (I0=>Add_1_A,
                I1=>XLXN_8,
                O=>XLXN_9);
   
   XLXI_4 : AND3
      port map (I0=>Add_1_En,
                I1=>XLXN_8,
                I2=>Add_1_A,
                O=>XLXN_17);
   
   XLXI_5 : AND3
      port map (I0=>Add_1_En,
                I1=>XLXN_9,
                I2=>Add_1_In_C,
                O=>XLXN_18);
   
   XLXI_6 : AND2
      port map (I0=>Add_1_B,
                I1=>Add_1_En_B,
                O=>XLXN_8);
   
   XLXI_7 : AND2
      port map (I0=>Add_1_En,
                I1=>XLXN_2,
                O=>Add_1_S);
   
   XLXI_8 : OR2
      port map (I0=>XLXN_17,
                I1=>XLXN_18,
                O=>Add_1_Out_C);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ALU_1Bit is
   port ( ALU_1_IN_A    : in    std_logic; 
          ALU_1_IN_B    : in    std_logic; 
          ALU_1_IN_C    : in    std_logic; 
          ALU_1_IN_EN0  : in    std_logic; 
          ALU_1_IN_EN1  : in    std_logic; 
          ALU_1_IN_EN2  : in    std_logic; 
          ALU_1_IN_EN3  : in    std_logic; 
          ALU_1_IN_INVA : in    std_logic; 
          ALU_1_OUT     : out   std_logic; 
          ALU_1_OUT_C   : out   std_logic);
end ALU_1Bit;

architecture BEHAVIORAL of ALU_1Bit is
   attribute BOX_TYPE   : string ;
   signal ALU_1_IN_ENA  : std_logic;
   signal ALU_1_IN_ENB  : std_logic;
   signal XLXN_1        : std_logic;
   signal XLXN_28       : std_logic;
   signal XLXN_29       : std_logic;
   signal XLXN_30       : std_logic;
   signal XLXN_31       : std_logic;
   signal XLXN_32       : std_logic;
   signal XLXN_33       : std_logic;
   signal XLXN_34       : std_logic;
   signal XLXN_35       : std_logic;
   signal XLXN_36       : std_logic;
   component Addierer_1Bit_MUSER_ALU_1Bit
      port ( Add_1_En    : in    std_logic; 
             Add_1_In_C  : in    std_logic; 
             Add_1_A     : in    std_logic; 
             Add_1_En_B  : in    std_logic; 
             Add_1_B     : in    std_logic; 
             Add_1_S     : out   std_logic; 
             Add_1_Out_C : out   std_logic);
   end component;
   
   component Logical_Unit_MUSER_ALU_1Bit
      port ( IN_A   : in    std_logic; 
             IN_B   : in    std_logic; 
             OUT_A  : out   std_logic; 
             OUT_B  : out   std_logic; 
             OUT_0  : out   std_logic; 
             OUT_1  : out   std_logic; 
             OUT_2  : out   std_logic; 
             IN_EN0 : in    std_logic; 
             IN_EN1 : in    std_logic; 
             IN_EN2 : in    std_logic);
   end component;
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
begin
   XLXI_1 : Addierer_1Bit_MUSER_ALU_1Bit
      port map (Add_1_A=>XLXN_31,
                Add_1_B=>XLXN_32,
                Add_1_En=>ALU_1_IN_EN3,
                Add_1_En_B=>XLXN_28,
                Add_1_In_C=>ALU_1_IN_C,
                Add_1_Out_C=>ALU_1_OUT_C,
                Add_1_S=>XLXN_36);
   
   XLXI_3 : Logical_Unit_MUSER_ALU_1Bit
      port map (IN_A=>XLXN_29,
                IN_B=>XLXN_30,
                IN_EN0=>ALU_1_IN_EN0,
                IN_EN1=>ALU_1_IN_EN1,
                IN_EN2=>ALU_1_IN_EN2,
                OUT_A=>XLXN_31,
                OUT_B=>XLXN_32,
                OUT_0=>XLXN_33,
                OUT_1=>XLXN_34,
                OUT_2=>XLXN_35);
   
   XLXI_4 : XOR2
      port map (I0=>XLXN_1,
                I1=>ALU_1_IN_INVA,
                O=>XLXN_29);
   
   XLXI_5 : AND2
      port map (I0=>ALU_1_IN_ENA,
                I1=>ALU_1_IN_A,
                O=>XLXN_1);
   
   XLXI_6 : AND2
      port map (I0=>ALU_1_IN_ENB,
                I1=>ALU_1_IN_B,
                O=>XLXN_30);
   
   XLXI_10 : VCC
      port map (P=>XLXN_28);
   
   XLXI_11 : OR4
      port map (I0=>XLXN_36,
                I1=>XLXN_35,
                I2=>XLXN_34,
                I3=>XLXN_33,
                O=>ALU_1_OUT);
   
   XLXI_12 : VCC
      port map (P=>ALU_1_IN_ENA);
   
   XLXI_13 : VCC
      port map (P=>ALU_1_IN_ENB);
   
end BEHAVIORAL;


