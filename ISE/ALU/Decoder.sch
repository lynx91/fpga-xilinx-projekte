<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="IN_F1" />
        <signal name="IN_F0" />
        <signal name="OUT_EN0" />
        <signal name="OUT_EN1" />
        <signal name="OUT_EN2" />
        <signal name="OUT_EN3" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="IN_F2" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40" />
        <signal name="XLXN_41" />
        <signal name="XLXN_42" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47" />
        <signal name="OUT_EN4" />
        <port polarity="Input" name="IN_F1" />
        <port polarity="Input" name="IN_F0" />
        <port polarity="Output" name="OUT_EN0" />
        <port polarity="Output" name="OUT_EN1" />
        <port polarity="Output" name="OUT_EN2" />
        <port polarity="Output" name="OUT_EN3" />
        <port polarity="Input" name="IN_F2" />
        <port polarity="Output" name="OUT_EN4" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="IN_F0" name="I" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="IN_F1" name="I" />
            <blockpin signalname="XLXN_10" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_7">
            <blockpin signalname="IN_F0" name="I" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_8">
            <blockpin signalname="IN_F1" name="I" />
            <blockpin signalname="XLXN_14" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_9">
            <blockpin signalname="XLXN_18" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="XLXN_9" name="I2" />
            <blockpin signalname="OUT_EN0" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_10">
            <blockpin signalname="XLXN_18" name="I0" />
            <blockpin signalname="IN_F1" name="I1" />
            <blockpin signalname="XLXN_11" name="I2" />
            <blockpin signalname="OUT_EN1" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_11">
            <blockpin signalname="XLXN_18" name="I0" />
            <blockpin signalname="XLXN_14" name="I1" />
            <blockpin signalname="IN_F0" name="I2" />
            <blockpin signalname="OUT_EN2" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_12">
            <blockpin signalname="XLXN_18" name="I0" />
            <blockpin signalname="IN_F1" name="I1" />
            <blockpin signalname="IN_F0" name="I2" />
            <blockpin signalname="OUT_EN3" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_13">
            <blockpin signalname="IN_F2" name="I" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_17">
            <blockpin signalname="IN_F2" name="I0" />
            <blockpin signalname="XLXN_28" name="I1" />
            <blockpin signalname="XLXN_27" name="I2" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_18">
            <blockpin signalname="IN_F2" name="I0" />
            <blockpin signalname="IN_F1" name="I1" />
            <blockpin signalname="XLXN_29" name="I2" />
            <blockpin signalname="XLXN_44" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_19">
            <blockpin signalname="IN_F2" name="I0" />
            <blockpin signalname="XLXN_30" name="I1" />
            <blockpin signalname="IN_F0" name="I2" />
            <blockpin signalname="XLXN_46" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_20">
            <blockpin signalname="IN_F2" name="I0" />
            <blockpin signalname="IN_F1" name="I1" />
            <blockpin signalname="IN_F0" name="I2" />
            <blockpin signalname="XLXN_47" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_21">
            <blockpin signalname="IN_F0" name="I" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_22">
            <blockpin signalname="IN_F1" name="I" />
            <blockpin signalname="XLXN_28" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_23">
            <blockpin signalname="IN_F0" name="I" />
            <blockpin signalname="XLXN_29" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_24">
            <blockpin signalname="IN_F1" name="I" />
            <blockpin signalname="XLXN_30" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_25">
            <blockpin signalname="XLXN_47" name="I0" />
            <blockpin signalname="XLXN_46" name="I1" />
            <blockpin signalname="XLXN_44" name="I2" />
            <blockpin signalname="XLXN_43" name="I3" />
            <blockpin signalname="OUT_EN4" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="400" y="208" name="XLXI_5" orien="R0" />
        <instance x="528" y="272" name="XLXI_6" orien="R0" />
        <instance x="528" y="368" name="XLXI_7" orien="R0" />
        <instance x="528" y="592" name="XLXI_8" orien="R0" />
        <iomarker fontsize="28" x="304" y="336" name="IN_F0" orien="R180" />
        <iomarker fontsize="28" x="304" y="560" name="IN_F1" orien="R180" />
        <branch name="OUT_EN0">
            <wire x2="1376" y1="208" y2="208" x1="1344" />
        </branch>
        <branch name="OUT_EN1">
            <wire x2="1376" y1="368" y2="368" x1="1344" />
        </branch>
        <branch name="OUT_EN2">
            <wire x2="1376" y1="528" y2="528" x1="1344" />
        </branch>
        <branch name="OUT_EN3">
            <wire x2="1376" y1="688" y2="688" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="1376" y="208" name="OUT_EN0" orien="R0" />
        <iomarker fontsize="28" x="1376" y="368" name="OUT_EN1" orien="R0" />
        <iomarker fontsize="28" x="1376" y="528" name="OUT_EN2" orien="R0" />
        <iomarker fontsize="28" x="1376" y="688" name="OUT_EN3" orien="R0" />
        <instance x="1088" y="336" name="XLXI_9" orien="R0" />
        <instance x="1088" y="496" name="XLXI_10" orien="R0" />
        <instance x="1088" y="656" name="XLXI_11" orien="R0" />
        <instance x="1088" y="816" name="XLXI_12" orien="R0" />
        <branch name="XLXN_9">
            <wire x2="800" y1="176" y2="176" x1="624" />
            <wire x2="1088" y1="144" y2="144" x1="800" />
            <wire x2="800" y1="144" y2="176" x1="800" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="800" y1="240" y2="240" x1="752" />
            <wire x2="1088" y1="208" y2="208" x1="800" />
            <wire x2="800" y1="208" y2="240" x1="800" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="800" y1="336" y2="336" x1="752" />
            <wire x2="1088" y1="304" y2="304" x1="800" />
            <wire x2="800" y1="304" y2="336" x1="800" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="800" y1="560" y2="560" x1="752" />
            <wire x2="1088" y1="528" y2="528" x1="800" />
            <wire x2="800" y1="528" y2="560" x1="800" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="928" y1="800" y2="800" x1="864" />
            <wire x2="1088" y1="272" y2="272" x1="928" />
            <wire x2="928" y1="272" y2="432" x1="928" />
            <wire x2="928" y1="432" y2="592" x1="928" />
            <wire x2="1088" y1="592" y2="592" x1="928" />
            <wire x2="928" y1="592" y2="752" x1="928" />
            <wire x2="1088" y1="752" y2="752" x1="928" />
            <wire x2="928" y1="752" y2="800" x1="928" />
            <wire x2="1088" y1="432" y2="432" x1="928" />
        </branch>
        <instance x="640" y="832" name="XLXI_13" orien="R0" />
        <iomarker fontsize="28" x="304" y="800" name="IN_F2" orien="R180" />
        <branch name="IN_F1">
            <wire x2="432" y1="560" y2="560" x1="304" />
            <wire x2="528" y1="560" y2="560" x1="432" />
            <wire x2="432" y1="560" y2="720" x1="432" />
            <wire x2="800" y1="720" y2="720" x1="432" />
            <wire x2="432" y1="720" y2="976" x1="432" />
            <wire x2="880" y1="976" y2="976" x1="432" />
            <wire x2="432" y1="976" y2="1184" x1="432" />
            <wire x2="1136" y1="1184" y2="1184" x1="432" />
            <wire x2="432" y1="1184" y2="1376" x1="432" />
            <wire x2="880" y1="1376" y2="1376" x1="432" />
            <wire x2="432" y1="1376" y2="1584" x1="432" />
            <wire x2="1136" y1="1584" y2="1584" x1="432" />
            <wire x2="528" y1="240" y2="240" x1="432" />
            <wire x2="432" y1="240" y2="400" x1="432" />
            <wire x2="800" y1="400" y2="400" x1="432" />
            <wire x2="432" y1="400" y2="560" x1="432" />
            <wire x2="1088" y1="368" y2="368" x1="800" />
            <wire x2="800" y1="368" y2="400" x1="800" />
            <wire x2="1088" y1="688" y2="688" x1="800" />
            <wire x2="800" y1="688" y2="720" x1="800" />
        </branch>
        <branch name="IN_F0">
            <wire x2="400" y1="336" y2="336" x1="304" />
            <wire x2="400" y1="336" y2="496" x1="400" />
            <wire x2="800" y1="496" y2="496" x1="400" />
            <wire x2="400" y1="496" y2="656" x1="400" />
            <wire x2="800" y1="656" y2="656" x1="400" />
            <wire x2="400" y1="656" y2="912" x1="400" />
            <wire x2="880" y1="912" y2="912" x1="400" />
            <wire x2="400" y1="912" y2="1120" x1="400" />
            <wire x2="880" y1="1120" y2="1120" x1="400" />
            <wire x2="400" y1="1120" y2="1312" x1="400" />
            <wire x2="1136" y1="1312" y2="1312" x1="400" />
            <wire x2="400" y1="1312" y2="1520" x1="400" />
            <wire x2="1136" y1="1520" y2="1520" x1="400" />
            <wire x2="528" y1="336" y2="336" x1="400" />
            <wire x2="400" y1="176" y2="336" x1="400" />
            <wire x2="1088" y1="464" y2="464" x1="800" />
            <wire x2="800" y1="464" y2="496" x1="800" />
            <wire x2="1088" y1="624" y2="624" x1="800" />
            <wire x2="800" y1="624" y2="656" x1="800" />
        </branch>
        <branch name="IN_F2">
            <wire x2="384" y1="800" y2="800" x1="304" />
            <wire x2="464" y1="800" y2="800" x1="384" />
            <wire x2="640" y1="800" y2="800" x1="464" />
            <wire x2="464" y1="800" y2="1024" x1="464" />
            <wire x2="464" y1="1024" y2="1040" x1="464" />
            <wire x2="1136" y1="1040" y2="1040" x1="464" />
            <wire x2="464" y1="1040" y2="1248" x1="464" />
            <wire x2="1136" y1="1248" y2="1248" x1="464" />
            <wire x2="464" y1="1248" y2="1440" x1="464" />
            <wire x2="1136" y1="1440" y2="1440" x1="464" />
            <wire x2="464" y1="1440" y2="1648" x1="464" />
            <wire x2="1136" y1="1648" y2="1648" x1="464" />
        </branch>
        <instance x="1136" y="1104" name="XLXI_17" orien="R0" />
        <instance x="1136" y="1312" name="XLXI_18" orien="R0" />
        <instance x="1136" y="1504" name="XLXI_19" orien="R0" />
        <instance x="1136" y="1712" name="XLXI_20" orien="R0" />
        <branch name="XLXN_27">
            <wire x2="1136" y1="912" y2="912" x1="1104" />
        </branch>
        <instance x="880" y="944" name="XLXI_21" orien="R0" />
        <branch name="XLXN_28">
            <wire x2="1136" y1="976" y2="976" x1="1104" />
        </branch>
        <instance x="880" y="1008" name="XLXI_22" orien="R0" />
        <branch name="XLXN_29">
            <wire x2="1136" y1="1120" y2="1120" x1="1104" />
        </branch>
        <instance x="880" y="1152" name="XLXI_23" orien="R0" />
        <branch name="XLXN_30">
            <wire x2="1136" y1="1376" y2="1376" x1="1104" />
        </branch>
        <instance x="880" y="1408" name="XLXI_24" orien="R0" />
        <instance x="1584" y="1440" name="XLXI_25" orien="R0" />
        <branch name="XLXN_43">
            <wire x2="1584" y1="976" y2="976" x1="1392" />
            <wire x2="1584" y1="976" y2="1184" x1="1584" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="1488" y1="1184" y2="1184" x1="1392" />
            <wire x2="1488" y1="1184" y2="1248" x1="1488" />
            <wire x2="1584" y1="1248" y2="1248" x1="1488" />
        </branch>
        <branch name="XLXN_46">
            <wire x2="1488" y1="1376" y2="1376" x1="1392" />
            <wire x2="1488" y1="1312" y2="1376" x1="1488" />
            <wire x2="1584" y1="1312" y2="1312" x1="1488" />
        </branch>
        <branch name="XLXN_47">
            <wire x2="1584" y1="1584" y2="1584" x1="1392" />
            <wire x2="1584" y1="1376" y2="1584" x1="1584" />
        </branch>
        <branch name="OUT_EN4">
            <wire x2="1872" y1="1280" y2="1280" x1="1840" />
        </branch>
        <iomarker fontsize="28" x="1872" y="1280" name="OUT_EN4" orien="R0" />
    </sheet>
</drawing>