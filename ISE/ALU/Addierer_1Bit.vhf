--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : Addierer_1Bit.vhf
-- /___/   /\     Timestamp : 11/06/2014 11:26:56
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl C:/Users/heringm/ownCloud/BA/FPGA/ISE/ALU/Addierer_1Bit.vhf -w C:/Users/heringm/ownCloud/BA/FPGA/ISE/ALU/Addierer_1Bit.sch
--Design Name: Addierer_1Bit
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Addierer_1Bit is
   port ( Add_1_A     : in    std_logic; 
          Add_1_B     : in    std_logic; 
          Add_1_En    : in    std_logic; 
          Add_1_En_B  : in    std_logic; 
          Add_1_In_C  : in    std_logic; 
          Add_1_Out_C : out   std_logic; 
          Add_1_S     : out   std_logic);
end Addierer_1Bit;

architecture BEHAVIORAL of Addierer_1Bit is
   attribute BOX_TYPE   : string ;
   signal XLXN_2      : std_logic;
   signal XLXN_8      : std_logic;
   signal XLXN_9      : std_logic;
   signal XLXN_17     : std_logic;
   signal XLXN_18     : std_logic;
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : XOR2
      port map (I0=>Add_1_In_C,
                I1=>XLXN_9,
                O=>XLXN_2);
   
   XLXI_2 : XOR2
      port map (I0=>Add_1_A,
                I1=>XLXN_8,
                O=>XLXN_9);
   
   XLXI_4 : AND3
      port map (I0=>Add_1_En,
                I1=>XLXN_8,
                I2=>Add_1_A,
                O=>XLXN_17);
   
   XLXI_5 : AND3
      port map (I0=>Add_1_En,
                I1=>XLXN_9,
                I2=>Add_1_In_C,
                O=>XLXN_18);
   
   XLXI_6 : AND2
      port map (I0=>Add_1_B,
                I1=>Add_1_En_B,
                O=>XLXN_8);
   
   XLXI_7 : AND2
      port map (I0=>Add_1_En,
                I1=>XLXN_2,
                O=>Add_1_S);
   
   XLXI_8 : OR2
      port map (I0=>XLXN_17,
                I1=>XLXN_18,
                O=>Add_1_Out_C);
   
end BEHAVIORAL;


