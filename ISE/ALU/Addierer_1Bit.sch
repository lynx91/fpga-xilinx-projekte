<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_2" />
        <signal name="XLXN_20" />
        <signal name="Add_1_En" />
        <signal name="XLXN_9" />
        <signal name="Add_1_In_C" />
        <signal name="XLXN_8" />
        <signal name="Add_1_A" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="Add_1_En_B" />
        <signal name="Add_1_B" />
        <signal name="Add_1_S" />
        <signal name="Add_1_Out_C" />
        <port polarity="Input" name="Add_1_En" />
        <port polarity="Input" name="Add_1_In_C" />
        <port polarity="Input" name="Add_1_A" />
        <port polarity="Input" name="Add_1_En_B" />
        <port polarity="Input" name="Add_1_B" />
        <port polarity="Output" name="Add_1_S" />
        <port polarity="Output" name="Add_1_Out_C" />
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="and3" name="XLXI_5">
            <blockpin signalname="Add_1_En" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="Add_1_In_C" name="I2" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_4">
            <blockpin signalname="Add_1_En" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="Add_1_A" name="I2" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="Add_1_En" name="I0" />
            <blockpin signalname="XLXN_2" name="I1" />
            <blockpin signalname="Add_1_S" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_1">
            <blockpin signalname="Add_1_In_C" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_2">
            <blockpin signalname="Add_1_A" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="Add_1_B" name="I0" />
            <blockpin signalname="Add_1_En_B" name="I1" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_8">
            <blockpin signalname="XLXN_17" name="I0" />
            <blockpin signalname="XLXN_18" name="I1" />
            <blockpin signalname="Add_1_Out_C" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="976" y="672" name="XLXI_5" orien="R90" />
        <instance x="512" y="672" name="XLXI_4" orien="R90" />
        <instance x="1744" y="624" name="XLXI_7" orien="R0" />
        <branch name="XLXN_2">
            <wire x2="1728" y1="464" y2="464" x1="1552" />
            <wire x2="1728" y1="464" y2="496" x1="1728" />
            <wire x2="1744" y1="496" y2="496" x1="1728" />
        </branch>
        <branch name="Add_1_En">
            <wire x2="576" y1="592" y2="592" x1="224" />
            <wire x2="1040" y1="592" y2="592" x1="576" />
            <wire x2="1040" y1="592" y2="672" x1="1040" />
            <wire x2="576" y1="592" y2="672" x1="576" />
            <wire x2="1744" y1="560" y2="560" x1="1040" />
            <wire x2="1040" y1="560" y2="592" x1="1040" />
        </branch>
        <instance x="1296" y="560" name="XLXI_1" orien="R0" />
        <branch name="XLXN_9">
            <wire x2="1104" y1="432" y2="432" x1="976" />
            <wire x2="1296" y1="432" y2="432" x1="1104" />
            <wire x2="1104" y1="432" y2="672" x1="1104" />
        </branch>
        <branch name="Add_1_In_C">
            <wire x2="1168" y1="240" y2="496" x1="1168" />
            <wire x2="1296" y1="496" y2="496" x1="1168" />
            <wire x2="1168" y1="496" y2="672" x1="1168" />
        </branch>
        <instance x="720" y="528" name="XLXI_2" orien="R0" />
        <branch name="XLXN_8">
            <wire x2="640" y1="336" y2="336" x1="528" />
            <wire x2="640" y1="336" y2="400" x1="640" />
            <wire x2="720" y1="400" y2="400" x1="640" />
            <wire x2="640" y1="400" y2="672" x1="640" />
        </branch>
        <branch name="Add_1_A">
            <wire x2="704" y1="464" y2="464" x1="224" />
            <wire x2="720" y1="464" y2="464" x1="704" />
            <wire x2="704" y1="464" y2="672" x1="704" />
        </branch>
        <iomarker fontsize="28" x="224" y="464" name="Add_1_A" orien="R180" />
        <iomarker fontsize="28" x="224" y="592" name="Add_1_En" orien="R180" />
        <branch name="Add_1_En_B">
            <wire x2="256" y1="304" y2="304" x1="240" />
            <wire x2="272" y1="304" y2="304" x1="256" />
        </branch>
        <branch name="Add_1_B">
            <wire x2="256" y1="368" y2="368" x1="240" />
            <wire x2="272" y1="368" y2="368" x1="256" />
        </branch>
        <instance x="272" y="432" name="XLXI_6" orien="R0" />
        <iomarker fontsize="28" x="240" y="304" name="Add_1_En_B" orien="R180" />
        <iomarker fontsize="28" x="240" y="368" name="Add_1_B" orien="R180" />
        <iomarker fontsize="28" x="1168" y="240" name="Add_1_In_C" orien="R270" />
        <branch name="Add_1_S">
            <wire x2="2032" y1="528" y2="528" x1="2000" />
        </branch>
        <iomarker fontsize="28" x="2032" y="528" name="Add_1_S" orien="R0" />
        <branch name="XLXN_18">
            <wire x2="1104" y1="944" y2="944" x1="912" />
            <wire x2="912" y1="944" y2="976" x1="912" />
            <wire x2="1104" y1="928" y2="944" x1="1104" />
        </branch>
        <instance x="784" y="976" name="XLXI_8" orien="R90" />
        <branch name="Add_1_Out_C">
            <wire x2="880" y1="1232" y2="1264" x1="880" />
        </branch>
        <iomarker fontsize="28" x="880" y="1264" name="Add_1_Out_C" orien="R90" />
        <branch name="XLXN_17">
            <wire x2="640" y1="928" y2="944" x1="640" />
            <wire x2="848" y1="944" y2="944" x1="640" />
            <wire x2="848" y1="944" y2="976" x1="848" />
        </branch>
    </sheet>
</drawing>