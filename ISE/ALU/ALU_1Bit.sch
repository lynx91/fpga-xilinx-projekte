<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="ALU_1_IN_INVA" />
        <signal name="ALU_1_IN_A" />
        <signal name="ALU_1_IN_ENA" />
        <signal name="ALU_1_IN_B" />
        <signal name="ALU_1_IN_ENB" />
        <signal name="ALU_1_IN_EN0" />
        <signal name="ALU_1_IN_EN1" />
        <signal name="ALU_1_IN_EN2" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36" />
        <signal name="ALU_1_OUT_C" />
        <signal name="ALU_1_OUT" />
        <signal name="ALU_1_IN_C" />
        <signal name="ALU_1_IN_EN3" />
        <port polarity="Input" name="ALU_1_IN_INVA" />
        <port polarity="Input" name="ALU_1_IN_A" />
        <port polarity="Input" name="ALU_1_IN_B" />
        <port polarity="Input" name="ALU_1_IN_EN0" />
        <port polarity="Input" name="ALU_1_IN_EN1" />
        <port polarity="Input" name="ALU_1_IN_EN2" />
        <port polarity="Output" name="ALU_1_OUT_C" />
        <port polarity="Output" name="ALU_1_OUT" />
        <port polarity="Input" name="ALU_1_IN_C" />
        <port polarity="Input" name="ALU_1_IN_EN3" />
        <blockdef name="Addierer_1Bit">
            <timestamp>2014-11-6T11:13:26</timestamp>
            <rect width="304" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="432" y1="-288" y2="-288" x1="368" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="Logical_Unit">
            <timestamp>2014-11-6T8:22:21</timestamp>
            <line x2="496" y1="-80" y2="-20" x1="496" />
            <line x2="400" y1="-16" y2="-80" x1="400" />
            <line x2="304" y1="-16" y2="-76" x1="304" />
            <rect width="496" x="60" y="-324" height="272" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="192" y1="-80" y2="-20" x1="192" />
            <line x2="96" y1="-80" y2="-20" x1="96" />
            <line x2="608" y1="-288" y2="-288" x1="544" />
            <line x2="608" y1="-224" y2="-224" x1="544" />
            <line x2="608" y1="-160" y2="-160" x1="544" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="Addierer_1Bit" name="XLXI_1">
            <blockpin signalname="ALU_1_IN_EN3" name="Add_1_En" />
            <blockpin signalname="ALU_1_IN_C" name="Add_1_In_C" />
            <blockpin signalname="XLXN_31" name="Add_1_A" />
            <blockpin signalname="XLXN_28" name="Add_1_En_B" />
            <blockpin signalname="XLXN_32" name="Add_1_B" />
            <blockpin signalname="XLXN_36" name="Add_1_S" />
            <blockpin signalname="ALU_1_OUT_C" name="Add_1_Out_C" />
        </block>
        <block symbolname="Logical_Unit" name="XLXI_3">
            <blockpin signalname="XLXN_29" name="IN_A" />
            <blockpin signalname="XLXN_30" name="IN_B" />
            <blockpin signalname="XLXN_31" name="OUT_A" />
            <blockpin signalname="XLXN_32" name="OUT_B" />
            <blockpin signalname="XLXN_33" name="OUT_0" />
            <blockpin signalname="XLXN_34" name="OUT_1" />
            <blockpin signalname="XLXN_35" name="OUT_2" />
            <blockpin signalname="ALU_1_IN_EN0" name="IN_EN0" />
            <blockpin signalname="ALU_1_IN_EN1" name="IN_EN1" />
            <blockpin signalname="ALU_1_IN_EN2" name="IN_EN2" />
        </block>
        <block symbolname="xor2" name="XLXI_4">
            <blockpin signalname="XLXN_1" name="I0" />
            <blockpin signalname="ALU_1_IN_INVA" name="I1" />
            <blockpin signalname="XLXN_29" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="ALU_1_IN_ENA" name="I0" />
            <blockpin signalname="ALU_1_IN_A" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="ALU_1_IN_ENB" name="I0" />
            <blockpin signalname="ALU_1_IN_B" name="I1" />
            <blockpin signalname="XLXN_30" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_10">
            <blockpin signalname="XLXN_28" name="P" />
        </block>
        <block symbolname="or4" name="XLXI_11">
            <blockpin signalname="XLXN_36" name="I0" />
            <blockpin signalname="XLXN_35" name="I1" />
            <blockpin signalname="XLXN_34" name="I2" />
            <blockpin signalname="XLXN_33" name="I3" />
            <blockpin signalname="ALU_1_OUT" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_12">
            <blockpin signalname="ALU_1_IN_ENA" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_13">
            <blockpin signalname="ALU_1_IN_ENB" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1632" y="1664" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1184" y="976" name="XLXI_3" orien="R0">
        </instance>
        <instance x="864" y="448" name="XLXI_4" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="864" y1="384" y2="384" x1="832" />
        </branch>
        <instance x="576" y="480" name="XLXI_5" orien="R0" />
        <instance x="576" y="624" name="XLXI_6" orien="R0" />
        <branch name="ALU_1_IN_INVA">
            <wire x2="848" y1="288" y2="288" x1="832" />
            <wire x2="848" y1="288" y2="320" x1="848" />
            <wire x2="864" y1="320" y2="320" x1="848" />
        </branch>
        <branch name="ALU_1_IN_A">
            <wire x2="576" y1="352" y2="352" x1="544" />
        </branch>
        <iomarker fontsize="28" x="544" y="352" name="ALU_1_IN_A" orien="R180" />
        <branch name="ALU_1_IN_ENA">
            <wire x2="576" y1="416" y2="416" x1="544" />
        </branch>
        <branch name="ALU_1_IN_B">
            <wire x2="576" y1="496" y2="496" x1="544" />
        </branch>
        <iomarker fontsize="28" x="544" y="496" name="ALU_1_IN_B" orien="R180" />
        <branch name="ALU_1_IN_ENB">
            <wire x2="576" y1="560" y2="560" x1="544" />
        </branch>
        <branch name="ALU_1_IN_EN0">
            <wire x2="1488" y1="1200" y2="1200" x1="1024" />
            <wire x2="1488" y1="960" y2="1200" x1="1488" />
        </branch>
        <branch name="ALU_1_IN_EN1">
            <wire x2="1584" y1="1264" y2="1264" x1="1024" />
            <wire x2="1584" y1="960" y2="1264" x1="1584" />
        </branch>
        <branch name="ALU_1_IN_EN2">
            <wire x2="1680" y1="1328" y2="1328" x1="1024" />
            <wire x2="1680" y1="960" y2="1328" x1="1680" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="1632" y1="1568" y2="1568" x1="1600" />
        </branch>
        <instance x="1600" y="1632" name="XLXI_10" orien="R270" />
        <branch name="XLXN_29">
            <wire x2="1152" y1="352" y2="352" x1="1120" />
            <wire x2="1152" y1="352" y2="688" x1="1152" />
            <wire x2="1184" y1="688" y2="688" x1="1152" />
        </branch>
        <branch name="XLXN_30">
            <wire x2="1008" y1="528" y2="528" x1="832" />
            <wire x2="1008" y1="528" y2="752" x1="1008" />
            <wire x2="1184" y1="752" y2="752" x1="1008" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="1280" y1="960" y2="1504" x1="1280" />
            <wire x2="1632" y1="1504" y2="1504" x1="1280" />
        </branch>
        <branch name="XLXN_32">
            <wire x2="1376" y1="960" y2="1632" x1="1376" />
            <wire x2="1632" y1="1632" y2="1632" x1="1376" />
        </branch>
        <instance x="2256" y="1024" name="XLXI_11" orien="R0" />
        <branch name="XLXN_33">
            <wire x2="2256" y1="688" y2="688" x1="1792" />
            <wire x2="2256" y1="688" y2="768" x1="2256" />
        </branch>
        <branch name="XLXN_34">
            <wire x2="2016" y1="752" y2="752" x1="1792" />
            <wire x2="2016" y1="752" y2="832" x1="2016" />
            <wire x2="2256" y1="832" y2="832" x1="2016" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="2000" y1="816" y2="816" x1="1792" />
            <wire x2="2000" y1="816" y2="896" x1="2000" />
            <wire x2="2256" y1="896" y2="896" x1="2000" />
        </branch>
        <branch name="XLXN_36">
            <wire x2="2256" y1="1376" y2="1376" x1="2064" />
            <wire x2="2256" y1="960" y2="1376" x1="2256" />
        </branch>
        <branch name="ALU_1_OUT_C">
            <wire x2="2096" y1="1632" y2="1632" x1="2064" />
        </branch>
        <iomarker fontsize="28" x="2096" y="1632" name="ALU_1_OUT_C" orien="R0" />
        <branch name="ALU_1_OUT">
            <wire x2="2544" y1="864" y2="864" x1="2512" />
        </branch>
        <iomarker fontsize="28" x="2544" y="864" name="ALU_1_OUT" orien="R0" />
        <instance x="544" y="480" name="XLXI_12" orien="R270" />
        <instance x="544" y="624" name="XLXI_13" orien="R270" />
        <iomarker fontsize="28" x="832" y="288" name="ALU_1_IN_INVA" orien="R180" />
        <iomarker fontsize="28" x="1024" y="1200" name="ALU_1_IN_EN0" orien="R180" />
        <iomarker fontsize="28" x="1024" y="1264" name="ALU_1_IN_EN1" orien="R180" />
        <iomarker fontsize="28" x="1024" y="1328" name="ALU_1_IN_EN2" orien="R180" />
        <iomarker fontsize="28" x="1024" y="1392" name="ALU_1_IN_EN3" orien="R180" />
        <branch name="ALU_1_IN_C">
            <wire x2="1632" y1="1440" y2="1440" x1="1600" />
        </branch>
        <iomarker fontsize="28" x="1600" y="1440" name="ALU_1_IN_C" orien="R180" />
        <branch name="ALU_1_IN_EN3">
            <wire x2="1328" y1="1392" y2="1392" x1="1024" />
            <wire x2="1632" y1="1376" y2="1376" x1="1328" />
            <wire x2="1328" y1="1376" y2="1392" x1="1328" />
        </branch>
    </sheet>
</drawing>