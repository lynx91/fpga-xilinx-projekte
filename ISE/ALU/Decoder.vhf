--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : Decoder.vhf
-- /___/   /\     Timestamp : 11/06/2014 13:14:58
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl C:/Users/heringm/ownCloud/BA/FPGA/ISE/ALU/Decoder.vhf -w C:/Users/heringm/ownCloud/BA/FPGA/ISE/ALU/Decoder.sch
--Design Name: Decoder
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity Decoder is
   port ( IN_F0   : in    std_logic; 
          IN_F1   : in    std_logic; 
          IN_F2   : in    std_logic; 
          OUT_EN0 : out   std_logic; 
          OUT_EN1 : out   std_logic; 
          OUT_EN2 : out   std_logic; 
          OUT_EN3 : out   std_logic; 
          OUT_EN4 : out   std_logic);
end Decoder;

architecture BEHAVIORAL of Decoder is
   attribute BOX_TYPE   : string ;
   signal XLXN_9  : std_logic;
   signal XLXN_10 : std_logic;
   signal XLXN_11 : std_logic;
   signal XLXN_14 : std_logic;
   signal XLXN_18 : std_logic;
   signal XLXN_27 : std_logic;
   signal XLXN_28 : std_logic;
   signal XLXN_29 : std_logic;
   signal XLXN_30 : std_logic;
   signal XLXN_43 : std_logic;
   signal XLXN_44 : std_logic;
   signal XLXN_46 : std_logic;
   signal XLXN_47 : std_logic;
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : component is "BLACK_BOX";
   
begin
   XLXI_5 : INV
      port map (I=>IN_F0,
                O=>XLXN_9);
   
   XLXI_6 : INV
      port map (I=>IN_F1,
                O=>XLXN_10);
   
   XLXI_7 : INV
      port map (I=>IN_F0,
                O=>XLXN_11);
   
   XLXI_8 : INV
      port map (I=>IN_F1,
                O=>XLXN_14);
   
   XLXI_9 : AND3
      port map (I0=>XLXN_18,
                I1=>XLXN_10,
                I2=>XLXN_9,
                O=>OUT_EN0);
   
   XLXI_10 : AND3
      port map (I0=>XLXN_18,
                I1=>IN_F1,
                I2=>XLXN_11,
                O=>OUT_EN1);
   
   XLXI_11 : AND3
      port map (I0=>XLXN_18,
                I1=>XLXN_14,
                I2=>IN_F0,
                O=>OUT_EN2);
   
   XLXI_12 : AND3
      port map (I0=>XLXN_18,
                I1=>IN_F1,
                I2=>IN_F0,
                O=>OUT_EN3);
   
   XLXI_13 : INV
      port map (I=>IN_F2,
                O=>XLXN_18);
   
   XLXI_17 : AND3
      port map (I0=>IN_F2,
                I1=>XLXN_28,
                I2=>XLXN_27,
                O=>XLXN_43);
   
   XLXI_18 : AND3
      port map (I0=>IN_F2,
                I1=>IN_F1,
                I2=>XLXN_29,
                O=>XLXN_44);
   
   XLXI_19 : AND3
      port map (I0=>IN_F2,
                I1=>XLXN_30,
                I2=>IN_F0,
                O=>XLXN_46);
   
   XLXI_20 : AND3
      port map (I0=>IN_F2,
                I1=>IN_F1,
                I2=>IN_F0,
                O=>XLXN_47);
   
   XLXI_21 : INV
      port map (I=>IN_F0,
                O=>XLXN_27);
   
   XLXI_22 : INV
      port map (I=>IN_F1,
                O=>XLXN_28);
   
   XLXI_23 : INV
      port map (I=>IN_F0,
                O=>XLXN_29);
   
   XLXI_24 : INV
      port map (I=>IN_F1,
                O=>XLXN_30);
   
   XLXI_25 : OR4
      port map (I0=>XLXN_47,
                I1=>XLXN_46,
                I2=>XLXN_44,
                I3=>XLXN_43,
                O=>OUT_EN4);
   
end BEHAVIORAL;


