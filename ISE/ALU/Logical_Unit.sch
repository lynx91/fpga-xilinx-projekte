<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="IN_EN2" />
        <signal name="IN_EN1" />
        <signal name="IN_EN0" />
        <signal name="IN_A" />
        <signal name="XLXN_8" />
        <signal name="IN_B" />
        <signal name="OUT_A" />
        <signal name="OUT_B" />
        <signal name="OUT_0" />
        <signal name="OUT_1" />
        <signal name="OUT_2" />
        <port polarity="Input" name="IN_EN2" />
        <port polarity="Input" name="IN_EN1" />
        <port polarity="Input" name="IN_EN0" />
        <port polarity="Input" name="IN_A" />
        <port polarity="Input" name="IN_B" />
        <port polarity="Output" name="OUT_A" />
        <port polarity="Output" name="OUT_B" />
        <port polarity="Output" name="OUT_0" />
        <port polarity="Output" name="OUT_1" />
        <port polarity="Output" name="OUT_2" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="IN_B" name="I0" />
            <blockpin signalname="IN_A" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="IN_EN0" name="I0" />
            <blockpin signalname="XLXN_1" name="I1" />
            <blockpin signalname="OUT_0" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="IN_EN1" name="I0" />
            <blockpin signalname="XLXN_2" name="I1" />
            <blockpin signalname="OUT_1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="IN_EN2" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="OUT_2" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_5">
            <blockpin signalname="IN_B" name="I0" />
            <blockpin signalname="IN_A" name="I1" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="IN_B" name="I" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_7">
            <blockpin signalname="IN_A" name="I" />
            <blockpin signalname="OUT_A" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_8">
            <blockpin signalname="IN_B" name="I" />
            <blockpin signalname="OUT_B" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="496" y="288" name="XLXI_1" orien="R0" />
        <instance x="1008" y="320" name="XLXI_2" orien="R0" />
        <instance x="1008" y="480" name="XLXI_3" orien="R0" />
        <instance x="1008" y="640" name="XLXI_4" orien="R0" />
        <instance x="496" y="448" name="XLXI_5" orien="R0" />
        <instance x="496" y="544" name="XLXI_6" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="1008" y1="192" y2="192" x1="752" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="1008" y1="352" y2="352" x1="752" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1008" y1="512" y2="512" x1="720" />
        </branch>
        <branch name="IN_EN2">
            <wire x2="1008" y1="576" y2="576" x1="960" />
            <wire x2="960" y1="576" y2="752" x1="960" />
        </branch>
        <branch name="IN_EN1">
            <wire x2="1008" y1="416" y2="416" x1="912" />
            <wire x2="912" y1="416" y2="752" x1="912" />
        </branch>
        <branch name="IN_EN0">
            <wire x2="1008" y1="256" y2="256" x1="864" />
            <wire x2="864" y1="256" y2="752" x1="864" />
        </branch>
        <branch name="IN_A">
            <wire x2="368" y1="320" y2="320" x1="288" />
            <wire x2="496" y1="320" y2="320" x1="368" />
            <wire x2="368" y1="320" y2="624" x1="368" />
            <wire x2="496" y1="160" y2="160" x1="368" />
            <wire x2="368" y1="160" y2="320" x1="368" />
        </branch>
        <branch name="IN_B">
            <wire x2="432" y1="512" y2="512" x1="288" />
            <wire x2="496" y1="512" y2="512" x1="432" />
            <wire x2="432" y1="512" y2="624" x1="432" />
            <wire x2="496" y1="224" y2="224" x1="432" />
            <wire x2="432" y1="224" y2="384" x1="432" />
            <wire x2="496" y1="384" y2="384" x1="432" />
            <wire x2="432" y1="384" y2="512" x1="432" />
        </branch>
        <iomarker fontsize="28" x="288" y="320" name="IN_A" orien="R180" />
        <iomarker fontsize="28" x="288" y="512" name="IN_B" orien="R180" />
        <instance x="400" y="624" name="XLXI_8" orien="R90" />
        <instance x="336" y="624" name="XLXI_7" orien="R90" />
        <branch name="OUT_A">
            <wire x2="368" y1="848" y2="880" x1="368" />
        </branch>
        <iomarker fontsize="28" x="368" y="880" name="OUT_A" orien="R90" />
        <branch name="OUT_B">
            <wire x2="432" y1="848" y2="880" x1="432" />
        </branch>
        <iomarker fontsize="28" x="432" y="880" name="OUT_B" orien="R90" />
        <iomarker fontsize="28" x="864" y="752" name="IN_EN0" orien="R90" />
        <iomarker fontsize="28" x="912" y="752" name="IN_EN1" orien="R90" />
        <iomarker fontsize="28" x="960" y="752" name="IN_EN2" orien="R90" />
        <branch name="OUT_0">
            <wire x2="1296" y1="224" y2="224" x1="1264" />
        </branch>
        <iomarker fontsize="28" x="1296" y="224" name="OUT_0" orien="R0" />
        <branch name="OUT_1">
            <wire x2="1296" y1="384" y2="384" x1="1264" />
        </branch>
        <iomarker fontsize="28" x="1296" y="384" name="OUT_1" orien="R0" />
        <branch name="OUT_2">
            <wire x2="1296" y1="544" y2="544" x1="1264" />
        </branch>
        <iomarker fontsize="28" x="1296" y="544" name="OUT_2" orien="R0" />
    </sheet>
</drawing>