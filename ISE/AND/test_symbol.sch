<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="IN_C" />
        <signal name="IN_A" />
        <signal name="IN_B" />
        <signal name="OUT_Y" />
        <port polarity="Input" name="IN_C" />
        <port polarity="Input" name="IN_A" />
        <port polarity="Input" name="IN_B" />
        <port polarity="Output" name="OUT_Y" />
        <blockdef name="und">
            <timestamp>2014-10-23T9:8:2</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <block symbolname="und" name="XLXI_1">
            <blockpin signalname="IN_C" name="C" />
            <blockpin signalname="IN_A" name="A" />
            <blockpin signalname="IN_B" name="B" />
            <blockpin signalname="OUT_Y" name="Y" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="816" y="1056" name="XLXI_1" orien="R0">
        </instance>
        <branch name="IN_C">
            <wire x2="816" y1="896" y2="896" x1="784" />
        </branch>
        <iomarker fontsize="28" x="784" y="896" name="IN_C" orien="R180" />
        <branch name="IN_A">
            <wire x2="816" y1="960" y2="960" x1="784" />
        </branch>
        <iomarker fontsize="28" x="784" y="960" name="IN_A" orien="R180" />
        <branch name="IN_B">
            <wire x2="816" y1="1024" y2="1024" x1="784" />
        </branch>
        <iomarker fontsize="28" x="784" y="1024" name="IN_B" orien="R180" />
        <branch name="OUT_Y">
            <wire x2="1232" y1="896" y2="896" x1="1200" />
        </branch>
        <iomarker fontsize="28" x="1232" y="896" name="OUT_Y" orien="R0" />
    </sheet>
</drawing>