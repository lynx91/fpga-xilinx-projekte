--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : und.vhf
-- /___/   /\     Timestamp : 10/23/2014 11:00:36
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl C:/Users/heringm/ownCloud/BA/FPGA/ISE/AND/und.vhf -w C:/Users/heringm/ownCloud/BA/FPGA/ISE/AND/und.sch
--Design Name: und
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity und is
   port ( A : in    std_logic; 
          B : in    std_logic; 
          C : in    std_logic; 
          Y : out   std_logic);
end und;

architecture BEHAVIORAL of und is
   attribute BOX_TYPE   : string ;
   signal XLXN_2 : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   XLXI_1 : AND2
      port map (I0=>B,
                I1=>A,
                O=>XLXN_2);
   
   XLXI_2 : OR2
      port map (I0=>C,
                I1=>XLXN_2,
                O=>Y);
   
end BEHAVIORAL;


