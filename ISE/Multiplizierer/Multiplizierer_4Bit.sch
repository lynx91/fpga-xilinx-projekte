<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_179" />
        <signal name="XLXN_180" />
        <signal name="XLXN_181" />
        <signal name="XLXN_182" />
        <signal name="MUL_4_Y0" />
        <signal name="MUL_4_X0" />
        <signal name="MUL_4_X1" />
        <signal name="MUL_4_X2" />
        <signal name="MUL_4_X3" />
        <signal name="MUL_4_En" />
        <signal name="MUL_4_K3" />
        <signal name="MUL_4_K2" />
        <signal name="MUL_4_K1" />
        <signal name="MUL_4_K0" />
        <signal name="MUL_4_P0" />
        <signal name="MUL_4_Y1" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="XLXN_21" />
        <signal name="MUL_4_P1" />
        <signal name="MUL_4_Y2" />
        <signal name="MUL_4_P2" />
        <signal name="MUL_4_Y3" />
        <signal name="MUL_4_P3" />
        <signal name="MUL_4_P4" />
        <signal name="MUL_4_P5" />
        <signal name="MUL_4_P6" />
        <signal name="MUL_4_P7" />
        <signal name="XLXN_287" />
        <signal name="XLXN_293" />
        <signal name="XLXN_297" />
        <signal name="XLXN_298" />
        <signal name="XLXN_299" />
        <signal name="XLXN_300" />
        <signal name="XLXN_301" />
        <port polarity="Input" name="MUL_4_Y0" />
        <port polarity="Input" name="MUL_4_X0" />
        <port polarity="Input" name="MUL_4_X1" />
        <port polarity="Input" name="MUL_4_X2" />
        <port polarity="Input" name="MUL_4_X3" />
        <port polarity="Input" name="MUL_4_En" />
        <port polarity="Input" name="MUL_4_K3" />
        <port polarity="Input" name="MUL_4_K2" />
        <port polarity="Input" name="MUL_4_K1" />
        <port polarity="Input" name="MUL_4_K0" />
        <port polarity="Output" name="MUL_4_P0" />
        <port polarity="Input" name="MUL_4_Y1" />
        <port polarity="Output" name="MUL_4_P1" />
        <port polarity="Input" name="MUL_4_Y2" />
        <port polarity="Output" name="MUL_4_P2" />
        <port polarity="Input" name="MUL_4_Y3" />
        <port polarity="Output" name="MUL_4_P3" />
        <port polarity="Output" name="MUL_4_P4" />
        <port polarity="Output" name="MUL_4_P5" />
        <port polarity="Output" name="MUL_4_P6" />
        <port polarity="Output" name="MUL_4_P7" />
        <blockdef name="Addierer_4bit">
            <timestamp>2014-10-29T9:24:18</timestamp>
            <rect width="304" x="64" y="-704" height="704" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <line x2="432" y1="-224" y2="-224" x1="368" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
            <line x2="432" y1="-96" y2="-96" x1="368" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="432" y1="-352" y2="-352" x1="368" />
            <line x2="368" y1="-656" y2="-656" x1="432" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="Addierer_4bit" name="XLXI_1">
            <blockpin signalname="XLXN_182" name="Add_4_B3" />
            <blockpin signalname="XLXN_5" name="Add_4_Out_C" />
            <blockpin signalname="XLXN_181" name="Add_4_B2" />
            <blockpin signalname="XLXN_180" name="Add_4_B1" />
            <blockpin signalname="XLXN_179" name="Add_4_B0" />
            <blockpin signalname="MUL_4_K0" name="Add_4_A0" />
            <blockpin signalname="MUL_4_K1" name="Add_4_A1" />
            <blockpin signalname="MUL_4_K2" name="Add_4_A2" />
            <blockpin signalname="MUL_4_K3" name="Add_4_A3" />
            <blockpin signalname="XLXN_293" name="Add_4_En" />
            <blockpin signalname="XLXN_8" name="Add_4_S_1" />
            <blockpin signalname="XLXN_7" name="Add_4_S2" />
            <blockpin signalname="XLXN_6" name="Add_4_S3" />
            <blockpin signalname="XLXN_297" name="Add_4_In_C" />
            <blockpin signalname="MUL_4_P0" name="Add_4_S0" />
            <blockpin signalname="MUL_4_Y0" name="Add_4_En_B" />
        </block>
        <block symbolname="buf" name="XLXI_27">
            <blockpin signalname="MUL_4_X0" name="I" />
            <blockpin signalname="XLXN_179" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_28">
            <blockpin signalname="MUL_4_X1" name="I" />
            <blockpin signalname="XLXN_180" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_29">
            <blockpin signalname="MUL_4_X2" name="I" />
            <blockpin signalname="XLXN_181" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_30">
            <blockpin signalname="MUL_4_X3" name="I" />
            <blockpin signalname="XLXN_182" name="O" />
        </block>
        <block symbolname="Addierer_4bit" name="XLXI_2">
            <blockpin signalname="XLXN_182" name="Add_4_B3" />
            <blockpin signalname="XLXN_21" name="Add_4_Out_C" />
            <blockpin signalname="XLXN_181" name="Add_4_B2" />
            <blockpin signalname="XLXN_180" name="Add_4_B1" />
            <blockpin signalname="XLXN_179" name="Add_4_B0" />
            <blockpin signalname="XLXN_8" name="Add_4_A0" />
            <blockpin signalname="XLXN_7" name="Add_4_A1" />
            <blockpin signalname="XLXN_6" name="Add_4_A2" />
            <blockpin signalname="XLXN_5" name="Add_4_A3" />
            <blockpin signalname="XLXN_293" name="Add_4_En" />
            <blockpin signalname="XLXN_18" name="Add_4_S_1" />
            <blockpin signalname="XLXN_19" name="Add_4_S2" />
            <blockpin signalname="XLXN_20" name="Add_4_S3" />
            <blockpin signalname="XLXN_297" name="Add_4_In_C" />
            <blockpin signalname="MUL_4_P1" name="Add_4_S0" />
            <blockpin signalname="MUL_4_Y1" name="Add_4_En_B" />
        </block>
        <block symbolname="Addierer_4bit" name="XLXI_3">
            <blockpin signalname="XLXN_182" name="Add_4_B3" />
            <blockpin signalname="XLXN_298" name="Add_4_Out_C" />
            <blockpin signalname="XLXN_181" name="Add_4_B2" />
            <blockpin signalname="XLXN_180" name="Add_4_B1" />
            <blockpin signalname="XLXN_179" name="Add_4_B0" />
            <blockpin signalname="XLXN_18" name="Add_4_A0" />
            <blockpin signalname="XLXN_19" name="Add_4_A1" />
            <blockpin signalname="XLXN_20" name="Add_4_A2" />
            <blockpin signalname="XLXN_21" name="Add_4_A3" />
            <blockpin signalname="XLXN_293" name="Add_4_En" />
            <blockpin signalname="XLXN_301" name="Add_4_S_1" />
            <blockpin signalname="XLXN_300" name="Add_4_S2" />
            <blockpin signalname="XLXN_299" name="Add_4_S3" />
            <blockpin signalname="XLXN_297" name="Add_4_In_C" />
            <blockpin signalname="MUL_4_P2" name="Add_4_S0" />
            <blockpin signalname="MUL_4_Y2" name="Add_4_En_B" />
        </block>
        <block symbolname="Addierer_4bit" name="XLXI_4">
            <blockpin signalname="XLXN_182" name="Add_4_B3" />
            <blockpin signalname="MUL_4_P7" name="Add_4_Out_C" />
            <blockpin signalname="XLXN_181" name="Add_4_B2" />
            <blockpin signalname="XLXN_180" name="Add_4_B1" />
            <blockpin signalname="XLXN_179" name="Add_4_B0" />
            <blockpin signalname="XLXN_301" name="Add_4_A0" />
            <blockpin signalname="XLXN_300" name="Add_4_A1" />
            <blockpin signalname="XLXN_299" name="Add_4_A2" />
            <blockpin signalname="XLXN_298" name="Add_4_A3" />
            <blockpin signalname="XLXN_293" name="Add_4_En" />
            <blockpin signalname="MUL_4_P4" name="Add_4_S_1" />
            <blockpin signalname="MUL_4_P5" name="Add_4_S2" />
            <blockpin signalname="MUL_4_P6" name="Add_4_S3" />
            <blockpin signalname="XLXN_297" name="Add_4_In_C" />
            <blockpin signalname="MUL_4_P3" name="Add_4_S0" />
            <blockpin signalname="MUL_4_Y3" name="Add_4_En_B" />
        </block>
        <block symbolname="buf" name="XLXI_44">
            <blockpin signalname="MUL_4_En" name="I" />
            <blockpin signalname="XLXN_293" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_45">
            <blockpin signalname="XLXN_287" name="I" />
            <blockpin signalname="XLXN_297" name="O" />
        </block>
        <block symbolname="gnd" name="XLXI_46">
            <blockpin signalname="XLXN_287" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <branch name="XLXN_5">
            <wire x2="3600" y1="960" y2="1280" x1="3600" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="3664" y1="960" y2="1280" x1="3664" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="3728" y1="960" y2="1280" x1="3728" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="3792" y1="960" y2="1280" x1="3792" />
        </branch>
        <instance x="3568" y="528" name="XLXI_1" orien="R90">
        </instance>
        <branch name="MUL_4_Y0">
            <wire x2="4224" y1="960" y2="992" x1="4224" />
        </branch>
        <iomarker fontsize="28" x="4224" y="992" name="MUL_4_Y0" orien="R90" />
        <instance x="1536" y="208" name="XLXI_27" orien="R0" />
        <instance x="1536" y="272" name="XLXI_28" orien="R0" />
        <instance x="1536" y="336" name="XLXI_29" orien="R0" />
        <instance x="1536" y="400" name="XLXI_30" orien="R0" />
        <branch name="MUL_4_X0">
            <wire x2="1536" y1="176" y2="176" x1="1504" />
        </branch>
        <iomarker fontsize="28" x="1504" y="176" name="MUL_4_X0" orien="R180" />
        <branch name="MUL_4_X1">
            <wire x2="1536" y1="240" y2="240" x1="1504" />
        </branch>
        <iomarker fontsize="28" x="1504" y="240" name="MUL_4_X1" orien="R180" />
        <branch name="MUL_4_X2">
            <wire x2="1536" y1="304" y2="304" x1="1504" />
        </branch>
        <iomarker fontsize="28" x="1504" y="304" name="MUL_4_X2" orien="R180" />
        <branch name="MUL_4_X3">
            <wire x2="1536" y1="368" y2="368" x1="1504" />
        </branch>
        <iomarker fontsize="28" x="1504" y="368" name="MUL_4_X3" orien="R180" />
        <branch name="XLXN_179">
            <wire x2="2640" y1="176" y2="176" x1="1760" />
            <wire x2="3024" y1="176" y2="176" x1="2640" />
            <wire x2="3408" y1="176" y2="176" x1="3024" />
            <wire x2="3792" y1="176" y2="176" x1="3408" />
            <wire x2="3792" y1="176" y2="528" x1="3792" />
            <wire x2="3408" y1="176" y2="1280" x1="3408" />
            <wire x2="3024" y1="176" y2="1952" x1="3024" />
            <wire x2="2640" y1="176" y2="2736" x1="2640" />
        </branch>
        <branch name="XLXN_180">
            <wire x2="2576" y1="240" y2="240" x1="1760" />
            <wire x2="2960" y1="240" y2="240" x1="2576" />
            <wire x2="3344" y1="240" y2="240" x1="2960" />
            <wire x2="3728" y1="240" y2="240" x1="3344" />
            <wire x2="3728" y1="240" y2="528" x1="3728" />
            <wire x2="3344" y1="240" y2="1280" x1="3344" />
            <wire x2="2960" y1="240" y2="1952" x1="2960" />
            <wire x2="2576" y1="240" y2="2736" x1="2576" />
        </branch>
        <branch name="XLXN_181">
            <wire x2="2512" y1="304" y2="304" x1="1760" />
            <wire x2="2896" y1="304" y2="304" x1="2512" />
            <wire x2="3280" y1="304" y2="304" x1="2896" />
            <wire x2="3664" y1="304" y2="304" x1="3280" />
            <wire x2="3664" y1="304" y2="528" x1="3664" />
            <wire x2="3280" y1="304" y2="1280" x1="3280" />
            <wire x2="2896" y1="304" y2="1952" x1="2896" />
            <wire x2="2512" y1="304" y2="2736" x1="2512" />
        </branch>
        <branch name="XLXN_182">
            <wire x2="2448" y1="368" y2="368" x1="1760" />
            <wire x2="2832" y1="368" y2="368" x1="2448" />
            <wire x2="3216" y1="368" y2="368" x1="2832" />
            <wire x2="3600" y1="368" y2="368" x1="3216" />
            <wire x2="3600" y1="368" y2="528" x1="3600" />
            <wire x2="3216" y1="368" y2="1280" x1="3216" />
            <wire x2="2832" y1="368" y2="1952" x1="2832" />
            <wire x2="2448" y1="368" y2="2736" x1="2448" />
        </branch>
        <branch name="MUL_4_K3">
            <wire x2="3984" y1="496" y2="528" x1="3984" />
        </branch>
        <iomarker fontsize="28" x="3984" y="496" name="MUL_4_K3" orien="R270" />
        <branch name="MUL_4_K2">
            <wire x2="4048" y1="496" y2="528" x1="4048" />
        </branch>
        <iomarker fontsize="28" x="4048" y="496" name="MUL_4_K2" orien="R270" />
        <branch name="MUL_4_K1">
            <wire x2="4112" y1="496" y2="528" x1="4112" />
        </branch>
        <iomarker fontsize="28" x="4112" y="496" name="MUL_4_K1" orien="R270" />
        <branch name="MUL_4_K0">
            <wire x2="4176" y1="496" y2="528" x1="4176" />
        </branch>
        <iomarker fontsize="28" x="4176" y="496" name="MUL_4_K0" orien="R270" />
        <branch name="MUL_4_P0">
            <wire x2="3920" y1="960" y2="992" x1="3920" />
        </branch>
        <iomarker fontsize="28" x="3920" y="992" name="MUL_4_P0" orien="R90" />
        <branch name="MUL_4_Y1">
            <wire x2="3840" y1="1712" y2="1760" x1="3840" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="3408" y1="1712" y2="1952" x1="3408" />
        </branch>
        <branch name="XLXN_19">
            <wire x2="3344" y1="1712" y2="1952" x1="3344" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="3280" y1="1712" y2="1952" x1="3280" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="3216" y1="1712" y2="1952" x1="3216" />
        </branch>
        <branch name="MUL_4_P1">
            <wire x2="3536" y1="1712" y2="1744" x1="3536" />
        </branch>
        <instance x="3184" y="1280" name="XLXI_2" orien="R90">
        </instance>
        <iomarker fontsize="28" x="3840" y="1760" name="MUL_4_Y1" orien="R90" />
        <iomarker fontsize="28" x="3536" y="1744" name="MUL_4_P1" orien="R90" />
        <branch name="MUL_4_Y2">
            <wire x2="3456" y1="2384" y2="2512" x1="3456" />
        </branch>
        <instance x="2800" y="1952" name="XLXI_3" orien="R90">
        </instance>
        <branch name="MUL_4_P2">
            <wire x2="3152" y1="2384" y2="2416" x1="3152" />
        </branch>
        <iomarker fontsize="28" x="3456" y="2512" name="MUL_4_Y2" orien="R90" />
        <iomarker fontsize="28" x="3152" y="2416" name="MUL_4_P2" orien="R90" />
        <branch name="MUL_4_Y3">
            <wire x2="3072" y1="3168" y2="3184" x1="3072" />
        </branch>
        <branch name="MUL_4_P3">
            <wire x2="2768" y1="3168" y2="3200" x1="2768" />
        </branch>
        <branch name="MUL_4_P4">
            <wire x2="2640" y1="3168" y2="3200" x1="2640" />
        </branch>
        <branch name="MUL_4_P5">
            <wire x2="2576" y1="3168" y2="3200" x1="2576" />
        </branch>
        <branch name="MUL_4_P6">
            <wire x2="2512" y1="3168" y2="3200" x1="2512" />
        </branch>
        <branch name="MUL_4_P7">
            <wire x2="2448" y1="3168" y2="3200" x1="2448" />
        </branch>
        <instance x="2416" y="2736" name="XLXI_4" orien="R90">
        </instance>
        <iomarker fontsize="28" x="3072" y="3184" name="MUL_4_Y3" orien="R90" />
        <iomarker fontsize="28" x="2768" y="3200" name="MUL_4_P3" orien="R90" />
        <iomarker fontsize="28" x="2640" y="3200" name="MUL_4_P4" orien="R90" />
        <iomarker fontsize="28" x="2576" y="3200" name="MUL_4_P5" orien="R90" />
        <iomarker fontsize="28" x="2512" y="3200" name="MUL_4_P6" orien="R90" />
        <iomarker fontsize="28" x="2448" y="3200" name="MUL_4_P7" orien="R90" />
        <instance x="4608" y="3152" name="XLXI_44" orien="R270" />
        <instance x="4832" y="3152" name="XLXI_45" orien="R270" />
        <branch name="XLXN_287">
            <wire x2="4800" y1="3152" y2="3184" x1="4800" />
        </branch>
        <instance x="4736" y="3312" name="XLXI_46" orien="R0" />
        <branch name="MUL_4_En">
            <wire x2="4576" y1="3152" y2="3184" x1="4576" />
        </branch>
        <iomarker fontsize="28" x="4576" y="3184" name="MUL_4_En" orien="R90" />
        <branch name="XLXN_293">
            <wire x2="3088" y1="2672" y2="2736" x1="3088" />
            <wire x2="3184" y1="2672" y2="2672" x1="3088" />
            <wire x2="3184" y1="2672" y2="2736" x1="3184" />
            <wire x2="4576" y1="2736" y2="2736" x1="3184" />
            <wire x2="4576" y1="2736" y2="2928" x1="4576" />
            <wire x2="3472" y1="1936" y2="1952" x1="3472" />
            <wire x2="3568" y1="1936" y2="1936" x1="3472" />
            <wire x2="3568" y1="1936" y2="1952" x1="3568" />
            <wire x2="4576" y1="1952" y2="1952" x1="3568" />
            <wire x2="4576" y1="1952" y2="2736" x1="4576" />
            <wire x2="3856" y1="1200" y2="1280" x1="3856" />
            <wire x2="3952" y1="1200" y2="1200" x1="3856" />
            <wire x2="3952" y1="1200" y2="1280" x1="3952" />
            <wire x2="4576" y1="1280" y2="1280" x1="3952" />
            <wire x2="4576" y1="1280" y2="1952" x1="4576" />
            <wire x2="4240" y1="448" y2="528" x1="4240" />
            <wire x2="4336" y1="448" y2="448" x1="4240" />
            <wire x2="4336" y1="448" y2="528" x1="4336" />
            <wire x2="4576" y1="528" y2="528" x1="4336" />
            <wire x2="4576" y1="528" y2="1280" x1="4576" />
        </branch>
        <branch name="XLXN_297">
            <wire x2="2768" y1="2448" y2="2736" x1="2768" />
            <wire x2="3568" y1="2448" y2="2448" x1="2768" />
            <wire x2="3152" y1="1856" y2="1952" x1="3152" />
            <wire x2="3952" y1="1856" y2="1856" x1="3152" />
            <wire x2="3536" y1="1072" y2="1280" x1="3536" />
            <wire x2="4336" y1="1072" y2="1072" x1="3536" />
            <wire x2="4800" y1="2384" y2="2384" x1="3568" />
            <wire x2="4800" y1="2384" y2="2928" x1="4800" />
            <wire x2="3568" y1="2384" y2="2448" x1="3568" />
            <wire x2="4800" y1="192" y2="192" x1="3920" />
            <wire x2="4800" y1="192" y2="960" x1="4800" />
            <wire x2="4800" y1="960" y2="1712" x1="4800" />
            <wire x2="4800" y1="1712" y2="2384" x1="4800" />
            <wire x2="3920" y1="192" y2="528" x1="3920" />
            <wire x2="4800" y1="1712" y2="1712" x1="3952" />
            <wire x2="3952" y1="1712" y2="1856" x1="3952" />
            <wire x2="4800" y1="960" y2="960" x1="4336" />
            <wire x2="4336" y1="960" y2="1072" x1="4336" />
        </branch>
        <branch name="XLXN_298">
            <wire x2="2832" y1="2384" y2="2736" x1="2832" />
        </branch>
        <branch name="XLXN_299">
            <wire x2="2896" y1="2384" y2="2736" x1="2896" />
        </branch>
        <branch name="XLXN_300">
            <wire x2="2960" y1="2384" y2="2736" x1="2960" />
        </branch>
        <branch name="XLXN_301">
            <wire x2="3024" y1="2384" y2="2736" x1="3024" />
        </branch>
    </sheet>
</drawing>