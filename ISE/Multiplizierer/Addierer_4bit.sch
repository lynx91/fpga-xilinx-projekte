<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_21" />
        <signal name="Add_4_En_B" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_33" />
        <signal name="Add_4_En" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_16" />
        <signal name="Add_4_S0" />
        <signal name="Add_4_S_1" />
        <signal name="Add_4_S2" />
        <signal name="Add_4_S3" />
        <signal name="Add_4_Out_C" />
        <signal name="XLXN_44" />
        <signal name="XLXN_45" />
        <signal name="Add_4_In_C" />
        <signal name="Add_4_B0" />
        <signal name="Add_4_A0" />
        <signal name="Add_4_A1" />
        <signal name="Add_4_B1" />
        <signal name="Add_4_B2" />
        <signal name="Add_4_A2" />
        <signal name="Add_4_A3" />
        <signal name="Add_4_B3" />
        <port polarity="Input" name="Add_4_En_B" />
        <port polarity="Input" name="Add_4_En" />
        <port polarity="Output" name="Add_4_S0" />
        <port polarity="Output" name="Add_4_S_1" />
        <port polarity="Output" name="Add_4_S2" />
        <port polarity="Output" name="Add_4_S3" />
        <port polarity="Output" name="Add_4_Out_C" />
        <port polarity="Input" name="Add_4_In_C" />
        <port polarity="Input" name="Add_4_B0" />
        <port polarity="Input" name="Add_4_A0" />
        <port polarity="Input" name="Add_4_A1" />
        <port polarity="Input" name="Add_4_B1" />
        <port polarity="Input" name="Add_4_B2" />
        <port polarity="Input" name="Add_4_A2" />
        <port polarity="Input" name="Add_4_A3" />
        <port polarity="Input" name="Add_4_B3" />
        <blockdef name="Addierer_1Bit">
            <timestamp>2014-10-29T8:23:41</timestamp>
            <rect width="304" x="64" y="-320" height="320" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="432" y1="-288" y2="-288" x1="368" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <block symbolname="buf" name="XLXI_5">
            <blockpin signalname="Add_4_En_B" name="I" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_10">
            <blockpin signalname="Add_4_En" name="I" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="Addierer_1Bit" name="XLXI_2">
            <blockpin signalname="Add_4_A2" name="Add_1_A" />
            <blockpin signalname="XLXN_21" name="Add_1_En_B" />
            <blockpin signalname="Add_4_B2" name="Add_1_B" />
            <blockpin signalname="Add_4_S2" name="Add_1_S" />
            <blockpin signalname="XLXN_11" name="Add_1_Out_C" />
            <blockpin signalname="XLXN_24" name="Add_1_En" />
            <blockpin signalname="XLXN_12" name="Add_1_In_C" />
        </block>
        <block symbolname="Addierer_1Bit" name="XLXI_1">
            <blockpin signalname="Add_4_A3" name="Add_1_A" />
            <blockpin signalname="XLXN_21" name="Add_1_En_B" />
            <blockpin signalname="Add_4_B3" name="Add_1_B" />
            <blockpin signalname="Add_4_S3" name="Add_1_S" />
            <blockpin signalname="Add_4_Out_C" name="Add_1_Out_C" />
            <blockpin signalname="XLXN_24" name="Add_1_En" />
            <blockpin signalname="XLXN_11" name="Add_1_In_C" />
        </block>
        <block symbolname="Addierer_1Bit" name="XLXI_4">
            <blockpin signalname="Add_4_A0" name="Add_1_A" />
            <blockpin signalname="XLXN_21" name="Add_1_En_B" />
            <blockpin signalname="Add_4_B0" name="Add_1_B" />
            <blockpin signalname="Add_4_S0" name="Add_1_S" />
            <blockpin signalname="XLXN_16" name="Add_1_Out_C" />
            <blockpin signalname="XLXN_24" name="Add_1_En" />
            <blockpin signalname="Add_4_In_C" name="Add_1_In_C" />
        </block>
        <block symbolname="Addierer_1Bit" name="XLXI_3">
            <blockpin signalname="Add_4_A1" name="Add_1_A" />
            <blockpin signalname="XLXN_21" name="Add_1_En_B" />
            <blockpin signalname="Add_4_B1" name="Add_1_B" />
            <blockpin signalname="Add_4_S_1" name="Add_1_S" />
            <blockpin signalname="XLXN_12" name="Add_1_Out_C" />
            <blockpin signalname="XLXN_24" name="Add_1_En" />
            <blockpin signalname="XLXN_16" name="Add_1_In_C" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="XLXN_21">
            <wire x2="576" y1="80" y2="80" x1="560" />
            <wire x2="832" y1="80" y2="80" x1="576" />
            <wire x2="1088" y1="80" y2="80" x1="832" />
            <wire x2="1344" y1="80" y2="80" x1="1088" />
            <wire x2="1344" y1="80" y2="576" x1="1344" />
            <wire x2="1088" y1="80" y2="1040" x1="1088" />
            <wire x2="832" y1="80" y2="1504" x1="832" />
            <wire x2="576" y1="80" y2="1968" x1="576" />
        </branch>
        <instance x="336" y="112" name="XLXI_5" orien="R0" />
        <branch name="Add_4_En_B">
            <wire x2="336" y1="80" y2="80" x1="304" />
        </branch>
        <iomarker fontsize="28" x="304" y="80" name="Add_4_En_B" orien="R180" />
        <branch name="XLXN_24">
            <wire x2="704" y1="160" y2="160" x1="560" />
            <wire x2="960" y1="160" y2="160" x1="704" />
            <wire x2="1216" y1="160" y2="160" x1="960" />
            <wire x2="1472" y1="160" y2="160" x1="1216" />
            <wire x2="1472" y1="160" y2="576" x1="1472" />
            <wire x2="1216" y1="160" y2="1040" x1="1216" />
            <wire x2="960" y1="160" y2="1504" x1="960" />
            <wire x2="704" y1="160" y2="1968" x1="704" />
        </branch>
        <instance x="336" y="192" name="XLXI_10" orien="R0" />
        <branch name="Add_4_En">
            <wire x2="336" y1="160" y2="160" x1="304" />
        </branch>
        <iomarker fontsize="28" x="304" y="160" name="Add_4_En" orien="R180" />
        <instance x="736" y="1504" name="XLXI_2" orien="R90">
        </instance>
        <branch name="XLXN_11">
            <wire x2="768" y1="1936" y2="1968" x1="768" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="1024" y1="1472" y2="1488" x1="1024" />
            <wire x2="1024" y1="1488" y2="1504" x1="1024" />
        </branch>
        <instance x="480" y="1968" name="XLXI_1" orien="R90">
        </instance>
        <branch name="XLXN_16">
            <wire x2="1280" y1="1008" y2="1024" x1="1280" />
            <wire x2="1280" y1="1024" y2="1040" x1="1280" />
        </branch>
        <instance x="1248" y="576" name="XLXI_4" orien="R90">
        </instance>
        <branch name="Add_4_S0">
            <wire x2="1536" y1="1008" y2="1040" x1="1536" />
        </branch>
        <branch name="Add_4_S_1">
            <wire x2="1280" y1="1472" y2="1488" x1="1280" />
            <wire x2="1280" y1="1488" y2="1504" x1="1280" />
        </branch>
        <branch name="Add_4_S2">
            <wire x2="1024" y1="1936" y2="1968" x1="1024" />
        </branch>
        <branch name="Add_4_S3">
            <wire x2="768" y1="2400" y2="2432" x1="768" />
        </branch>
        <instance x="992" y="1040" name="XLXI_3" orien="R90">
        </instance>
        <branch name="Add_4_Out_C">
            <wire x2="512" y1="2400" y2="2432" x1="512" />
        </branch>
        <iomarker fontsize="28" x="1536" y="1040" name="Add_4_S0" orien="R90" />
        <iomarker fontsize="28" x="1280" y="1504" name="Add_4_S_1" orien="R90" />
        <iomarker fontsize="28" x="1024" y="1968" name="Add_4_S2" orien="R90" />
        <iomarker fontsize="28" x="768" y="2432" name="Add_4_S3" orien="R90" />
        <iomarker fontsize="28" x="512" y="2432" name="Add_4_Out_C" orien="R90" />
        <branch name="Add_4_In_C">
            <wire x2="1536" y1="400" y2="576" x1="1536" />
        </branch>
        <branch name="Add_4_B0">
            <wire x2="1280" y1="400" y2="576" x1="1280" />
        </branch>
        <branch name="Add_4_A0">
            <wire x2="1408" y1="400" y2="576" x1="1408" />
        </branch>
        <iomarker fontsize="28" x="1536" y="400" name="Add_4_In_C" orien="R270" />
        <iomarker fontsize="28" x="1408" y="400" name="Add_4_A0" orien="R270" />
        <iomarker fontsize="28" x="1280" y="400" name="Add_4_B0" orien="R270" />
        <branch name="Add_4_A1">
            <wire x2="1152" y1="960" y2="1040" x1="1152" />
        </branch>
        <branch name="Add_4_B1">
            <wire x2="1024" y1="960" y2="1040" x1="1024" />
        </branch>
        <branch name="Add_4_B2">
            <wire x2="768" y1="1440" y2="1504" x1="768" />
        </branch>
        <branch name="Add_4_A2">
            <wire x2="896" y1="1440" y2="1504" x1="896" />
        </branch>
        <branch name="Add_4_A3">
            <wire x2="640" y1="1920" y2="1968" x1="640" />
        </branch>
        <branch name="Add_4_B3">
            <wire x2="512" y1="1920" y2="1968" x1="512" />
        </branch>
        <iomarker fontsize="28" x="512" y="1920" name="Add_4_B3" orien="R270" />
        <iomarker fontsize="28" x="640" y="1920" name="Add_4_A3" orien="R270" />
        <iomarker fontsize="28" x="768" y="1440" name="Add_4_B2" orien="R270" />
        <iomarker fontsize="28" x="896" y="1440" name="Add_4_A2" orien="R270" />
        <iomarker fontsize="28" x="1024" y="960" name="Add_4_B1" orien="R270" />
        <iomarker fontsize="28" x="1152" y="960" name="Add_4_A1" orien="R270" />
    </sheet>
</drawing>